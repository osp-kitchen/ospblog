#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'OSP'
SITENAME = 'osp blog'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

RELATIVE_URLS = True

STATIC_PATHS = ['images']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/category/%s.atom.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = 'feeds/author/%s.atom.xml'
AUTHOR_FEED_RSS = None
TAG_FEED_RSS = 'feeds/tag/%s.atom.xml'

THEME = "theme/osp-blog"
CSS_FILE = 'screen.css'
THEME_STATIC_DIR = 'theme'

DEFAULT_PAGINATION = 10

DISPLAY_CATEGORIES_ON_MENU = True

# URL settings
ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
CATEGORY_URL = '{slug}.html'
CATEGORY_SAVE_AS = '{slug}.html'
ARTICLE_LANG_URL = '{category}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = '{category}/{slug}-{lang}.html'
PAGE_LANG_URL = '{slug}-{lang}.html'
PAGE_LANG_SAVE_AS = '{slug}-{lang}.html'

DIRECT_TEMPLATES = [
'index', 'tags', 'categories', 'authors', 'archives', 'search']

PLUGIN_PATHS = ['plugins']
PLUGINS = ['tipue_search', 'neighbors',]

TIPUE_SEARCH = True

YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.attr_list': {},
    },
    'output_format': 'html5',
}


# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
