## OSP-blog

This repo aims to study if a port of the OSP wordpress blog to a Pelican structure is viable. Meanwhile it's a a backup or an export or a migration.

* http://ospublish.constantvzw.org/
* http://docs.getpelican.com/en/stable/importer.html
* getpelican.com/
* http://ospublish.constantvzw.org/blog/attachments

xml files at root are wordpress exports, that Pelican imported

### listing of the virtualenv requirements to run the blog locally: (includes some unusual dependencies for pelican-import)

beautifulsoup4==4.6.0
blinker==1.4
bs4==0.0.1
docutils==0.14
feedgenerator==1.9
Jinja2==2.9.6
lxml==4.0.0
Markdown==2.6.9
MarkupSafe==1.0
pelican==3.7.1
pkg-resources==0.0.0
Pygments==2.2.0
python-dateutil==2.6.1
pytz==2017.2
six==1.11.0
Unidecode==0.4.21

