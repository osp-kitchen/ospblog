Title: Turning around practises
Date: 2009-03-25 16:23
Author: Lauren
Tags: Education, Tools
Slug: turning-around-practises
Status: published

A month ago I decided to do the step. Doing a partition on my computer
forced me to cynically erase all my data. Since Mac OSX Tiger did not
provide any kind of Bootcamp assistant (it was exclusively held for
Leopard) and executing partitions through the Terminal was not really
pleased by my system, I was forced to reinstall my whole system. That
means installing Leopard, doing a partition and finally installing
Ubuntu. Summarized it sounds quiet easy.

So the adventure started after having installed Ubuntu. Scribus was my
hope for the toolkit that I had to make for little kids. . Although it
takes illogical twists from time to time, it is does bare possibilities.
Exporting to pdf did pass through some problems. Exporting a dashed
hairline for instance is impossible. It keeps it as a hairline unless
you make it 0,3 mm. Which is almost a hairline... Some funny twists.

Completing my task for a
[Boektegoed](http://www.uitinvlaanderen.be/agenda/e/boektegoed-maak-je-eigen-boek-in-de-bib/616D610F-0FCF-E4C7-B23A1DC2E808AE9E)
through open source software was a challenge. Being so used to use your
computer through patterns of habits, it was quiet an experience. Turning
around practises, un bouleversement qui est inévitable.
![bouleversement]({filename}/images/uploads/picture-17.png "bouleversement"){: .alignleft .size-medium .wp-image-2300 }
