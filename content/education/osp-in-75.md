Title: OSP in 75
Date: 2008-12-10 16:28
Author: Harrisson
Tags: Education, Live Print Party, Workshops + teaching
Slug: osp-in-75
Status: published

[![]({filename}/images/uploads/macaron_osp_75.png "macaron_osp_75"){: .alignnone .size-medium .wp-image-1581 }]({filename}/images/uploads/macaron_osp_75.png)

OSP is invited for a workshop day at 75 school, in Woluwe, Brussels.  
Wednesday 17 December.  
Music playlisting, video browsing, recipes printing, font remixing and
pancake making.
