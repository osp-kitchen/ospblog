Title: parallel publishing I
Date: 2009-03-05 15:05
Author: Ivan
Tags: Education, News, Education, Handmade, Standards + Formats, Workshops + teaching
Slug: parallalel-publishing-i
Status: published

on the first day of our workshop at
[bordeaux](http://www.rosab.net/edit/), we decided to collectively
re-enact an esther ferrer performance. this is a
[youtube](http://www.youtube.com/watch?v=r6yVM4hnP7U) that gives an idea
of her movements.

with this as starting point, we drew our own paths in the
[capc](http://www.bordeaux.fr/ebx/portals/ebx.portal?_nfpb=true&_pageLabel=pgSomRub11&classofcontent=sommaire&id=2010)...  
![ferrer\_edit1]({filename}/images/uploads/1.jpg "ferrer_edit1"){: .size-full .wp-image-2040 }  
![ferrer\_edit0]({filename}/images/uploads/0.jpg "ferrer_edit0"){: .size-full .wp-image-2040 }
