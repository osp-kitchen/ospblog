Title: Hit any key to exit
Date: 2011-01-31 23:28
Author: Femke
Tags: Tools, Culture of work, History, Licenses, Thoughts + ideas
Slug: hit-any-key-to-exit
Status: published

![]({filename}/images/uploads/hit.png "hit"){: .alignnone .size-full .wp-image-5668 width="280" height="53"}

Finally... *How to choose and install ~~four~~ three new distributions*

Since announcing that it [was time for a new
adventure](http://ospublish.constantvzw.org/tools/time-for-a-new-adventure),
colleagues and friends have advised me what to install and why (see
notes below). In the last few weeks I tried to follow up on their tips
and I have tried out many distributions.

I decided for a multi-boot with Debian, Fedora plus two other
distributions. The plan was that by using the same home partition for
different installs, it should be easy to move between them.

In the quiet period between Christmas and New Year, I started up Ubuntu
for the last time. I used the usb-live-creator to make a bootable
usb-stick with a Debian iso-image on it. Once finished, it ironically
congratulated me:  
'*Now you can run Ubuntu from a stick!*'  
I reboot, and the adventure begins.

In-between downloads and file-checks, partitions, bootloaders,
grub-rescue, lost file systems, unknown linux kernels, Master Boot
Records, fdisk, ext2, ext3, ext4, i386, initrd and initramfs I often
lose track of what I am trying to do. I start to include the term
'newbie' with my searches; most explanations that come up when I just
look for the error messages, make no sense to me. The split that Ubuntu
makes between different levels of use (color-coding them!) might be
patronizing and annoying, but to typecast myself as beginner, dummie,
noob … that feels terrible too.  
<!--more-->  
Slowly, through trial and error, I begin to see the difference between
the messiness of some installers, and my own misunderstanding of the
boot-process. I'm not completely confident about manipulating grub.conf
yet, but after two days I get to grips with the system again. In the
mean time, I have tried to clarify the goal of the exercise:

1.  Find out about projects that are clear about their allegiance with
    Free Software
2.  Choose a new distribution that I can work with: somehow inviting
    different levels of use; minimal needs: functional wireless,
    multiple screens, microphone, preferably out of the box
3.  Learn about Free Software vocabularies, communities, cultures
    (visual and textual vocabularies of digital desktops)
4.  Switch to a distro that has an interesting approach to design

When most Free Software activists lie awake about non-free kernel blobs,
I care about naming schemes, fonts, logo's and icon-sets. The relation
between software politics and visual representation is of special
interest to us at OSP; we think that Free Software should run more than
kernel-deep.

Three days later, I settle surprisingly (disappointingly?) close to home
with just two very similar distributions running: [Debian
Squeeze](http://www.debian.org/releases/testing/) ('unstable') and
[Trisquel](http://trisquel.info/). I have tried to install Fedora as a
third option, but encounter multiple problems with the install media
(live-usb, live-cd, netinstall). I do manage to boot Fedora 13 at some
point but for the final installation-round keyboard and mouse are not
recognized so I will return to this later.

![]({filename}/images/uploads/P1110001-400x3002.jpg "P1110001-400x300"){: .alignnone .size-full .wp-image-5668 width="280" height="53"}

Debian and Trisquel have very different approaches to 'how free software
is made', although both are based on Debian and both use Gnome as the
default window-manager. Where Debian clarifies, develops and negotiates
it's position based on a constitution, social contract, and policy
documents ... Trisquel is more like a cool 'de-branding'-exercise, using
scripts to clean out and sometimes replace non-free elements from the
Ubuntu kernel. Strict (my Lenovo wireless card won't work as it requires
non-free particles) and elegant. It is also one of the rare
distributions endorsed by the Free Software Foundation
((<http://en.wikipedia.org/wiki/List_of_Linux_distributions_endorsed_by_the_Free_Software_Foundation>)).

The Debian community is not busy with design ((famously the Debian
typeface is proprietary <http://wiki.debian.org/DebianLogo>)) as is
evident from their laconicly un-styled documents, manuals and other web
pages. Debian apparently leaves all interface decisions to the gnome
project; the large and widespread project has really no design-team at
work.

Trisquel is a bit more interesting in that sense; if you don't take
their neo-druidism too litteral (release names: Awen, Dwyn, Robur and
Taranis), it has an intelligent approach to their redrawing of gnome;
uses Droid fonts, presents their logo as a Debian-hommage and provides
the nicest packagemanager icon as of yet.

![]({filename}/images/uploads/update-manager_th.png "update-manager_th"){: .alignnone .size-full .wp-image-5668 width="280" height="53"}

But since both Trisquel and Debian use gnome, and the gnome-preferences
are stored in the .gconfd folder. In my clever one-home-multiple install
solution, both installs point to the same folder so it means that they
end up looking *exactly* the same :-)

Two weeks later, I add CrunchBang to the collection. CrunchBang is
another recent Debian spin that a friend at
[Samedies](http://samedi.collectifs.net) suggests. It's a relief to work
with OpenBox (the windowmanager installed by default) and I enjoy the
way it invites to edit config files directly, the way the Conky
system-manager works and the tone of the CrunchBang forums feels right:
assuming an interest in experimental use, but ready to explain even if
questions are basic. “Anyone who uses CrunchBang should be comfortable
with occasional or even frequent breakage”.  
The project seems to find a nice balance between GUI and
configurability, and prefers to direct you to commandline rather than
GUI but does so without the kinds of discouraging warnings that Debian
likes to issue. With great power comes great responsibility.

![]({filename}/images/uploads/Screenshot-snelting@fs-debian_-.png "Screenshot-snelting@fs-debian_ ~"){: .alignnone .size-full .wp-image-5668 width="280" height="53"}

Philip Newborough, 'distro leader' of CrunchBang explains why the
decision was made to base the newest release on Debian, and not on
Ubuntu:

“Unlike the Ubuntu project, Debian does not have a commercial sponsor
with any commercial interests. This was never an issue for myself, until
recently when Canonical seem to have become less of a sponsor and more
of a governing party”
((<http://reddevil62-techhead.blogspot.com/2010/03/interview-crunchbang-creator-explains.html>))

The list of software included is nicely different from most other Debian
derivatives, although I am surprised to find Google Chrome, Flash and
Skype installed by default; it might be more about delivering 'modern'
and 'sleek' than anything else. CrunchBang aesthetics are a sort of
contemporary-geek, meaning that most of the visual identity is made up
of text, preferably in black-and-white (("The new design will be
changing in the opposite direction to that recently taken by the Ubuntu
design team, which only seems fitting. So, whereas the new style in
Ubuntu is inspired by the idea of “Light”, the new style for CrunchBang
will be inspired by the idea of “Dark”. o\_O"
<http://crunchbanglinux.org/blog/2010/11/20/new-website-design/>)).

The next distro on the list is Arch-Linux (and Fedora of course), but
for the time being I'll happily stick with CrunchBang.

![]({filename}/images/uploads/2011-01-23-1295803149_1280x1024_scrot.png "2011-01-23--1295803149_1280x1024_scrot"){: .alignnone .size-full .wp-image-5668 width="280" height="53"}

\[to be continued\]

<div style="font-family:mono; font-size: 10px;">

**N O T E S**
</p>
*What I was advised to install*:

**Free Software Foundation endorsed distributions**  
<http://en.wikipedia.org/wiki/List_of_Linux_distributions_endorsed_by_the_Free_Software_Foundation>

Only 10 or so? Very limited list. Excluding even Debian; seems extremely
purist. Imagery reflects. Quite a few Ubuntu liberation projects,
relatively. This discussion between Shuttleworth and Stallman is
reffered to: <http://www.iosn.net/regional/wsis-2005> »
<http://video.google.com/videoplay?docid=-694927630239078625> (Stallman
on cooking and recipes. Comparing proprietary software to colonialism in
Tunis. *Don't let freedom slip through your fingers because you don't
bother to close their hands. Divide and rule is the nature of
proprietary software; giving local elites privileges; in return they can
keep others down. Addicts, creating dependencies through gratis
software. Recruiting schools as agents.*)

Very many projects with Spanish language base, or language issues as a
starting point. Ones I find interesting:

- BLAG  
- Trisquel - Ubuntu based  
- gNewSense - Ubuntu based, next release Debian  
- Dragora has best image sofar:
<http://dragora.org/wiki/_media/wiki.png> but hardly any documentation
in English  
- Ututo - Gentoo based - all in Spanish

**Peter L** / *OpenSuse*  
"It runs QT ten times faster and as I told you years ago, you'll
certainly notice the difference in running Scribus."

**Michael M** / *Mint*  
"Try Mint. It is just like Ubuntu with the outer layer removed."

**OSP**  
- *Linux Arch*  
- *Linux From Scratch* (Pierre M: 'Constant needs to run LFS
somewhere')  
- “*Debian* is always there as a fall-back”  
- *slackware*

**Juliane** / *Black Snake*  
"If you want to be like a system administrator"

**Dave C** / *Fedora*  
I have been using Fedora very happily for 18 month now – it has the most
libre policies of any popular distro, and I am very excited about the
future of its design team. I think in Fedora’s governance structures and
Red Hat’s strategy, RHAT has got the balance between corporate
sponsorship/paid contributors and community participation far more right
than Debian/Ubuntu, Novell/SUSE, or any others. It also helps that is
also the most cutting edge distro as well as the most libre :) And the
rpmfusion repos are set up very sensibly too, with a good libre archive
for things free in EU but not USA.

**John H**  
There are at least two really alternative OSes that I think it would be
great for you to try: Haiku and Etoile OS. I think there is probably
some optimally old hardware for running Haiku available in the Constant
offices already. I'll be doing a reinstall of BeOS on my old k6-2
machine when I am back in the states and comparing it to Haiku on the
same hardware. Etoile is a re-implemntation of the entire Alan Kay
Dynabook project, complete with Smalltalk programmability and modular
application blocks that can be linked together into personal
applications :)

(Linux? both under MIT/BSD license)

**Nicolas M**  
<em>My last attempt at Debian was… difficult and the
technocracy/meritocracy/bureaucracy in place doesn't make me feel more
comfortable. I don't feel like leaving Ubuntu yet. Time to experiment
multi-boots.

I agree the OS is very important but I would really like to understand
what is pushing the whole array of closures happening right now and
trying to figure out where are the priorities, because we will not be
able to be on every front. I have the impression we need to come up with
a global critique and proposal from the inside. I think that we need to
understand where we made mistakes also ourselves. We, also, made it
possible for all this to happen. And nothing was hidden to us.

Found this while looking for complementary info about Christophe's text:
<http://www.debian.org/vote/2010/vote_002></em>

**Sophie S** / *CrunchBang*  
"A Debian GNU/Linux based distribution offering a great blend of speed,
style and substance. Using the nimble Openbox window manager, it is
highly customisable and provides a modern, full-featured GNU/Linux
system without sacrificing performance."

**Alex L + Stephanie V** / *Archlinux*  
The odd one out.  
<https://wiki.archlinux.org/index.php/The_Arch_Way>

D I S T R I B U T I O N S

**Debian**  
As one of the largest distributions around, it has been referred to as
the 'default fallback option' but I think this non-commercial project
deserves more than that. Although technically very close to Ubuntu,
politically and culturally Debian is on another planet. Preferring slow
releases over cutting edge, an almost obsessive attention for permission
structures, organised according to a constitution, social contract, and
policy documents, this distro targets administrators as well as desktop
users like me.

The 'Debian Social Contract' establishes the community as a society.

“We acknowledge that some of our users require the use of works that do
not conform to the Debian Free Software Guidelines. We have created
“contrib” and “non-free” areas in our archive for these works. The
packages in these areas are not part of the Debian system, although they
have been configured for use with Debian. We encourage CD manufacturers
to read the licenses of the packages in these areas and determine if
they can distribute the packages on their CDs. Thus, although non-free
works are not a part of Debian, we support their use and provide
infrastructure for non-free packages (such as our bug tracking system
and mailing lists).”  
<http://www.debian.org/social_contract>

I like how they manage to be clear without purism but I am most of all
impressed by the discussions this self-defined rule of conduct allows.
The establishment of groups that work on the inclusion of women in
kernel development for example, is a result of a community that has does
not take inclusiveness for granted. Though both Ubuntu-women and
Fedora-women exist, Debian was the first to recognize the issue. Its
societal approach (elections and votes!) obviously also accounts for a
bureaucratic culture that is taking itself extremely serious. Seems
largeness of project also invites reflection on why, what, how.

“The Debian project has been working in removing non-free firmware from
the Linux kernel shipped with Debian for the past two release cycles. At
the time of the releases of Debian 4.0 “Etch” and 5.0 “Lenny”, however,
it was not yet possible to ship Linux kernels stripped of all non-free
firmware bits. Back then we had to acknowledge that freedom issues
concerning Linux firmware were not completely sorted out”

Why not Fedora:  
"Red Hat is in a tough spot. Most of their revenue streams are based on
sales, support, and training while the open nature of Linux has resulted
in thousands of freely-available Linux resources on the Web. Their
survival depends on having a product that is proprietary enough to make
you dependent upon them for upgrades and support. And when they became a
publically-held company they were under pressure to meet the
expectations of Wall Street analysts for revenue growth and cash flows
every quarter. (Did you think it was just a coincidence that they were
churning out new versions of what is now Fedora at an average of two a
year?) In time, Red Hat's dominance will likely kill off smaller
commercial distributions like Mandrake and TurboLinux and dealing with
Red Hat will be no different than dealing with Microsoft."
<http://www.aboutdebian.com/>

Visual culture is brutal. Amazingly little attention for detail,
typography, lay-out. Looks like early days computer culture.  
<http://www.snt.utwente.nl/logo/logo.php?style=2&text=FTP%20Service>

Naming scheme: Toy Story :-O  
Debian = Debra Murdoch + Ian

"If you can boot from a CD, boot a live CD that has grub2 and use it to
reinstall grub to the master boot record. There are plenty of tutorials
explaining how to do this step-by-step."

Master Boot Record <http://en.wikipedia.org/wiki/Master_boot_record>

Familiar with slight differences ... i need to add myself to the
sudo-ers file, the same old warning about using power responsibly etc.
and upon opening Synaptic, I get this message:

"Granted permissions without asking for password.  
The '/usr/sbin/synaptic' program was started with the privileges of the
root user without the need to ask for a password, due to your system's
authentication mechanism setup.  
It is possible that you are being allowed to run specific programs as
user root without the need for a password, or that the password is
cached.  
*This is not a problem report; it's simply a notification to make sure
you are aware of this*"

Switching to Iceweasel - a logo that creates trouble:

"Some of the icons and artwork used in Firefox are trademarked and
copyrighted. According to my limited understanding, this violates
DFSG\#1 which states the component must be freely redistributable and
DFSG \#3 which states that the license must allow derivatives. If Debian
was granted some sort of special permission to use these trademarked
items, it would violate DFSG \#8 which states that licenses must not be
specific to Debian. I have also seen some discussion over whether Debian
can or should use the “Firefox” name without using the official icons."
<http://n01getsout.com/blog/2006/11/18/why-iceweasel-instead-of-firefox-on-debian>

"This effect of the Mozilla trademark policy led to a long debate within
the Debian Project in 2004 and 2005. (...) 'Iceweasel' was subsequently
used as the example name for a rebranded Firefox in the Mozilla
Trademark Policy, and became the most commonly used name for a
hypothetical rebranded version of Firefox. By January 1, 2005,
rebranding as a strategy was being referred to as the 'Iceweasel
route'."
[http://en.wikipedia.org/wiki/IceWeasel](https://fedoraproject.org/w/uploads/thumb/e/e7/4Foundations.png/150px-4Foundations.png)

"There is an extension that lets you change your user agent on-the-fly.
Moreover, this site has provided a definition file that lets you browse
as if you were on Firefox 2 for Windows XP"

**Fedora**  
“Freedom. Friends. Features. First.”. Logo: eternal 8. All logos carry
tm signs.
<https://fedoraproject.org/w/uploads/thumb/e/e7/4Foundations.png/150px-4Foundations.png>

Confident tone: Thanks for downloading Fedora! We know you'll love it!

The freedom, friend stuff is a bit much, especially since it blends so
well with the other blue F. Do like the way this friendship was
presented at LGM.

The imagery, tagline puts 'values' high on the list - comparable with
Debian. It seems making the difference to Red Hat proper, requires the
project to define itself along the lines of this. Remember CC problems:
'sharing' is done amongst friends. Again, the Debian harshness seems
more interesting to me. Why exactly?

“The four foundations are the core values of the Fedora community. They
sprung from work on the Fedora marketing plan, and have replaced the old
“infinity, freedom, voice” slogan. The original slogan emerged from the
design of the Fedora logo. That logo has become a very powerful and
effective part of Fedora's brand and image, but does not sufficiently
describe our core values in a clear and effective way.”

So, from 'infinity, freedom, voice' to 'Freedom. Friends. Features.
First'. Note the full stops replacing comma's.

The last F, First might be more interesting than the trophy images
promises. “Fedora always aims to provide the future, first”. Well,
'provide the future' is a bit pedantic but there is a sense of curiosity
that speaks from the definition of 'first' that I like. It obviously
pits itself against the slow development cycle of Debian.

Fedora Spins are interesting; a distribution that promotes customization
of the distro itself…

“Fedora Spins are alternate version of Fedora, tailored for various
types of users via hand-picked application sets and other
customizations.”

"It would be nice to have content in a design spin - but we don't have a
policy for packaging content. Content like gimp brushes, color palettes,
fonts... eventually one idea was to have a Fedora Design Studio, which
would have one for graphic design, web design, etc."  
<http://fedoraproject.org/wiki/Talk:Design_Suite>

Getting frustrated with installation -- try every possible way of
creating the Live-usb stick (unetbooting, live-usb creator, commandline)
from different sources and with different iso images, but the result is
always the same mysterious message:

Boot error - Sleeping forever

A recurring problem?  
[http://forums.fedoraforum.org/showthread.php?t=234025&highlight=LIVEUSB+NO+ROOT  
https://fedorahosted.org/liveusb-creator/report/1?sort=created&asc=0  
](http://forums.fedoraforum.org/showthread.php?t=234025&highlight=LIVEUSB+NO+ROOT%20https://fedorahosted.org/liveusb-creator/report/1?sort=created&asc=0)

Finally, I figure out how to use the netinstall for F12 to install F13
(not latest; netinstall for 14 fails too) and than to upgrade to F14
with the help of <http://fedoraproject.org/wiki/PreUpgrade>  
Unfortunately no net-install exists for spins.

The install is very slow (2 hrs), but successful. While subsequently
updating the system (takes over an hour as well) I explore this new
territory: Fedora is surprisingly similar in structure, with only small
differences, as far as I can discover.

Difference between Fedora and Ubuntu:  
"But when distributions use the same desktop, the way that Fedora and
Ubuntu do, then the differences are likely to be unnoticeable to three
out of four users. These days, you are even unlikely to find any
differences in speed or stability unless you have some unusual hardware
configuration."
<http://itmanagement.earthweb.com/osrc/article.php/12068_3862556_3/Fedora-vs-Ubuntu-Is-Either-Better.htm>

Too small partition, running out of space for the update. Decide to
install 3rd system and re-size partition using gparted. gparted does not
work on LVM

**Trisquel**  
Visual references, naming scheme: neo-druidry / celtic - releases: Awen,
Dwyn, Robur, Taranis. Developed for/in Galician.

“Trisquel is a fully free operating system based in GNU/Linux, for home
users, small enterprises and educational centers.”
<http://trisquel.info>

“Our logo is the triskelion, a Celtic symbol of evolution and wisdom.
Our Trisquel (the Spanish name for this triple spiral form) resembles
the Debian logo, as a form of recognition of the distro we originally
based our project on. The base color we use is \#004DB1, and the font is
Droid Sans.”
[http://trisquel.info/en/wiki/logo](http://fluxbox.org/screenshots/)

"Trisquel does not include the vanilla Linux kernel you can find at the
Linux project servers, but a cleaned up version of Ubuntu's version of
the kernel. Both the upstream versions include non-free binary-only
firmware files, and also a lot of binary blobs hidden in .c and .h files
in the form of huge sequences of numbers. To provide our users with a
fully free kernel we use a set of scripts based in the ones from
Linux-libre, with some modifications of our own."  
[http://trisquel.info/en/wiki/how-trisquel-made](http://fluxbox.org/screenshots/)

The 'purifying projects' are scary -- lack humor. Very obedient to Mr.
Stallman. Still, the method they choose is super interesting: standing
on the hands of giants; nice parasites.

Installing trisquel, means no wireless. Help ends me to the FSF list of
non-free hardware. I get the point, but what to do?  
[http://intellinuxwireless.org/?n=downloads](http://fluxbox.org/screenshots/)

I can install Skype (with authentication warning - don't really
understand what they mean by 'risk'). Also a bit boring; nothing
changes? Otherwise everything works very well; even the microphone works
euhm ... as well as in Ubuntu.

**CrunchBang**  
"The new design will be changing in the opposite direction to that
recently taken by the Ubuntu design team, which only seems fitting. So,
whereas the new style in Ubuntu is inspired by the idea of “Light”, the
new style for CrunchBang will be inspired by the idea of “Dark”. o\_O"  
[http://crunchbanglinux.org/blog/2010/11/20/new-website-design/](http://fluxbox.org/screenshots/)

"Not recommended for anyone who requires a stable system. Anyone who
uses CrunchBang should be comfortable with occasional or even frequent
breakage. Remember, CrunchBang Linux could make your computer go CRUNCH!
BANG! :)"  
[http://reddevil62-techhead.blogspot.com/2010/03/interview-crunchbang-creator-explains.html](http://fluxbox.org/screenshots/)

Other packages I tried to install

**BLAG**

“works to overthrow corporate control of information and technology
through community action and spreading Free Software.”

Developed by the “brixton linux action group”, Graphics, language: very
English: <http://www.blagblagblag.org/images/programming.jpg> … quite
beautiful. Discovering fluxbox <http://fluxbox.org/screenshots/>

“BLAG 90000 (oxygen) is based on Fedora 9”

Not much has been happening the last year - I will come back once the
new version is stable?

\[live-usb produces infinite sleep as well\]

**gNewSense**

Visual references, naming scheme: new age?  
Bonzai tree = logo. “it does not use a lot of the shiny methods that
Ubuntu does”

“gNewSense is a GNU/Linux distribution based on Ubuntu. Its goal is to
maintain the user-friendliness of Ubuntu, but with all non-free software
and binary blobs removed. The Free Software Foundation considers
gNewSense to be a GNU/Linux distribution composed entirely of free
software.”

“Neither Debian nor Ubuntu are fully free. Ubuntu installs non-free
software by default. Debian provides non-free software through its
repositories and includes non-free kernel drivers. We were also the
first distribution to remove GLX, which Debian had ignored for years”  
<http://www.gnewsense.org>

Almost similar approach as Trisquel, but somehow less precise. If I need
to choose between neo-druids and neo-zen, the first wins. Esthetic
choice?

**Linux Mint**

Very difficult to feel the political difference between Ubuntu. It is
lighter, but how does it make a difference? I really do not see the
interest ... seems not very clear?

**Tests, choosers, comparison**  
<http://www.zegeniestudios.net/ldc> --&gt; type of license is not a
question  
<http://en.wikipedia.or/wiki/Comparison_of_Linux_distributions>

“Ubuntu provides specific repositories of nonfree software, and
Canonical expressly promotes and recommends nonfree software under the
Ubuntu name in some of their distribution channels. Even if you try to
avoid all of that, the default application installer will advertise
nonfree software to you.”  
<http://www.gnu.org/philosophy/common-distros.html>

M I S C E L L A N E O U S

How do you know whether swap is at sdb1 or sdb5? &gt; fdisk

<p>
Nice explanation of GRUB:
<http://www.linux.com/community/blogs/notes-about-grub-multiboot-configuration-filesystem-compatibility-and-repair.html>

</div>

==============================
