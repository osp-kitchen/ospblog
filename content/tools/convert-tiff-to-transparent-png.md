Title: Convert tiff to transparent PNG
Date: 2006-11-26 17:39
Author: nicolas
Tags: Tools, Command Line, How-to, Scripting
Slug: convert-tiff-to-transparent-png
Status: published

Since long, we wished to write about scripting for image creation and
manipulation. There are many reason why you would spend some time to do
it. To resize a lot of images by hand can be a tedious task, or your
software misses a component to achieve a particular result. Or you want
to turn a web application into an image editor, etc.

![From tiff to
png]({filename}/images/uploads/compare-trans.png){: #image158}

We will start with a modest example taken from a real life situation.
We, Femke and Nicolas, are working on an illustration in Inkscape. For
this illustration, we have scanned a lot of notes we have written on
paper. The scanned images have been saved in tiff. We have imported them
in Inkscape and started making the composition. Half-way we realise that
it should be a lot more easier to work with the same images but saved as
PNG with a transparent background. As there is 165 images to transform,
to do it one by one in Gimp sounds just frightening. This is where the
wonderful [Imagemagick](http://www.imagemagick.org/script/index.php)
software enters into play.  
<!--more-->  
Imagemagick is shipped with every major linux distribution or can easily
be installed by the different package managers. It is also available on
windows; and on MacosX via the Fink installer. Once there, Imagemagick
gives you many tools to edit, resize, transform images. One of them is
*convert* that takes a file in input and converts it into (nearly) any
format. In our case, a simple conversion was not enough since we wanted
also to transform the white colour into a transparent background. The
following command did the trick for one image:

> convert myfile.tiff -transparent white myfile.png

To apply it to a whole directory of images and keep the filenames, we
had to include it in a small shell script:

>     #!/bin/sh
>     for file in `ls | grep tiff`
>     do
>       convert "$file" -fuzz 5% -transparent white "${: #image158}.png"
>       echo "writing ${: #image158}.png"
>     done

The *fuzz* parameter makes it possible to give transparency to
'nearly-white' pixels.
