Title: Thank You PoDoFo
Date: 2009-10-02 10:18
Author: Femke
Tags: Tools, Not-Courier sans, Tools
Slug: thank-you-podofo
Status: published

![podofo]({filename}/images/uploads/podofo.jpg "podofo"){: .alignright .size-medium .wp-image-3503 }

When budget is limited and time is short: [PoDoFo
tiling](http://www.oep-h.com/impose/) + wallpaper glue +
[NotCourierSans](http://openfontlibrary.org/media/files/OSP/411) to the
rescue!

![P1060715]({filename}/images/uploads/P1060715.JPG "P1060715"){: .alignright .size-medium .wp-image-3503 }  
![P1060711]({filename}/images/uploads/P1060711.JPG "P1060711"){: .alignright .size-medium .wp-image-3503 }  
<small>Clementine mounts the [Constant
Verlag](http://constantvzw.org/verlag) colophon for its launch last
night in 17 Rue de la Senne</small>
