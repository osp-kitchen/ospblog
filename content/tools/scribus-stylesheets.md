Title: Stylesheets
Date: 2006-03-21 11:57
Author: Femke
Tags: Tools, Scribus
Slug: scribus-stylesheets
Status: published

Often I have wondered why DTP programmes did not have both an "edit
source" view and a "preview mode", so that you could alternate between
those two views and apply styles with more rigour if needed.  
<!--more-->  
You can see why I am so excited about using the Story Editor in Scribus.
It feels much closer to marking up HTML, which in turn is familiar to
the pre-computer practice of writing type setting instructions.

But looking at a Scribus .sla file in an editor, I realise that
typographic markup is completely mixed with character data (how on earth
does the story editor manage to pick out the right information!), which
might explain why this idea could be obvious in theory but harder to
achieve in practice.

Part of the unpredictability of style-behaviour in Scribus (in fact,
this is a problem with any text-layout programme I have ever used) I
guess has to do with this mixing as I can imagine re-applying styles a
few times will result in messy code. It feels quite similar to what
happens when applying deprecated markup in NVU or Dreamweaver from the
WYSIWYG editor, without cleaning up the source.

Other problems might occur through irregular interpretation of cascading
effects, but this is just a guess.

Right now, Styles in Scribus operate way more confused and less
sophisticated than even the simplest Cascading Style Sheet does. If
Scribus would apply the web standards ethics (separating content from
form ;-)), could it be possible to simply edit those styles in a file?

Over the last few years many graphic designers have taught themselves to
handle CSS stylesheets with grace, and it would seem logic to apply that
logic / these skills to Desk top publishing too.

In this way, Scribus could become more transparent and compatible with
other media. Not only through the way it handles its file-formats
(import-export), but also through how it connects different practices of
design.
