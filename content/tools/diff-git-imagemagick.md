Title: diff git imagemagick
Date: 2010-10-19 12:03
Author: Ivan
Tags: Tools, git
Slug: diff-git-imagemagick
Status: published

[![]({filename}/images/uploads/Imagemagick-logo.png "Imagemagick"){: .alignnone .size-full .wp-image-5085 }](http://ospublish.constantvzw.org/tools/diff-git-imagemagick/attachment/imagemagick-logo)

<a href="http://ospublish.constantvzw.org/tools/diff-git-imagemagick/attachment/200px-git-logo-svg" rel="attachment wp-att-5084">![]({filename}/images/uploads/200px-Git-logo.svg_.png "git"){: .alignnone .size-full .wp-image-5085 }

</a>From wikipedia: [*In computing, diff is a file comparison utility
that outputs the differences between two
files*](http://en.wikipedia.org/wiki/Diff).

We have been playing with git. There's more coming! For now, this quick
[prototype](http://ospublish.constantvzw.org/git_imagemagick_diff.ogg)
(30M) illustrates some ideas about diff, and images files. It will
hopefully be improved in the future :)

In the context of a configuration file, we are combining git's
flexibility regarding the
[choice](http://www.kernel.org/pub/software/scm/git/docs/git-difftool.html)
of a difftool with the [image
compare](http://www.imagemagick.org/Usage/compare/) features of
Imagemagick:

`[difftool "mydifftool"] cmd = composite $LOCAL $REMOTE -compose difference x:`

It's a digest of two stackoverflow threads:  
[Are there revision control systems for
images?](http://stackoverflow.com/questions/3368407/are-there-revision-control-systems-for-images)  
[How do I view 'git diff' output with visual diff
program?](http://stackoverflow.com/questions/255202/how-do-i-view-git-diff-output-with-visual-diff-program/)

It's all lo-fi.
