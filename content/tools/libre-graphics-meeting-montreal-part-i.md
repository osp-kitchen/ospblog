Title: Libre Graphics Meeting
Date: 2007-05-09 07:33
Author: Harrisson
Tags: Tools, LGM 2007
Slug: libre-graphics-meeting-montreal-part-i
Status: published

**Montreal, 4-6 May 2007**

[![squirl.JPG]({filename}/images/uploads/squirl.JPG){: .float}]({filename}/images/uploads/squirl.JPG "squirl.JPG")An
intense meeting of demos and how-to’s – LGM takes place in three fairly
intimate rooms in the Ecole Polytechnique of the University of Montreal,
which means we have enough time and opportunity to ask our questions,
set up interviews, discuss bread baking with developers, designers,
typographers, researchers gathered.

The university is located on the slopes of the Mont Royal so between
presentations we spot skunks and squirls in the park.

<!--more-->  
It is both inspiring and strangely intimidating to meet the makers of
many familiar tools: The Scribus team is present in full force, key
developers of Blender, Gimp and Inkscape are around and speak about
their tools with infectiuous enthusiasm. We need to make a bit of an
effort to make people understand that our interest goes beyond feature
requests and bug reports. Collective charm needed to convince George
Williams, developer of FontForge, to do an interview with us. “I have
nothing interesting to say” he keeps telling us during the Grand Supper.
Well, he finally agreed to an interview [so judge for your
selves](http://ospublish.constantvzw.org/?p=221).

[![inkscape.JPG]({filename}/images/uploads/inkscape.JPG)]({filename}/images/uploads/inkscape.JPG "inkscape.JPG"){: .float}**Friday
4 May** we land in LGM with a talk by Bryce Harrington + team,
presenting a history of Inkscape in screen shots
([slides](http://www.bryceharrington.org/lgm07/)), and an overview of
ways FLOSS projects are usually structured. Inkscape is according to
Bryce Harrington based on a 'hive' model; a relatively unstructured
coming and going of high energy collective work. Bryce ends with
presenting the upcoming release of Inkscape 0.46. New features will
amongst others include: gradient in typography / etching effect / print
dialogue. He also brings up Inkbook – decentralized, networked drawing
in .svg; interesting stuff.

Jakob Steiner demos Inkscapes' ability to produce photo realistic
illustrations, such as Motorola phones and shiny racing cars thanks to
blur, transparency and mask functions.

Of the more strange but fascinating kind: Igor Novikov and Valek
Philippov reverse engineer the Corel Draw image format. Their
hallucinating travels through endless bin-hex dumps show, that also for
them, structure reveals itself through transformation. Igor presents
CDR-Explorer, a tool to visualize binary formats for reverse engineering
purposes. The Explorer allows you to look at a raw file as if you look
at a folder structure, which facilitates the understanding of its
various building blocks.

Peter Linell starts his talk on Scribus with telling about his first
encounter with developer/initiator Franz Schmidt. He portrays Scribus as
a program built around robust + safe pdf-export. Linell goes in to
various new features, amongst which the ability to import layered
Photoshop images, and ways to process images inside Scribus – duotone
etc. can now be edited from inside the software. Apart from the ability
to add bleed and registration marks, he presents The Color Wheel,
including the ability to pre-visualise a document as seen by someone
with various flavors of colorblindness (this feature deserves its own
post!). Andreas Vox follows with the new lay-out system (NLS), in
Scribus, explaining the transition from old, convoluted code via a
'legacy mode' in to the New Lay-out System. Character styles, word
spacing and optical alignment are amongst new additions. In the near
future we can expect paragraph optimisation for hyphenation too.

[![orange.JPG]({filename}/images/uploads/orange.JPG)]({filename}/images/uploads/orange.JPG "orange.JPG"){: .float}The
evening is reserved for the Grand Souper LGM with live music and
convivial atmosphere in the very orange patio of the Ecole
Polytechnique.

The OSP presentation is planned early morning on **Saturday May 5**. We
speak about the relation between tools and what they produce; about
creativity and efficiency, using the tale of the frog that turned into a
prince (text and images follow!). The small audience that is present in
the lecture room early enough, seems pleased though slightly surprised
by our unorthodox approach.

In one of the classrooms reserved for LGM, Karin Delvare (The Gimp)
tours through different layers of participation in FLOSS projects and
explains amongst others the art of doing a bug report. In the main hall,
Hubert Figuiere speaks about how to organise images using metadata; his
presentation is concluded with a discussion about various
media​/supports for archiving.

Michael Terry from the University of Waterloo is involved in developing
InGimp. This software automatically archives userdata / behavior for the
purpose of usability studies. Interesting was that the system makes log
files automatically publicly available (leaving out any data that might
breach privacy), allowing developers to analyse incredible amounts of
ultra precise data on for example the amount of time spent on certain
tasks, patterns of access, combinations and successions of tasks. As a
gadget / feature the team developed a 'dynamic profile visualisation' –
basically a stick figure with representations of assumed tasks, of ways
of working.

[![cedric.JPG]({filename}/images/uploads/cedric.JPG)]({filename}/images/uploads/cedric.JPG "cedric.JPG"){: .float}Back
in the classroom, Cedric Gémy (~~responsible~~ contributor to Inkscape
and Gimp documentation) demonstrates with élan the possibilities of
using free standards and open source software, moving from Inkskape to
Blender to Gimp to Scribus; Boudewijn Rempt and Cyrille Berger show new
functionalities of Krita, a simple image editor with the ability to do
for example realistically behaving watercolour images (colours actually
blend!) and dry brushtrokes.

For the presentation of Alexandre Robin: *Our first year of Graphic
Design 100% Open Source*, [see this OSP
post](http://ospublish.constantvzw.org/?p=223).

[![spiro.JPG]({filename}/images/uploads/spiro.JPG)]({filename}/images/uploads/spiro.JPG "spiro.JPG"){: .float}Dave
Crossland (designer, lecturer) and Nicolas Spalinger (SIL) speak about
the Open Font License, and the way metadata on fonts could reflect these
licenses. They spend the second part of their presentation showing Spiro
which caused great enthusiasm in the audience.

[![linnell.JPG]({filename}/images/uploads/linnell.JPG){: .float}]({filename}/images/uploads/linnell.JPG "linnell.JPG")In
a lively Q +A session, Peter Linnell demonstrates the robustness of
Scribus in a prepress context, explaining the set-up of PDF export and
how 'self-defensive-code' is employed to please even the most
conservative printshop requirements. To his mind often faulty fonts are
the cause of output problems – Scribus therefore is ultra careful with
loaded fonts; it is the reason why a first start-up is slow; each font
is checked glyph by glyph before it is added to the list.

The day ends with a crisp presentation of Raphael Meltz, [Le
Tigre](http://www.le-tigre.net/), a French monthly magazine entirely
layed-out in Scribus. Raphael shows Le Tigres' work flow of both their
monthly print and daily pdf-publication; including a rudimentar
end-to-end solution between SPIP (a web based Content Management System)
and Scribus. Raphael was clear about their economical and philosophical
motivations for using FLOSS, without being shy about the possible
problems and difficulties, which had most of all to do with html
import-export issues.

At the end of the day we interview Nicolas Spalinger and Pierre-Luc
Auclair (contributor to the Deja-Vu font project). The evening FLOSS
font enthusiasts gather in Concordia University to watch
[Helvetica](http://www.helveticafilm.com), a full length documentary on
the modernist typeface premiering in Montreal.

**Sunday May 6** starts gently with a private tutorial on Inkscape and
Scribus by Cedric Gémy; we talk lay-out with Raphael Mertz.

The second presentation this weekend by Igor Novikov is even more
exciting, demonstrating SK1, a vector illustration programme,
specifically for prepress. SK1 offers solutions for many problems with
CMYK export we find in other programs. Their user interface is
refreshingly different and their set of sample images fantastic.
Discussing the history of the program (a fork of Skencil), shows that
the Ukraine context requires its own approach. That is more or less the
subject of the interview we do later with Igor and Valek.

Andy Fitzsimon impresses with his virtuosity in Inkscape (glassy icons
galore!) but most of all with his intelligent approach to the management
of translatable design elements across multiple languages. Making use of
.svg, he manages to employ the .po format (used in many translation
projects) to translate styled typography. We are also interested in his
use of the xml-editor as design tool.

Jon Phillips presents [The Open Clipart
Project](http://www.openclipart.org/), and most of all
[ccHost](http://wiki.creativecommons.org/CcHost), a sort of repository
system where images, sounds and other files licensed under Creative
Commons can be made available for download; making direct connections
from applications such as Inkscape to ccHost ('save to ccHost'). George
Williams (FontForge) was interested to add the possibility to direct
upload fonts from FontForge too.

[![bof.JPG]({filename}/images/uploads/bof.JPG){: .float}]({filename}/images/uploads/bof.JPG "bof.JPG")After
the conference is officially over, a BOF is (Birds Of Feathers:
developers gathering) organised on type management and its future.

About 10 developers involved in projects relevant to typography, gather
around the table for an intense session on standards, the workings of
fontconfig ... Exciting to witness how everyone seems to come up with
*real* proposals that will undoubtly alter the future of using fonts on
linux systems.

[![breakfast.JPG]({filename}/images/uploads/breakfast.JPG)]({filename}/images/uploads/breakfast.JPG "breakfast.JPG"){: .float}We
spend **Monday morning** interviewing Andreas Vox about Scribus and Igor
and Valek about SK1 and Ukrainian software culture; soon here on OSP.
