Title: OSP for VJ10
Date: 2007-10-17 16:01
Author: Harrisson
Tags: Tools, Works, Batik, Inkscape, SVG
Slug: osp-for-vj10
Status: published

[![vjx.png]({filename}/images/uploads/vjx.png)]({filename}/images/uploads/vjx.png "vjx.png")

OSP are currently working and testing hard for [Verbindingen - Jonction
10](http://www.constantvzw.com/vj10) festival, organised by meta
collaborators [Constant](http://www.constantvzw.com). Offset CYMK
Printout (5000 ex.) expected for next wednesday, with all the blurs,
transparencies, gradients and fonts...

While trying to export the svg from inkscape to pdf, we encountered few
problems with transparencies, and blur was completely ignored. We found
a solution with using the [batik SVG
toolset](http://xmlgraphics.apache.org/batik/) from Apache.  
<!--more-->  
Steps to reproduce:

1.  Download the batik SVG toolset from
    <http://xmlgraphics.apache.org/batik/#download>
2.  Let's assume you unpacked the batik toolset in a folder:
    `/home/you/batik`
3.  In the commandline, type: `cd /home/you/batik/batik-1.7` (this may
    vary depending on the version of batik you have downloaded)
4.  Let's assume you saved your svg file produced in inkscape here:
    `/home/you/yourfolder/yourfile.svg`
5.  In the commandline, type:
    `java -jar batik-rasterizer.jar -m application/pdf /home/you/yourfolder/yourfile.svg`
6.  This produces the file `/home/you/yourfolder/yourfile.pdf` with
    correct transparency and perfectly rendered blurs!

