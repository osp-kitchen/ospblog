Title: Summer of Code / Season of Usability
Date: 2008-03-20 12:11
Author: Femke
Tags: Tools, Scribus, Usability links
Slug: summer-of-code-season-of-usability
Status: published

Scribus is included in this year's **Google Summer of Code** and now
looking for contributions in the form of proposals and feedback to
already formulated ideas:
<http://wiki.scribus.net/index.php/GsoC_2008_Ideas>  
Scribus is also actively looking for student applications:  
<http://wiki.scribus.net/index.php/GsoC_2008_Example_proposal>

From the [Gnome Usability
list](http://www.mail-archive.com/usability@gnome.org/info.html):  
"**Season of Usability** is a series of mentored student projects to
encourage students of usability, user-interface design, and interaction
design to get involved with Free/Libre/Open-Source Software (FLOSS)
projects. It offers an excellent way to gain experience in the
interdisciplinary and collaborative development of user interface  
solutions in international software projects."  
<http://season.openusability.org/index.php/projects>
