Title: LaTeX Project
Date: 2006-03-17 15:31
Author: Harrisson
Tags: Tools, Type, LaTex, Retrospective Reading
Slug: latex-project
Status: published

From WORDS MADE FLESH  
Code, Culture, Imagination  
by Florian Cramer  
(p22)

The idea that beauty materializes in numerical proportions according to
mathematical laws continues to be popular in scientific and engineering
cultures, too. Since the early 1970s, Donald Knuth, widely considered
the founder of computer science as an independent academic discipline,
published his textbooks under the title *The Art of Computer
Programming*. He understands “art” as the formal beauty and logical
elegance of the source code. The software TeX which he wrote to typeset
his books correspondingly implements a classicist post-Renaissance
typography whose notions of beauty are embedded in Knuth’s algorithms
for line spacing and paragraph adjustment. At MIT, Knuth initiated a
*project God and computers* whose results were an exhibition of Bible
calligraphies and, in 2001, a book *Things a Computer Scientist Rarely
Talks About*. In this book, Knuth remembers how as a student he read a
computer program code that he found “absolutely beautiful. Reading it
was just like hearing a symphony.” This was how he “got into software,”
teaching it as an art rather than a science. <!--more-->The hacker credo
put down by Steven Levy in 1983 that “you can create art and beauty with
computers” has its roots in Knuth’s teaching. It ultimately means that a
program is not a transparent tool for creating beauty—like, for example,
a graphics program—, but that it is beautiful by itself. Both schools,
highbrow academic computer science and more underground hacker culture,
perpetuate a Pythagorean, classicist understanding of art as formal
beauty. This concept blatantly lags behind modern concepts of art. Since
romanticism and 20th century art, aesthetic understandings of art were
not just about beauty, but included the sublime, grotesque and ugly as
well. The same is true, implicitly at least, for the Greek and Roman
antiquity whose highest art form, tragedies, were about violence and
despair.

Download the text here:  
<http://pzwart.wdka.hro.nl/mdr/research/fcramer/wordsmadeflesh/>
