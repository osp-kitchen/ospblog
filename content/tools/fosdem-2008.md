Title: FOSDEM 2008
Date: 2008-02-23 20:26
Author: Femke
Tags: Tools, Standards + Formats
Slug: fosdem-2008
Status: published

![fosdem.jpg]({filename}/images/uploads/fosdem.jpg)  
This weekend the annual meeting of Free and Open Source Developers
([FOSDEM](http://www.fosdem.org/2008)) takes place in Brussels. As
usual, the ULB fills up with developers from all over Europe, discussing
large scale projects such as Gnome desktop, Mozilla, Xorg and PHP.
Unfortunately none of the talks addressed our usual working tools (we'll
see more of that in [LGM
2008](http://www.libregraphicsmeeting.org/2008/) coming May), but many
relevant issues were/are discussed.  
<!--more-->  
**Community and Code**

[![p1000880.JPG]({filename}/images/uploads/p1000880.JPG){: .float}]({filename}/images/uploads/p1000880.JPG "p1000880.JPG")It
is impressive how large scale software projects with thousands of
volunteers can function, and [Robert
Watson](http://www.fosdem.org/2008/schedule/speakers/robert+watson)
spoke about exactly that. Using the long history of developing FreeBSD
(30 years!) as a case study, he spoke about tools and approaches that
matter when keeping such a project alive. Interesting elements were the
use of parallel version control systems;
[Perforce](http://www.perforce.com/) allows for more flexible project
management and [CVS](http://www.nongnu.org/cvs/) keeps track of long
term development. He also discussed ways they resolve conflicts and the
joy of having some of the original programmers around - this might have
inspired their successful mentoring projects.

**Standards versus Patents**

[Pieter Hintjens](http://fosdem.org/2008/interview/pieter+hintjens)
introduced the term: 'captive standards' as a way to make understandable
the difference between proprietary standards such as
[OOXML](http://www.noooxml.org/), and let's say HTML. While a standard
might be public (i.e. the specifications are published), it can still be
patented and owned by a single company. The owner can than at any time
decide to make changes, retract, prevent others to implement or to make
users pay; this is what Hintjens means by a standard being *captive*.
With the current push by governments for open standards (recently for
example [in The
Netherlands](http://www.ez.nl/dsc?c=getobject&s=obj&objectid=154648)) it
is important to ensure that the difference between open standard and
captive standard remains clear.

**Women in FreeJava**

[![p1000893.JPG]({filename}/images/uploads/p1000893.JPG){: .float}]({filename}/images/uploads/p1000893.JPG "p1000893.JPG")In
the [FreeJava development
room](http://www.fosdem.org/2008/schedule/devroom/freejava), we were
introduced to [Duchess](http://jduchess.org/), an international network
of female Java Programmers. Asked to speak about their project and about
why they felt the need to start such a group, an interesting discussion
sparked off about why women are less present (especially in Free
Software!). It was interesting to witness a generally felt concern with
the lack of diversity in software projects, and at the same time
frustrating that it is so hard to speak about what could be done about
it. I think this studies (Maastricht Economic and social Research and
training centre on Innovation and Technology, 2006) might be a good
place to start: <http://www.flosspols.org/> and particularly
[Gender\_Integrated\_Report\_of\_Findings.pdf](http://www.flosspols.org/deliverables/FLOSSPOLS-D16-Gender_Integrated_Report_of_Findings.pdf)

**Sound Copyright**

[![p1000890.JPG]({filename}/images/uploads/p1000890.JPG){: .float}]({filename}/images/uploads/p1000890.JPG "p1000890.JPG")Although
[The Sound Copyright Campaign](http://soundcopyright.eu/) was introduced
in the left over minutes of one of the so-called 'lightning talks', and
it's url passed around on little handcut notes... the issue deserves
center stage. Now that *tracks from the first golden age of recorded
sound reach the end of their copyright term (50 years), recording
companies are trying to extend the lease. This just at the moment
seminal soul, reggae, and rock and roll recordings are about to be free
from legal restrictions, allowing anyone (including performers
themselves and their heirs) to preserve, reissue, and remix them*. Sign
the petition!
