Title: One thing leads to another
Date: 2010-07-11 13:36
Author: Femke
Tags: Tools, Type, animation, Inkscape, Libre Fonts
Slug: one-thing-leads-to-another
Status: published

[![]({filename}/images/uploads/list.png "list"){: .float }]({filename}/images/uploads/list.png)Following
a trackback, Alexandre discovered the work of Lafkon studio a few days
ago. Than, through Antonio Roberts' comment on this same post, I find
out about his work with animated fontfiles. Antonio writes:

"*Font files are files that attribute a style to the otherwise plain
text that we see on screen. The computer treats this only as an
attribute of the text and can understand it regardless of what font file
is used or how it looks to the viewer*"

<http://www.hellocatfood.com/2010/06/30/i-am-sitting-in-a-room/>

Exploring his blog further, I am intrigued by a series of
inkscape-animations, and learn they are based on the svgbuild script in
Ed Halley's Programmer's Notebook:

"*This script takes a SVG (scaleable vector graphics) file, and uses the
InkScape application to render each frame of a movie animation. If
viewed in sequence, a virtual "camera" is animated along a tour of the
image as it is constructed, entity by entity, from nothing up to the
final construction.*"

<http://halley.cc/code/?python/svgbuild.py>

Ed Halleys' notebook contains many more interesting things to play with.
buzz.py ("a phrase generator which assembles phrases from random
pieces") or strokes.py ("Routines for recognizing handwriting strokes
and gestures").

It will lead to something else one day.

<div class="clear">

</div>
