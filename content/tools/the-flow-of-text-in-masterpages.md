Title: The Flow of Text in MasterPages
Date: 2007-04-23 20:33
Author: Ivan
Tags: Tools, Python, Scribus
Slug: the-flow-of-text-in-masterpages
Status: published

The scripting in Scribus happens in the house of
[Python](http://python.org/). The Scribus module is loaded into scripts
with the standard *import scribus* or *from scribus import \**. In this
post, we'll be using only one function off this module:

`createText(x, y, width, height)`

This function puts a TextBox of the specified width and height at the
*(x, y)*-coordinates of your document.
[This](http://docs.scribus.net/index.php?lang=en&page=scripterapi-object)
is its documentation.

The TextBoxes of Scribus Templates are eerily insular. Since we really
didn't find a way to make text flow in Scribus MasterPages, we were
forced to write a script. It builds on a script that was posted some
time ago on the Scribus [mailing
list](http://nashi.altmuehlnet.de/pipermail/scribus/2005-December/014475.html).  
<!--more-->  
We have added an alternate to the Flow of Text that may be useful to the
users of the script. The TextFlow in the original is fine, but ours is
fine, too. To push the Text as a River metaphor a bit further: the text
must flow, and we're redirecting it with a Dam.

To see the difference, it's probably best to run the script itself.
Fetch the
[TextBoxes.py](http://pzwart2.wdka.hro.nl/~ilopez/TextBoxes.py) and
[Boxes.py](http://pzwart2.wdka.hro.nl/~ilopez/Boxes.py) files, and put
them in the same directory. Open Scribus and load a document. Things
will only work if there's already a Scribus document open.

`even = [[30, 30, 10, 10], [50, 50, 20, 20], [80, 80, 30, 30]]`  
`odd = [[30, 240, 10, 10], [50, 210, 20, 20], [80, 170, 30, 30]]`

The above are the lines in Boxes.py where you specify the size and
position of your TextBoxes. It uses the same units as your Already
opened Scribus document. Note that you can have different layouts for
odd and even pages. Once you've written a layout, execute TextBoxes.py
from the aptly named *Script* menu.

Boxes.py may be the only file that you will need to edit. It has a
couple of other *tweakable* parameters. Explore : )
