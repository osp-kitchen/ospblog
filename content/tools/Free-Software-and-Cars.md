﻿Title: Free Software and Cars
Date: 2009-08-12 22:55
Author: OSP
Tags: Tools, Culture of work, Free Software Community, Inkscape, Scribus, Tools

![iQ]({filename}/images/uploads/iQ1.png "iQ"){: .alignright .size-full .wp-image-3289 }  
In this recent Toyota campaign, a race-driver draws a font. And if you
watch the fast-paced 'making-of' video closely, it seems that Please
Make Me Design (a band of type-designers from Brussels) used Free
Software to produce it: FontForge and OpenLayers are used to create the
project.

![screenshot-inkscape]({filename}/images/uploads/screenshot-inkscape.png "screenshot-inkscape"){: .alignright .size-full .wp-image-3289 }

A few years ago, I started to collect
Free-Software-screenshots-featuring-cars. I was thinking about the
design of softwares, which means to somehow imagine how a tool will be
used, even before it exists. I started looking at the kinds of
screenshots F/LOSS graphics projects put out to advertise their
projects, because it might show something about the nature of the
practice that it intends to support. At that time, many of them featured
... cars. Confirming all the gendered cliché's of your typical software
developer, Scribus, Gimp and Inkscape had independently decided to
feature high powered, mean machines as the main subject to demo their
softwares with. Luckily, the collection never grew very large, and I am
starting to think there is a chance the F/LOSS community finally will is
coming to terms with its own blind spot vis-a-vis the lack of diversity
within.

![13674]({filename}/images/uploads/13674.jpg "13674"){: .alignright .size-full .wp-image-3289 }

"*There is no such thing as a perfect application, just as there are no
perfect automobiles, and the designers know this*" ((Gregory Pittman
compares the look-and-feel of Scribus to that of a Mercedes car. Scribus
mailinglist,
<http://lists.scribus.info/pipermail/scribus/2008-February/027866.html>))

To see Free Software being featured in a car-ad is an exciting
development; it hints at the possibility of those tools being
interesting and mature enough to appear on the radar of design agencies.
But it is also frustrating that designers + developers meet on the same
predictable representation of technology.
