Title: Figlet...Cowsay...
Date: 2008-08-25 00:13
Author: Yi
Tags: Tools, Command Line, How-to, Type
Slug: figletcowsay
Status: published

During the Polish Print Party, a scene performed silently behind the bar
counter...

they are about :

**===
[FIGlet]({filename}/images/uploads/fig01new.png)
==============================**

-   Go to Synaptic to install the Figlet package
-   Find the pre-installed fonts: <span
    style="color: #333333;">/usr/share/figlet</span>
-   <span style="color: #808080;"><span style="color: #000000;">To add
    more fonts:</span></span><span
    style="color: #333333;">/usr/share/figlet</span><span
    style="color: #000000;"> (</span>That is for all users. Otherwise,
    you have to put the new fonts in a directory of your choice by
    indicated with <span style="color: #808080;">-d</span><span
    style="color: #000000;">(directory))</span>

<span style="color: #000000;">*It will look like*
[this]({filename}/images/uploads/fig02-new.png)</span>

<span style="color: #333333;">yi@core2duo:\~\$ figlet -f banner it will
be looked like this</span>

*or like*
[this]({filename}/images/uploads/fig03-new.png)

<span style="color: #333333;">yi@core2duo:\~\$ figlet -d
/home/yi/figfonts -f isometric2 or like this</span>

Have fun with *F*rank, *I*van and *G*len's *let*ters!

===
[**Cowsay**]({filename}/images/uploads/cow01.png)
/ **Cowthink =========================  
**

-   find out [cow's home](http://www.nog.net/~tony/warez/cowsay.shtml)
    to start installation[  
   ](http://www.nog.net/~tony/warez/cowsay.shtml)
-   <span style="color: #333333;"><span
    style="color: #000000;">then</span>: sudo apt-get install
    cowsay</span>
-   open file: <span style="color: #333333;">.bashrc</span>
-   <span style="color: #000000;">add line</span>
-   <span style="color: #333333;">echo Bonjour \$USER</span>

*Sometimes it looks like*
[this]({filename}/images/uploads/cow02.png)

yi@core2duo:\~\$ cowsay -d \$M like this

*Sometimes it looks like*
[that]({filename}/images/uploads/cow03.png)

yi@core2duo:\~\$ cowsay -p \$M like that

Or
like[...]({filename}/images/uploads/cow04.png)

yi@core2duo:\~\$ cowsay -f dragon.cow like...

<span style="color: #333333;"><span style="color: #000000;">A vous de
jouer :)</span>  
</span>
