Title: It's possible?
Date: 2011-02-16 12:13
Author: OSP
Tags: Tools, Python, Scribus, Scripting, Watch this thread
Slug: its-possible
Status: published

The following question was posted by Pascal Lapalme on the Scribus
mailinglist:

<div style="font-family:mono">

"I will animate a conf to explain how to use Scribus, Gimp and Inkscape
in a classroom with studient to make a news paper (look here : Atelier
108 - Des outils libres pour la création d'un journal
<http://colloque.aquops.qc.ca/cgi-bin/WebObjects/colloque.woa/wo/0.1.29.1.0.7.0.8>).
In my conf, I will ask everyone to use twitter to make comments, ask
questions, etc.  
I would like to use Scribus and his hability to script to take RSS file
and make a booklet of all this posts...  
It's possible ?  
How can I make it or where do I begin my search in the doc ?"

</div>

We are curious to see the result!  
Follow the thread here:
<http://lists.scribus.info/pipermail/scribus/2011-February/041959.html>
