Title: Proprietary formats, tips and tricks: stuffit and psd.
Date: 2007-04-28 11:11
Author: nicolas
Tags: Tools, Command Line, How-to, Standards + Formats
Slug: proprietary-formats-tips-and-tricks-stuffit-and-psd
Status: draft

Graphic designers send me often big email attachments which are
compressed by the Aladin software Stuffit. And if I find a way to open
them, they usually contain files that are saved in the internal
photoshop format PSD. Here follow a few tips to work around the
problems.

Imagine, I receive an attachment called ImageFolder.sit that contains a
compressed folder of 100 images named Image1.psd, Image2.psd,
Image3.psd, ... Img100.psd.

First how to open .sit file... Linux distributions like Ubuntu let you
install a package call macutils. Once installed you can run commands
such as:  
Which do not always work. But there is still hope, eventhough it is not
free software. Alladin releases a version of Stuffit Expander for Linux.
Once installed, it lets you run commands like:

In my experience, most of the time, these softwares succeed to
uncompress stuffed files. Most of the time, unfortunately means that on
rare occasions, you end up with error messages. The only solution then
is to try and convince your colleague designers to at least consider
compress their files with the zip utility that MacOSX integrates in the
system's browser.

Second, I want to convert easily the PSD files into jpg to integrate
them into webpages. I will use the Imagemagick program called convert to
flatten the layers, preserve the quality of the images and saved them
into the jpeg format while resizing them at 20% of their size.
