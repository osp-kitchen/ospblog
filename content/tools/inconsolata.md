Title: Inconsolata
Date: 2007-07-24 12:06
Author: Femke
Tags: Tools, Type, Fontforge, Libre Fonts, Scribus
Slug: inconsolata
Status: published

One of our [Rotterdam
reporters](http://pzwart.wdka.hro.nl/mdma/staff/jjfcramer/) made us
notice
[Inconsolata](http://www.levien.com/type/myfonts/inconsolata.html), a
monospaced font designed by Ghostscript maintainer [Raph
Levien](http://levien.com/). Levien offers an OTF version, plus 'raw'
fontforge files on his webpage.

> First and foremost, Inconsolata is a humanist sans design. I strove
> for the clarity and clean lines of Adrian Frutiger's Avenir (the
> lowercase "a", in particular, pays homage to this wonderful design),
> but also looked to Morris Fuller Benton's Franklin Gothic family for
> guidance on some of my favorite glyphs, such as lowercase "g" and "S",
> and, most especially, the numerals. <small>(Ralph Levien)</small>

Dutch sample text generated in Scribus:

[![sample.jpg]({filename}/images/uploads/sample_th.jpg)]({filename}/images/uploads/sample1.jpg "sample.jpg")

Scribus uses the [Gutenberg text version of Multatuli's Max
Havelaar](http://www.gutenberg.org/etext/11024). The English default is
Bram Stoker's Dracula. Of course you could select your own preferred
source!
