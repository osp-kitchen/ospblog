Title: Page tiling with poster
Date: 2007-11-06 18:09
Author: Femke
Tags: Tools, Command Line, How-to, Printing + Publishing
Slug: page-tiling-with-poster
Status: published

![]({filename}/images/uploads/correct.jpg)  
<small>Virginie and Laurence checking the
[V/J10](http://www.constantvzw.com) program in real size.</small>

**[Poster](http://www.ctan.org/tex-archive/support/poster/poster.txt)**
is an excellent tool to print .eps or .ps documents in tiles. You can
adjust the final size of the file, the amount of overlap, size of the
media you print on, work from a percentage ('enlarge 500%') etc. Install
poster through Synaptic package manager (Debian / Ubuntu) or download
[here](http://www.geocities.com/SiliconValley/5682/poster.zip).

To print an A2 file on A4 sheets, you need to type this line in your
terminal (all in one line):

`$ poster -ma4 -ia2 -pa2 -v /home/yourfolder/file.eps > /home/yourfolder/tiles.eps`

![]({filename}/images/uploads/tiles.jpg){: .float}Poster
will than produce a 9 page .eps document on A4 size with crop marks and
cutting marks.

Only downside is that the resulting file is very large; in fact 9 times
as large as the original file.

<div class="clear">

</div>
