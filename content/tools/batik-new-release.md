Title: Batik, new release
Date: 2008-01-17 17:44
Author: nicolas
Tags: Tools, Batik, Command Line, SVG
Slug: batik-new-release
Status: published

The Apache Foundation has released a new version of Batik.

> Batik is a Java-based toolkit for applications or applets that want to
> use images in the Scalable Vector Graphics (SVG) format for various
> purposes, such as display, generation or manipulation.

![Using Batik’s thumbnail function to navigate a complex SVG
document]({filename}/images/uploads/batik.png)  
<small>Using Batik’s thumbnail function to navigate a complex SVG
document</small>

Batik is not only relevant for java developers. It also contains various
tools that can help in converting svg files into other formats ([see
how](http://ospublish.constantvzw.org/?p=350) we used it to convert the
svg produced with Inkscape into a pdf), [encode ttf
fonts](http://xmlgraphics.apache.org/batik/tools/font-converter.html) in
svg font format or simply view an svg document in a browser (batik ships
with the [squiggle
browser](http://xmlgraphics.apache.org/batik/tools/browser.html)which is
the most reliable svg viewer for the moment)  
Last but not least, Batik's latest release now supports many features
related to animation.

[Download batik
here](http://xmlgraphics.apache.org/batik/download.cgi)and enjoy!
