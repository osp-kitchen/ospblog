Title: mybadge.png
Date: 2010-02-27 19:00
Author: OSP
Tags: Tools, How-to, LGM 2010, Manual
Slug: mybadge-png
Status: published

[![]({filename}/images/uploads/mybadge2.png "mybadge"){: .alignleft .size-full .wp-image-4124 }](http://rw.stdin.fr/CookBook/Pledgie) [Alexandre
Leray](http://stdin.fr/) has written a nice tutorial on how to create
your own pledgie-badge: <http://rw.stdin.fr/CookBook/Pledgie>

Thank you [ginger coons](http://adaptstudio.ca/blog) for your tasty
habanero pepper and don't forget to **donate to
<http://pledgie.com/campaigns/8926>**!
