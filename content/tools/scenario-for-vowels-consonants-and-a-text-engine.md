Title: Scenario for vowels, consonants and a text-engine
Date: 2010-11-23 21:48
Author: OSP
Tags: Tools, Type, Scribus, Scribus Re-loaded, typography
Slug: scenario-for-vowels-consonants-and-a-text-engine
Status: published

Imagine ((This *Scenario for vowels, consonants and a text-engine*
describes the sophisticated new text-engine that the Scribus team is
preparing us at this very moment)) a
[paragraph]({filename}/images/uploads/whathappenswhenyoubakeacake.html)
in Scribus. The first word consists of three vowels and a consonant. The
Scribus *text-engine* receives this *cluster* of characters as a first
input. Now the *shaper* will start to work; it has to decide what
features need to be applied to the cluster. Do characters need to be
re-ordered? Do ligatures need to be applied? If the answer is 'yes', the
shaper will ask the font-software if there is a ligature available that
can be used. Each cluster is treated in this way and subsequently added
to the list of *glyphs*. This list of glyphs output by the shaper is
than entered into the second part of the text-engine, the
*block-compositor*. The block-compositor is in contact with the
*line-lay-outer*. It knows that a certain line starts at a given point
and it also knows about the width of the frame containing it. The
block-compositor will ask the line-lay-outer to come up with a certain
amount of propositions per line. Per proposition, a penalty is given,
based on the quality of the break (does it require hyphenation?) and the
amount of stretching needed to reach the width of the frame. Now the
block-composer can produce an oriented graph
((http://mathworld.wolfram.com/OrientedGraph.html)) on the basis of
which it can decide what is the least penalized route ((Compare the
concept of badness in TeX:
http://en.wikibooks.org/wiki/TeX/definition/badness))). But a second
parameter is also taken into account: what combination of
line-propositions will result in the most uniform distribution of
line-width per paragraph? After the block-compositor has found the
optimal solution, the text engine outputs the paragraph to the canvas.
