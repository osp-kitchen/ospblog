Title: Inkscape plugins in Python
Date: 2008-02-17 22:39
Author: Michael
Tags: Tools, Inkscape, Scripting, SVG
Slug: inkscape-plugins-in-python
Status: published

[![circles.png]({filename}/images/uploads/circles.png)]({filename}/images/uploads/circles.png "circles.png")[]({filename}/images/uploads/circles.zip "circles.zip")

Inkscape allows python scripts to be used as effects plugins. In a
nutshell: you use the [DOM](http://developer.mozilla.org/en/docs/DOM) to
create / manipulate the structure of the SVG document and use CSS
properties to style -- so there's quite some overlap with "regular" CGI
& web programming.

This example (circles) is based on the example given on the [Inkscape
wiki](http://wiki.inkscape.org/wiki/index.php/PythonEffectTutorial). I
started by making a simple Inkscape file (with a single circle), saving
the file, then opened it in a text editor to view the "raw" XML
structure of the SVG. I used this as a guide for what the code needed to
produce.

[circles.zip]({filename}/images/uploads/circles.zip "circles.zip")
