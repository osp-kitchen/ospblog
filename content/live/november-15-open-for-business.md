Title: November 15: nm-X
Date: 2007-10-14 16:10
Author: OSP
Tags: Live, London, Presentations
Slug: november-15-open-for-business
Status: published

[![nmx.jpg]({filename}/images/uploads/nmx.jpg){: .float}](http://nm-x.com/event/2007/11/open-source-publishing-briefing)
"*What we make is often defined by what tools we can use to make it*" is
the motto of an Open Source Publishing event coming up in London.

OSP will assist Simon Worthington and Laura Oldenburg (Mute) at this
event, which is hosted by Central St. Martin's College of Art and
Design.

<div class="clear">

</div>

<http://nm-x.com/event/2007/11/open-source-publishing-briefing>  
<!--more-->

> "What we make is often defined by what tools we can use to make it.
> This briefing introduces designers and publishers who are moving
> beyond the limited set of possibilities offered commercially available
> publishing tools.
>
> "We will look at three key tools: 'The Gimp' (an open source bitmap
> graphics tool, like Adobe Photoshop), 'Inkscape' (an open source
> vector illustration tool, like Adobe Illustrator), and Scribus – an
> open source layout tool like Quark Express or Adobe InDesign.
>
> But besides specific tools, this briefing moves beyond outmoded
> boundaries in the publishing process between writing, designing, and
> printing: between the page and the 'Net. Attendees will hear from
> presenters how dispensing with these boundaries creates immense
> creative and business opportunities for cross-format experimentation
> in the future of print and publishing.
>
> This briefing will be presented by Simon Worthington and Laura
> Oldenbourg of Mute Magazine and Openmute."
