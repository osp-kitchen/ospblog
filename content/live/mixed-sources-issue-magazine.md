Title: Mixed Sources: Issue Magazine
Date: 2008-03-13 12:52
Author: OSP
Tags: Live, News, Printing + Publishing, Reading list
Slug: mixed-sources-issue-magazine
Status: published

[![sources.jpg]({filename}/images/uploads/sources.jpg)](http://mixedsources.tk/)  
Issue Magazine launch: **Friday March 21 18:00 - 19:30**  
Log on via: <http://mixedsources.tk/>

For the launch of [Issue Magazine](http://www.issue-magazine.net/),
Stéphanie Vilayphiou and Alexandre Leray are hosting an on line round
table with Arie Altena, Jouke Kleerebezem and Harrisson.
