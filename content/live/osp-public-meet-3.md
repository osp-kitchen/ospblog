Title: OSP Public Meet #3
Date: 2012-04-17 12:54
Author: Eric
Tags: Live, News, OSP Public Meets, Works
Slug: osp-public-meet-3
Status: published

The next Osp Public Meet will take place on Monday the 23rd. For this
third edition, we will try to precise our exhibitionist experiment of
talking internal affairs in public. Here below are the few code lines of
the new version.

[![]({filename}/images/uploads/Screenshot-at-2012-04-17-124954.png "Screenshot at 2012-04-17 12:49:54"){: .aligncenter .size-medium .wp-image-7129 }]({filename}/images/uploads/Screenshot-at-2012-04-17-124954.png)  
*Thank you Sarah Magnan who was with us the past two weeks!*

**18h00** ¬ This is an OSP meet. It's a regular meeting, it is not
presentations (let's repeat it : normally these meetings are internal).
We have all trivia to discuss, but these trivia bring us to discuss
things that seems to be more broad and could be interesting. A lot of
people ask us what is OSP and how we work, and one day we thought that
maybe a way to enter in contact with us is this strange operation of
doing an internal stuff in public. It could be frustrating, but we've
decided to push it further than the presentations we've done in the two
previous public meets. Read the ordre du jour, come, take a seat, don't
expect to understand everything, but we hope that some stuff could
interest you. The hardcore core! We smoke!  
As ordre du jour, keypoints in the agenda, some current jobs such as
Danslab, Stuttgart workshop, osp website and server, aether9
publication, OSP summer school, Relais Culture Europe, Spion and
[Link](http://link.hfk-bremen.de "Link").

**19h30** ¬ React to what you've seen/heard, bring some projects that
you want to show us, we present some stuff we want to share with you
(and between us), we discuss. Table ronde! And we drink beer, bring
some! We continue to smoke!

**21h00** ¬ We eat something, bring some food and more to drink. We
continue to discuss, but the music -free- begin with some volume. And
dance!
