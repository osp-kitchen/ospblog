Title: Semiotics of the Kitchen Radio (update)
Date: 2008-11-12 10:00
Author: OSP
Tags: Live, News, Command Line, Cooking, Culture of work, Radio
Slug: semiotics-of-the-kitchen-radio
Status: published

**Saturday November 15, 15:00** (doors open: 14:30)  
Live broadcast from FoAM, Koolmijnenkaai 30-34 Brussels  
Listen on-line: <http://www.constantvzw.be:8000/kitchen.ogg>  
<small>Emission en Français + English</small>

***Studio audience welcome!***

As part of the ongoing series [Verbindingen/Jonctions
11](http://data.constantvzw.org/site/spip.php?article395), OSP prepares
a live radio broadcast in the [FoAM Open
Kitchen](http://fo.am/open_kitchen). Experimenting with the displacement
of multi sensory experiences, we'll think out loud about the preparation
and distribution of food as metaphor, structure, vocabulary and rhythm.

![]({filename}/images/uploads/knife1.jpg "knife1"){: width="100" height="93"}
![]({filename}/images/uploads/cake-logo1.png "cake-logo1"){: width="100" height="93"}
![]({filename}/images/uploads/pppick1.jpg "pppick1"){: width="100" height="93"}
![]({filename}/images/uploads/xxx-cube_cola_land_medlge.jpg "xxx-cube_cola_land_medlge"){: width="100" height="93"}

<!--more-->  
[Download playlist
here]({filename}/images/uploads/conduite.pdf)

Yi Jiang serves us black and green tea with fitting accompaniments while
we listen to: an interview with artist Kate Rich about cooking and
trade, a conversation with Nicolas Malevé about Cake PHP frameworks and
other recipes, a reportage on Colruyt computer systems, Christophe
Piette (R.O.T) playing kitchen sounds, Ivan Monroy Lopez performing his
essay on man pages and manuals, a text-to-speech cookbook, pickles from
Anderlecht, 'Comédie de moeurs' (readings by Rachelle Sassi) and of
course a mouthwatering playlist.

<small>Martha Rosler, *Semiotics of the Kitchen*, 7" (1975), CakePHP
logo, Pickles et Frères, Pickles from Anderlecht (2008), Kate Rich &
Kayle Brandon, *Cube-Cola Poster Land* (2006)</small>
