Title: Print Party 2.01 at Université Attac
Date: 2006-09-12 20:42
Author: Femke
Tags: Live, News, Print Party, Printing + Publishing
Slug: print-party-201-at-universite-attac
Status: published

**Conditions of production and cultural consumption**  
After Nicolas gave a brief history of how author rights have developed
and how the free software and copyleft movements responded to its
increasing restrictive use...

![nicolas\_speaks.JPG]({filename}/images/uploads/nicolas_speaks.JPG){: #image110}

...while trying to be not to grim about the way copyright laws are
currently used and abused...

![votez.JPG]({filename}/images/uploads/votez.JPG){: #image110}  
<small>This poster by Act Up (questioning the geneology of ideas put
forward by French conservative politician Nicolas Sarkozy) was banned,
because photographers' rights were supposedly violated.</small>

...Harrisson and Femke offered the audience a small tasting of
publishing, designing and printing with Open Source tools.

![talking.JPG]({filename}/images/uploads/talking.JPG){: #image110}  
<!--more-->  
We used the opportunity to launch **Retrospective Readings** with a
small edition of Richard Stallman's GNU Manifesto - twenty years later
still worth (re-)reading.

![propaganda]({filename}/images/uploads/propaganda.JPG)
