Title: Dingbat Liberation Fest II
Date: 2009-12-04 18:13
Author: OSP
Tags: Live, dingbats, Print Party, Standards + Formats, Unicode
Slug: dingbat-liberation-fest-ii
Status: published

### Friday December 11

Location: [CASCO, Office for Art, Design and
Theory](http://www.cascoprojects.org/) Utrecht (NL)

☔ ✈ ❀ ☺ ❍ ✌ ☺ ✈ ❉
=================

\[caption id="attachment\_3675" align="alignright" width="400"
caption="Marzipan production at the Dingbat Liberation Fest
I"\][![Marzipan production at the Dingbat Liberation Fest
I]({filename}/images/uploads/marzipan.jpg "marzipan"){: .size-medium .wp-image-3675 }]({filename}/images/uploads/marzipan.jpg)\[/caption\]Next
week, OSP is happy to re-play [Dingbat
kitchen](http://constantvzw.org/vj12/spip.php?article35) with students
from the [Arnhem Academy of Art and
Design](http://www.artez.nl/mediagraphicdesign).

"*Dingbats have sneaked into most computers nowadays. They're as much a
part of the Unicode Standard as the Roman Alphabet is. Dingbats are up
for grabs; it’s just a matter of locating them on your harddisk. The
font projects OSP develops and the dingbats they sample, function in a
something that could perhaps be called a Commons. But how can we really
get our hands on them? Let's find out and liberate (y)our dingbats!*"  
<!--more-->  
**Ingredients**

- 800 grams ground almonds  
- 800 grams powdered sugar  
- a bit of water  
- almond essence  
- rolling pin  
- 1 large bowl  
- 1 large spoon  
- grease proof paper  
- multiple installs of Inkscape  
- cookie cutters  
- a beamer (preferably 2 that can project side by side)  
- an internet connection  
- natural food coloring (3 different colors)  
- a complete overview of the Unicode standard  
- a complete set of Unicode numbers 2700–27BF including their
descriptions, printed on paper slips and folded inwards  
- 200 sheets of black paper (A4)  
- 200 sheets of white paper (A4)  
- 15 cutters  
- glue  
- 2 serving trays or 3 large plates  
- tape  
- 3 digital cameras  
- cardboard or cutting mats
