Title: Exhibition Prix Fernand Baudin
Date: 2010-02-08 09:57
Author: OSP
Tags: Live, News, Awards, books, Printing + Publishing
Slug: exhibition-prix-fernand-baudin-prijs
Status: published

### 26 February – 10 March 2010

![]({filename}/images/uploads/baudin_arrow1.png "baudin_arrow"){: .alignleft .size-medium .wp-image-3910 }  
Exhibition in Brussels presenting the bookawards and nominations of the
Fernand Baudin Prize 2009, the prize of the [Most Beautiful
Book](http://ospublish.constantvzw.org/news/osp-wins-fernand-baudin-prize)s
in Brussels and Wallonia.

BIP  
Rue Royale, 2-4  
1000 Brussels

Opening, award ceremony of the honorary diploma’s + release of the
catalogue: **25th of February 2010 at 6 pm**

<!--more-->  
For it’s second edition 2009-2010, the Prize was extended to the Walloon
Region thanks to the support of Wallonia-Brussels International (WBI)
and the Walloon Agency for Export and Foreign Investments (AWEX).

The international jury convened at the Erg to choose the most beautiful
books that were designed, published or printed in Brussels and Wallonia
in 2009.

The jury consisted of the following six members:  
Roger WILLEMS, graphic artist, publisher, chairman of the Jury (NL)  
Willem OOREBEEK, artist, teacher (NL)  
Jean-Marie COURANT, graphic artist, curator, teacher (F)  
David POULLARD, typographer, teacher (F)  
Jan WOUTER HELSPEEL, graphic artist, teacher (B)  
Drita KOTAJI, book historian, teacher (B)

The 83 received books, presented by category (general literature
(fiction), general literature (non-fiction), artists’ books, art books
and catalogues, architecture and urban planning books, books for young
people and comic books) were placed at the disposal of the jury.

20 books were nominated and the Prize was awarded to 10 books, that were
given the title “Winner of the Fernand Baudin Prize 2009”. The three
parties of the award winning book; the graphic designer, the publisher
and the printer, will receive an honorary diploma.
