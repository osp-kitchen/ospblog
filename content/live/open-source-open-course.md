Title: OSP + Open Source / Open Course
Date: 2009-03-24 18:23
Author: OSP
Tags: Live, News, Print Party
Slug: open-source-open-course
Status: published

### mardi 31 mars 2009 @ ERG, Bruxelles

[![2412606153\_62d4f8c1d5]({filename}/images/uploads/2412606153_62d4f8c1d5.jpg "2412606153_62d4f8c1d5"){: .alignright .size-full .wp-image-2295 }](http://www.multimedialab.be/blog/?p=1204)

**Open Course / Open Source : une journée d’information et de rencontre
sur le logiciel libre dans le domaine de l’art. Présentations, partage
d’expériences, print-party and more ...**

Auditoire P7 • Erg, 87, Rue du page, 1050 Bruxelles  
<http://www.multimedialab.be/blog/?p=1204>

<!--more-->  
De 09h30 à 12h00 :  
Marc Wathieu : Open Source, une introduction.  
Durée : +/-20 min  
Web : <http://www.multimedialab.be/>

Lionel Maes : Présentation de son projet «Homeostatic»  
Durée : +/- 60 min  
Web : <http://www.psykolio.com/flux/>

Sébastien Denooz et Stéphane Noël : Dernières nouvelles de la planète
SPIP  
Durée : +/- 60 min  
Web : <http://www.workplace.lescorsaires.be/an12/>

Harrisson : Présentation du travail de OSP sur le livre «CROSS-over»  
Web
:[http://ospublish.constantvzw.org/works/kanttekeningen-sidemarks](%20http://ospublish.constantvzw.org/works/kanttekeningen-sidemarks)  
Durée : +/- 60 min

De 14h00 à 17h00 :

Open Source Publishing : Re-Print Party (cuisine, mise en page et
impression)  
Web :
[http://ospublish.constantvzw.org/](%20http://ospublish.constantvzw.org/works/kanttekeningen-sidemarks)
