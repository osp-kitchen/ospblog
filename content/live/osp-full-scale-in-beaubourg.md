Title: OSP full scale in Beaubourg
Date: 2009-03-18 19:31
Author: Harrisson
Tags: Live, News, OSP-DIN, Works, Culture of work, Design philosopy, DIN, Inkscape, Libre Fonts, Map, Tools, Type
Slug: osp-full-scale-in-beaubourg
Status: published

![install]({filename}/images/uploads/install.jpg "install"){: .alignleft .size-full .wp-image-2209 }

As a side effect of the BPI/cinéma du réel festival, OSP has been asked
to "perform" the program map in full scale in the Centre Georges
Pompidou main hall, where the festival occurs.  
The map is a derivative from the printed one, folded into the brochure.  
It networks the festival selected movies and the subjective links
between them.  
<!--more-->  
**FONTS**

![din]({filename}/images/uploads/din.png "din"){: .alignleft .size-full .wp-image-2209 }  
DIN  
As the Centre Pompidou graphic chart is very restrictive, we had to use
the DIN font. But impossible to use the FF DIN from fontshop, licensed.
So we encoded the first cut of the open source DIN, from drawing of 1932
we get in Berlin in our previous adventures. Only capitals yet, but work
is in progress.

![atlas]({filename}/images/uploads/atlas.png "atlas"){: .alignleft .size-full .wp-image-2209 }  
ATLAS  
For the printed map, we used a font Harrisson designed few years ago:
the Atlas font, with country shapes instead of letters as glyphs. This
font will be soon released with OFL license. We'll tell you the inner
story of this font later.

**MONTAGE**

![montage\_a4]({filename}/images/uploads/montage_a4.jpg "montage_a4"){: .alignleft .size-full .wp-image-2209 }  
The re-drawing of our file "en dur" was a very strong and singular
experience. We had the feeling of being into the file, and
re-interpreting SVG code

![montage\_a42]({filename}/images/uploads/montage_a42.jpg "montage_a42"){: .alignleft .size-full .wp-image-2209 }  
It was like crossing the screen, and performing the choreography of the
interface is a nice journey! The installation is the translation of the
file into an object, the transformation of vectors into movements.

![collaborateurs]({filename}/images/uploads/collaborateurs.jpg "collaborateurs"){: .alignleft .size-full .wp-image-2209 }  
It is a new interpretation, a new version. It's the gap between the
mouseclick and the gesture by editing on another support. We felt the
distance and the similarities of the interface choreography and the
gesture choreography.  
A new articulation...

![yeux\_digit]({filename}/images/uploads/yeux_digit.png "yeux_digit"){: .alignleft .size-full .wp-image-2209 }

![yeux\_dur]({filename}/images/uploads/yeux_dur.jpg "yeux_dur"){: .alignleft .size-full .wp-image-2209 }

![torodigit]({filename}/images/uploads/torodigit.png "torodigit"){: .alignleft .size-full .wp-image-2209 }

![toro\_dur]({filename}/images/uploads/toro_dur.jpg "toro_dur"){: .alignleft .size-full .wp-image-2209 }  
It becomes something human again.

**FIX**

![plan]({filename}/images/uploads/plan.jpg "plan"){: .alignleft .size-full .wp-image-2209 }  
Collaborators of this piece were convinced by the idea and came to help.
Among them, FIX, a Paris renowned graffiti artist. Graffiti crosses our
problematics in many fields.  
First he tried to follow the original computer file to the maximum, but
result was poor: all sensibility and spontaneity in the marking the
piece was disappearing. Paradoxical situation for a tagger...  
It was important that he could take the work for himself, and not being
just an executant... So we simply redefined space of movement for him to
translate the vectors with his feelings.

![fix]({filename}/images/uploads/fix.jpg "fix"){: .alignleft .size-full .wp-image-2209 }  
He could then confront his drawing and calligraphy experience to a new
tool for him: 3M 471 model tape. And adapt his habits and talent. He
found new ways to use it and we could all benefit from it.

The success of this job was to share a space of translation. And the
interpretation gives a lively vibration to the general aspect of the
installation.

It's an articulation.

![game]({filename}/images/uploads/game.jpg "game"){: .alignleft .size-full .wp-image-2209 }  
With FLOSS, the resistance of the tool is now for us such a daily meal,
that it has become a work field, an investigation space, and a
playground.
