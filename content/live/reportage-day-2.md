Title: Reportage : day 2
Date: 2009-05-08 06:29
Author: OSP
Tags: Live, News, LGM 2009
Slug: reportage-day-2
Status: published

This foggy fog second day of LGM starts in the run to arrive on time for
[our talk](http://ospublish.constantvzw.org/image/?level=album&id=30).

In the [middle of the
day](http://www.libregraphicsmeeting.org/2009/program.php), [Ginger
Coons](http://adaptstudio.ca/) put the fluid into the audience, and his
very vigorous talk bring a rare momentum of open and deep discussion at
LGM. So just before eating, we have [a more clear
picture](http://ospublish.constantvzw.org/image/?level=album&id=29) of
the gradient of opinions on how to try to push publishing floss farther.

At the type workshop of the mid-day, we follow the steps to install
always more fresh [Fontmatrix](http://fontmatrix.net/) and discover how
to theme Fontforge (some OSP manuals to come) with full-of-energy
Nicolas Spalinger and Pierre Marchand.

*"Les utilisateurs de logiciels libres ne les utilisent pas, ils s'en
servent."* — Pierre Marchand, LGM 2009, Montréal, in the corridors

[![img\_0163]({filename}/images/uploads/img_0163.jpg "img_0163"){: .alignleft .size-medium .wp-image-2579 }]({filename}/images/uploads/img_0163.jpg)
