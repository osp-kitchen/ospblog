Title: A beautiful compliment
Date: 2010-02-27 11:00
Author: OSP
Tags: Live, Awards, books, LaTex, Scribus
Slug: a-beautiful-compliment
Status: published

[![](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=30957&g2_serialNumber=2){: .alignnone }![](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=30969&g2_serialNumber=2){: .alignnone }![](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=31023&g2_serialNumber=2){: .alignnone }![](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=31044&g2_serialNumber=2){: .alignnone }](http://gallery.constantvzw.org/main.php?g2_itemId=30910)

"*Livre de texte dense, a priori sans soucis de graphisme, et je
découvre que c'est tout l'inverse.*" (("*Book of dense text, a priori
without concern about graphic design, and I discover that it's quite the
opposite*" Ariane Bosshard in: Prix Fernand Baudin: les plus beaux
livres à Bruxelles et en Wallonie 2009))

More pictures: <http://gallery.constantvzw.org/main.php?g2_itemId=30910>
