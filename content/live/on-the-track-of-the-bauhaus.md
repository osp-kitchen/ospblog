Title: on the track of the Bauhaus
Date: 2010-06-05 09:32
Author: OSP
Tags: Live, News, Presentations
Slug: on-the-track-of-the-bauhaus
Status: published

Yesterday, OSP was on a road trip to Weimar to give a talk at the
[Typogravieh Lebt](http://typogravieh-lebt.de) symposium organized by
the Bauhaus-university of Weimar.

![]({filename}/images/uploads/P1040574.jpg "P1040574"){: .alignleft .size-medium .wp-image-4554 }

![]({filename}/images/uploads/P1040660.jpg "P1040660"){: .alignleft .size-medium .wp-image-4554 }

We are now at the venue, about to start... It's going to be great!
