Title: ☔☺☔ ✈ ❍ ✌
Date: 2009-11-16 11:01
Author: OSP
Tags: Live, dingbats, Print Party, Type
Slug: dingbat-liberation-fest-populated-fonts
Status: published

Thursday Jeudi 26/11
--------------------

### Dingbat Liberation Fest + Populated Fonts

**@ Verbindingen/Jonctions 12: *[By Data We
Mean](http://www.constantvzw.org/vj12)***  
[Zennestraat 17 Rue de la Senne, Brussels
Bruxelles](http://constantvzw.org/vj12/spip.php?article16)

<!--more-->

### Dingbat Liberation Fest

14:00 &gt; 17:00

\[EN\] Dingbat kitchen! The fonts OSP started from and the dingbats that
they sample come from a something that could perhaps be called a
Commons. Dingbats have sneaked into most computers nowadays. They’re as
much a part of the Unicode Standard as the Roman Alphabet. Dingbats are
there for grabbing. It’s just a matter of locating them in your hard
disk.

\[FR\] Un atelier cuisine autour des dingbats communs. Les polices de
caractères et dingbats à partir desquelles OSP a travaillé, proviennent
d’un échantillon de quelque chose qui pourrait être appelé un bien
commun. Ces symboles se sont infiltrés dans la plupart de nos
ordinateurs. Ils font partie de la norme Unicode comme l’alphabet
romain. Reste à les localiser sur votre disque dur.

### Populated Fonts

17:00 &gt; 18:00

\[EN\] A presentation of the typographic design process that connects
the Cimatics platform identity with Verbindingen/Jonctions 12: a
development and extension of a pictures/glyph translation process.
Around dingbats and into the em, Ivan et Ludi will present an attempt
(crack) to develop images contained into data and Unicode standards (or
its negative). The Showery Weather, the Have a Nice Day!, and the black
blocks of the Unicode Standard are circumventing the old AsciiArt engine
to generate matrix landscapes populated by text.

\[FR\] Présentation du design typographique qui relie l’identité de
Verbindingen/Jonctions 12 au graphisme de la platforme Cimatics: le
développement et l'extension d’un processus de traduction image/glyphe.
Autour du dingbats, Ivan et Ludi présentent une tentative de révéler des
images cachées ou contenues dans les données et les standards. Une bande
de pictogrammes "temps de pluie", un smiley et quelques blocs noirs du
standard Unicode contournent le vieux moteur AsciiArt pour générer des
paysages matriciels, peuplés de texte.

<small>In collaboration with En collaboration avec le Festival Cimatics
2009</small>
