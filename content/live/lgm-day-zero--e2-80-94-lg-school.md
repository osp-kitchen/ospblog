Title: LGM Day zero — LG-school
Date: 2010-05-26 23:12
Author: OSP
Tags: Live, News, LGM, LGM 2010
Slug: lgm-day-zero-%e2%80%94-lg-school
Status: published

\[caption id="attachment\_4527" align="alignnone" width="400"
caption="Beehive at Constant the night before"\]![Beehive at Constant
the night
before]({filename}/images/uploads/IMAG0595.jpg "IMAG0595"){: .size-medium .wp-image-4527 }\[/caption\]

\[caption id="attachment\_4528" align="alignnone" width="400"
caption="\*Something\* in preparation"\]![\*Something\* in
preparation]({filename}/images/uploads/IMAG0596.jpg "IMAG0596"){: .size-medium .wp-image-4527 }\[/caption\]

\[caption id="attachment\_4530" align="alignnone" width="400"
caption="Last junk food before LGM refined catering!"\]![Last day of
junk
food!]({filename}/images/uploads/IMAG0599.jpg "IMAG0599"){: .size-medium .wp-image-4527 }\[/caption\]

\[caption id="attachment\_4531" align="alignnone" width="400"
caption="OSP gang"\]![OSP
gang]({filename}/images/uploads/IMAG0600.jpg "IMAG0600"){: .size-medium .wp-image-4527 }\[/caption\]

\[caption id="attachment\_4534" align="alignnone" width="400"
caption="Ginger presenting her workshop — no computer, no slides, just
sharp sentences!"\]![Ginger presenting his workshop — no computer, no
slides, just sharp
sentences!]({filename}/images/uploads/IMAG0603.jpg "IMAG0603"){: .size-medium .wp-image-4527 }\[/caption\]

\[caption id="attachment\_4536" align="alignnone" width="400"
caption="HD swing : Windows &gt; Linux for the workshop &gt; Windows
(1/2h for 10 discs)"\]![HD swing : Windows &gt; Linux for the workshop
&gt; Windows (1/2h for 10
discs)]({filename}/images/uploads/IMAG0605.jpg "IMAG0605"){: .size-medium .wp-image-4527 }\[/caption\]

\[caption id="attachment\_4535" align="alignnone" width="400"
caption="Lunch contacts exchange..."\]![Lunch contacts
exchange...]({filename}/images/uploads/IMAG0604.jpg "IMAG0604"){: .size-medium .wp-image-4527 }\[/caption\]
