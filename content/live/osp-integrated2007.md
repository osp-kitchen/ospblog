Title: November 3: OSP @ Integrated2007
Date: 2007-10-08 23:43
Author: Femke
Tags: Live, News, Presentations
Slug: osp-integrated2007
Status: published

On Saturday November 3, OSP will lecture at
[Integrated2007](http://www.integrated2007.com) "*a new vital
international design conference taking place in deSingel Antwerp*":

> "OSP takes you on a trip into the wonderful universe of Free Software.
> We will meet alien devices and extraterrestrial tools, previously
> unknown to the world of graphic design. We like to imagine what would
> happen if we reinvented or remixed the softwares we work with. While
> conventional packages prevent such forms of use through extremely
> restrictive licenses, Open Source Software invites you to dive under
> the hood of your creative suite. Will we be confused by what we find,
> or open our eyes to new ways of making? OSP seizes the opportunity to
> think out loud about what other tools are possible and what is
> possible with other tools. On our return to earth, nothing will look
> the same again."

<http://www.integrated2007.com>
