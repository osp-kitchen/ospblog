Title: + 4
Date: 2011-02-03 21:29
Author: OSP
Tags: Live, Tools, How-to
Slug: 4-2
Status: published

\[gallery link="file"\]  
Last night, we piped 4 Mac into Dual-Boot Mac OSX and Ubuntu.  
To follow the steps is now quite a routine which can be combined with
cheap beers and pizza.  
1 install [rEfit](http://refit.sourceforge.net/);  
2 create a new partition using Disk Utility;  
3 reboot on a Ubuntu 10.10 dvd.  
-  
→ 3 Wins - 1 failure  
Myriam, Sebastien and Pierre M. switched their machine to the other
side.  
-  
notes - to be able to partition is better - to have a working cd driver
can help (the USB drive didn't work).

At the end of the night, relieved, freed, flabbergasted and more.
