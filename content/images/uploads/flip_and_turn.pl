#!/usr/bin/env perl
use strict;

my $person;
my $people = 5;
my $copyNumber;
foreach $person (1..$people) {
    $copyNumber = sprintf "%03d", $person;
    system("lpr -o page-set=odd -#1 booklet$copyNumber" . ".pdf");
}
