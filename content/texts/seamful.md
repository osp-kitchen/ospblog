Title: seamful
Date: 2009-05-19 18:12
Author: Femke
Tags: Texts, Further reading
Slug: seamful
Status: published

"*Open Source* doesn't mean free access, nor open space or open air; it
presumes a
[seamful](http://www.themobilecity.nl/2008/01/05/designing-for-locative-media-seamless-or-seamful-experiences/)
approach to design as a response to the increasing reliance on
technology and its accessibility"

[http://jaromil.dyne.org/journal/research\_2009.html...](http://jaromil.dyne.org/journal/research_2009.html#Positions%20in%20Flux)
