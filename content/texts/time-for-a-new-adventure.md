Title: Time for a new adventure
Date: 2010-11-02 12:02
Author: Femke
Tags: Texts, Tools, Design philosopy, Discussion, Thoughts + ideas, Tools
Slug: time-for-a-new-adventure
Status: published

[![]({filename}/images/uploads/package-manager-icon-100x85.png "package-manager-icon"){: .alignnone .size-thumbnail .wp-image-5191 width="100" height="85"}]({filename}/images/uploads/package-manager-icon.png)
[![]({filename}/images/uploads/Ubuntu_Software_Center_icon_th.png "Ubuntu_Software_Center_icon"){: .alignnone .size-thumbnail .wp-image-5191 width="100" height="85"}]({filename}/images/uploads/Ubuntu_Software_Center_icon.png)

Dear Ubuntu,

A few weeks ago I needed to re-install my computer. Using the convenient
Startup Disk Creator, I prepared a USB-stick with a fresh version of
Ubuntu 10.04 Lucid Lynx, rebooted the computer and clicked 'install'.
While the Ubuntu slide-show took my mind off the wait and the
automatized installation process was taking place, I fondly remembered
the nervous excitement that installing earlier versions would bring.
Although I sometimes miss that sense of adventure, I admire the rapid
development that Ubuntu has gone through. Compared to my first
experience in 2006, the install is now faultless, fast and pleasant.

But the last slide took me by surprise. It reads: “Take a look at the
Ubuntu Software Center to install (and remove) software from our online
repository, which we carefully organize to be safe and up to date.” And:
“If you need something that isn't available through us, find out if
there is a Debian package or another repository available. That way, it
will be really easy to install and you will receive automatic updates.”

Software from *our* online repository available through *us*?  
<!--more-->  
I opened the Applications menu on the completed Lucid Lynx install and
took a thorough look at the Ubuntu Software Center for the first time. I
discovered that the cardboard box icon that represented the repository
has been replaced by a brown paper shopping bag and also the vocabulary
has changed. 'Ubuntu Software' covers the main category of the Debian
archive; the rest now falls under 'Other Software', including packages
in the categories contrib, non-free and potentially packages for sale.
The term 'Third party' was awkward already, but to replace it by 'Other'
seems vague on purpose. Even more alarming is that the Software Center
refers to some non-free resources as 'Canonical partners'.

I use Ubuntu since 2006 because I sympathize with your attempt to invite
users along that traditionally do not side with Free Software. I
certainly enjoyed the increasing stability and ease-of-use that the
distribution provides, without sacrificing command-line access. By
offering non-free packages such as Adobe Flash, Adobe Reader and Skype
as 'Third party sources', you made me aware and responsible for my own
choice to install non-free software. To a user migrating from MacOS X
like me, collaborating with people using proprietary tools, it seemed a
healthy approach to digital practice; a clean break between free and
non-free is easier said than done. Through the project's obvious care
for interface-issues, Ubuntu also seemed to avoid the over-simplified
GUI vs. command-line position that is present in many F/LOSS projects
and I secretly hoped that with your attention for design, you would one
day find us a way out of there.

The latest developments are unfortunately not very promising as they
seem to be about hiding rather than finding interesting hybrid
expressions. To compare installing Free Software to shopping is missing
the point in such a grotesque way that I have decided to refrain from
installing Ubuntu in the future. Here is more background on this
decision:

If you want to be in contact with software as an actual material, you
need an operating system that can be questioned and experienced. For
years, subsequent versions of Ubuntu offered the kind of reflexive
environment that I was looking for, if only in contrast with the system
I moved away from. The current redesign and rephrasing that is supposed
to help make using Ubuntu a more reliable and predictable experience,
leaves me feeling alienated from how and why this technology is being
produced. It obscures the characteristics of the system, equals out the
variations between different sources that it is built upon and
ultimately starts to internalize a commercial vocabulary. Speaking of
Technology, we are Dreaming of Society ((Mirko Tobias Schaeffer:
Designing a Better Tomorrow: How design is informed by metaphors, images
and associations of social progress
[http://river-valley.tv/designing-a-better-tomorrow](Mirko%20Tobias%20Schaeffer:%20Designing%20a%20Better%20Tomorrow:%20How%20design%20is%20informed%20by%20metaphors,%20images%20and%20associations%20of%20social%20progress%20http://river-valley.tv/designing-a-better-tomorrow))).

Over the last few years, I spent many hours with 'sudo apt-get install'
and Synaptic ((Synaptic is a Graphical User Interface for apt-get or
Aptitude, the Debian package-manager.)), browsing exotic softwares that
I happily installed, tried and uninstalled. In that way I learned about
dependencies and libraries and ultimately about the many cultures of
software. This sense of interdependence, of richness and diversity has
been a source of inspiration for many of the activities and ideas we
developed at OSP ((“We launched OSP because our portfolio started to
fill up with designs for alternative music, copyleft activities and
Linux Install Parties and the gap between the language of our work and
the jargon of the commercial software we used became more obvious with
every new job. We were also interested in the role that software plays
in the creative process and trying to find out how our digital tools
could become a creative and substantial element in design itself. But
since the software packages of Adobe Inc. have become quite the standard
in art academies, creative studios and printshops, it is difficult to
detect their influence, let alone analyse their effect.” Awkward
Gestures. OSP in The Mag.net reader 3: Processual Publishing. Actual
Gestures, edited by Alessandro Ludovico and Nat Muller. 2008)).

The changes in the re-branded Ubuntu remind me of how much the software
repository is at the heart of a Linux-distribution. By defining what
goes into the categories main, contrib or non-free and deciding which
update is considered stable and which one is unstable, the package
managers make the repository a primary location for taking position on
software. Re-phrasing the terminology around it, both visually and in
terms of language is therefore an important shift, as it redefines the
politics of use.

The fact that Ubuntu wants to develop revenue models ((“In order to
monetize, Canonical needs to change its user base, Enderle said.
However, it first needs to provide a platform that's more polished so
people are more likely to buy. Adding the Unity interface for highly
mobile computing is an 'important' step in making Canonical's platform
more user-friendly, but 'they still have a long way to go and will need
to change the perceptions surrounding the product as well if they want
to attract people who will spend money on it” Ubuntu Maverick Meerkat
Pokes Its Head Out. Richard Adhikari in LinuxInsider
[http://www.linuxinsider.com/rsstory/71012.html](“In%20order%20to%20monetize,%20Canonical%20needs%20to%20change%20its%20user%20base,%20Enderle%20said.%20However,%20it%20first%20needs%20to%20provide%20a%20platform%20that's%20more%20polished%20so%20people%20are%20more%20likely%20to%20buy.%20Adding%20the%20Unity%20interface%20for%20highly%20mobile%20computing%20is%20an%20'important'%20step%20in%20making%20Canonical's%20platform%20more%20user-friendly,%20but%20'they%20still%20have%20a%20long%20way%20to%20go%20and%20will%20need%20to%20change%20the%20perceptions%20surrounding%20the%20product%20as%20well%20if%20they%20want%20to%20attract%20people%20who%20will%20spend%20money%20on%20it”%20Ubuntu%20Maverick%20Meerkat%20Pokes%20Its%20Head%20Out.%20Richard%20Adhikari%20in%20LinuxInsider%20%3Ca%20href=)))
is obviously not a problem in itself. It is important that projects like
yours find ways to be sustainable, and with such a widely used
distribution you probably need more money than I can imagine. But in
order to monetize the project, you apparently feel the need to hide the
interesting trouble that comes with Free Software, and so you decided to
put a grocery store at the heart of it. I think it is a mistake to speak
about Free Software as products free for the taking, especially as they
will soon be joined by others being for sale.

At first I found it hard to believe that part of the Ubuntu redesign was
commissioned to people not engaged in free culture whatsoever and sad to
see them consequently use proprietary tools with no apparent discomfort.
((For the redesign of the Ubuntu Font, Canonical decided to commission
the London design agency Dalton Maag Ltd. The font is released under a
free license, but designed using the proprietary tool FontLab without
interaction from the Free Font community.
http://www.daltonmaag.com/docs/FontDevelopment.pdf. See also Nathan
Willis: *Ubuntu's font beta sparks discussions about open font
development* <http://lwn.net/Articles/396711/>)) Had I cared to look at
the Brand Communication Guide, this should not have come as a shock. The
new visual strategy is built around so-called voice, audience and
developer sliders and the whole design concept is meant to elegantly
express a series of binary oppositions: Community (Ubuntu Orange) vs.
Commercial (Canonical Aubergine), Consumer vs. Enterprise, End-user vs.
Engineer. “If you find that you have positioned a slider in the centre,
think carefully about the purpose of your communication and what it is
trying to say. In general, the only time it’s useful to say “both” is
for transitional or gateway pages, like a home page” ((Ubuntu Brand
Communication Guide
<http://design.canonical.com/brand/A.%20Brand_Communication_Guide_v1.pdf>)).

It worries me that Canonical, a company committed to both F/LOSS and
design is so unambitious about visualizing the vibrancy of the Free
software community. This is bad news for F/LOSS as a whole, bad news for
the excellent Libre Graphic tools out there that could have gained from
your support, but also for design itself. Once again it is being reduced
to a cover-up, facilitating the disconnect between the technologies it
is supposed to represent.

The decision I made in 2006 to move away from integrated, seamless
systems literally has caught up with me. It is fair to say that the urge
to try out other F/LOSS operating systems is partially motivated by the
feeling that I am getting used to the Ubuntu-way to such an extent that
it is hard to stay aware of it (I almost missed the changes happening to
the package manager). The direction Ubuntu is taking right now, means
that in order for “people to love and appreciate free software”
((<http://wiki.ubuntu.com/Brand>)) you make it a priority to be in
direct competition with proprietary systems. You decided to replace the
Human-theme by the Light-theme (“Visually, light is beautiful, light is
ethereal, light brings clarity and comfort” ) and are busy with Core
Brand Values. Now Ubuntu starts to speak like the proprietary tools I
wanted to leave behind, it is time to move on.

The cardboard box icon that has been in use throughout various
Gnome-icon sets always seemed a bit of an empty metaphor. Looking at it
in this context, it gains power as a subtle allusion to
transportability, modularity, storage and archive. It ultimately refers
to opening Pandora's box ((Using her fingers, the maid pried open the
lid of the great jar,  
Sprinkling its contents; her purpose, to bring sad hardships to
mankind.  
Nothing but Hope stayed there in her stout, irrefrangible dwelling,  
Under the lip of the jar, inside, and she never would venture  
Outdoors, having the lid of the vessel itself to prevent her,  
Willed there by Zeus, who arranges the storm clouds and carries the
aegis.  
Otherwise, myriad miseries flit round miserable mortals;  
Furthermore, full is the earth of much mischief, the deep sea also.  
<http://www.press.uchicago.edu/Misc/Chicago/329658.html>)), unleashing
the unruly forces of Free Software. It is this unreliable potential that
I have grown to love and appreciate in Free Software and I would like to
find an operating system that reflects it. A distribution that cares
about interface, does not confuse openness with consistency, is not
afraid to risk reliability over solidarity and can think beyond circles
when it comes to visually representing a community. One that does not
mistake users for consumers, nor has a binary view on differentiating
between them and developers.

Before saying goodbye I would like to thank you because without Ubuntu I
would not have made it this far. I truly hope you will stay around,
convincing many other users to consider Free Software. But I am ready to
embark on a new adventure.

Kind regards,

Femke
