Title: Skeleton, Corset, Skin
Date: 2006-11-03 01:14
Author: Femke
Tags: Texts, Reading list, Standards + Formats, Thoughts + ideas
Slug: skeleton-corset-skin
Status: published

![The Men of
Arntz]({filename}/images/uploads/arntz_men.JPG){: #image143}![The
Men of
Arntz]({filename}/images/uploads/arntz_men.JPG){: #image143}![The
Men of
Arntz]({filename}/images/uploads/arntz_men.JPG){: #image143}  
[Stroom Den
Haag](http://stroom.nl/activiteiten/manifestatie.php?m_id=4653044)
started their year long project *After Neurath* with a public symposium.
*After Neurath* looked/looks at the relevance of 1930's philosopher and
information activist Otto Neurath, and as you can imagine various
familiar issues came up.

More information about Otto Neurath:
<http://www.stroom.nl/webdossiers/webdossier.php?wd_id=3530772>

The project is curated by Steve Rushton. Speakers: Frank Hartmann, Robin
Kinross, Kristóf Nyíri and myself.

My talk is here:
[corsetskinskeleton.pdf]({filename}/images/uploads/corsetskinskeleton.pdf){: #image143}
