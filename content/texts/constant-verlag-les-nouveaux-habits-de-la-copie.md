Title: Les nouveaux habits de la copie
Date: 2006-09-10 22:49
Author: Femke
Tags: Texts, Constant Verlag, Licenses
Slug: constant-verlag-les-nouveaux-habits-de-la-copie
Status: published

[![habits.jpg]({filename}/images/uploads/habits.jpg){: #image120}]({filename}/images/uploads/habits.jpg "habits.jpg"){: #image120}  
**Les nouveaux habits de la copie**, Nicolas Malevé  
Text available on line:
<http://www.constantvzw.com/downloads/nouveaux_habits.pdf>  
PDF lay-out:
[habits\_quarantainec.pdf]({filename}/images/uploads/habits_quarantainec.pdf){: #image120}  
PDF cover:
[cover\_robot.pdf]({filename}/images/uploads/cover_robot.pdf){: #image120}  
License: Copyleft, License Art Libre  
Date of publishing: 07-07-2006 (Quarantaine)
