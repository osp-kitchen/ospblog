Title: The double aspect of code
Date: 2007-05-16 19:52
Author: Femke
Tags: Texts, Reading list, Standards + Formats
Slug: the-double-aspect-of-code
Status: published

![isotype\_hair\_in\_drill.jpg]({filename}/images/uploads/isotype_hair_in_drill.jpg)

> "Neurath's pictograms owe much to the Modernist belief that reality
> may be modified by being codified – standardised, easy-to-grasp
> templates as a revolution in human affairs. But the templates
> themselves, or the code, may end up in their turn aestheticised,
> reified, in need of a further round of de-cryption, a paradigm common
> also to failed revolutions. It is this double aspect of code as
> invisible, totalising system and an apparent mechanism for intervening
> in it, and the constant relay between them that opens this specious
> dichotomy onto a wider social history (...)"

Marina Vishmidt in: [The Dutch Are Weeping in Four Universal Pictorial
Languages](http://www.metamute.org/en/The-Dutch-Are-Weeping-in-Four-Universal-Pictorial-Languages-At-Least)
(Mute, May 2007)
