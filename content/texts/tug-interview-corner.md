Title: TUG Interview Corner
Date: 2008-06-10 20:04
Author: Femke
Tags: Texts, LaTex, Reading list
Slug: tug-interview-corner
Status: published

![TeX
logo]({filename}/images/uploads/logonavyongrey.jpg){: .float}Dave
Walden from the TeX User Group introduces his excellent [TUG Interview
Corner](http://www.tug.org/interviews) as follows:

"*technology is created by and evolves with use by people, and the
points of view and backgrounds of the people influence the technology*"

The ever growing list of interviewees include Barbara Beeton, Donald
Knuth, Herman Zapf and numerous other less widely known TeX users and
developers. The interview corner is a meticulously edited set of
documents, portraying the full spectrum of the TeX project. Absolutely
worth reading and/or browsing through.

<http://www.tug.org/interviews/>
