Title: The transformer
Date: 2009-10-09 13:28
Author: Femke
Tags: Texts, Culture of work, Data analysis, Reading list
Slug: the-transformer
Status: published

[![marie\_neurath]({filename}/images/uploads/marie_neurath.jpg "marie_neurath"){: .alignright .size-medium .wp-image-3523 }]({filename}/images/uploads/marie_neurath.jpg)  
<small>The transformer (Marie Neurath) at work</small>

Today a long awaited booklet arrived in the post: *[The transformer,
principles of making Isotype
charts](http://www.hyphenpress.co.uk/books/978-0-907259-40-4)* written
by Robin Kinross & Marie Neurath. It is inspiring in its modest but
precise description of unorthodox working methods developed by
philosopher, sociologist, and economist Otto Neurath and his associates.
To produce 'pictures out of data', a central role was given to 'the
transformer'. Marie Neurath explains:

> It is the responsibility of the 'transformer' to understand the data,
> to get all necessary information from the expert, to decide what is
> worth transmitting to the public, how to make it understandable, how
> to link it with general knowledge or with information already given in
> other charts. In this sense, the transformer is the 'trustee of the
> public'

<http://www.hyphenpress.co.uk/books/978-0-907259-40-4>
