Title: FOSS and the Commercial Print World
Date: 2007-01-03 12:28
Author: Femke
Tags: Texts, Libre Fonts, Printing + Publishing
Slug: foss-and-the-commercial-print-world
Status: published

Craig Bradney and Peter Linnell discuss the future of Free and Open
Source Software for commercial printing:

> Right now, I would say the biggest weakness from an FOSS point of view
> is there are few good high quality fonts. It is one of those areas
> which requires tremendous amounts of QA to make them reliable in the
> commercial print world. This is highlighted throughout our
> documentation.

(QA = [Quality
Assurance](http://en.wikipedia.org/wiki/Quality_assurance) ;-))

Read the interview here:
[http://www.kde.me.uk/index.php?page...](http://www.kde.me.uk/index.php?page=fosdem-interview-scribus)
