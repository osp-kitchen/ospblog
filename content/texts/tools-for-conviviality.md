Title: Tools for conviviality
Date: 2010-01-28 01:35
Author: Femke
Tags: Texts, books, Retrospective Reading, Tools
Slug: tools-for-conviviality
Status: published

In the train back from [Stuttgart](http://nm.merz-akademie.de), I read
*[Tools for
Conviviality](http://clevercycles.com/tools_for_conviviality/)*, a
pamphlet by social philosopher Ivan Ilich (1973). A 'convivial society',
he argues, is a society in which everyone can act autonomously, and this
can be achieved through the design and use of 'convivial tools':

> People feel joy, as opposed to mere pleasure, to the extent that their
> activities are creative; while the growth of tools beyond a certain
> point increases reglementation, dependence, and impotence. I use the
> term "tool" broadly enough to include not only simple hardware such as
> drills, pots, syringes, brooms, building elements, or motors, and not
> just large machines like cars or power stations; I also include among
> tools productive institutions such as factories that produce tangible
> commodities such as corn flakes or electric current, and productive
> systems for intangible commodities such a those which produce
> "education," "health," "knowledge" or "decisions."
