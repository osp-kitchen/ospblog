Title: Dear Software Developer,
Date: 2007-06-04 10:56
Author: Femke
Tags: Texts, Reading list, Scribus
Slug: dear-software-developer
Status: published

![letter.jpg]({filename}/images/uploads/letter.jpg)  
~~We hope you are as much inspired by these texts as we are.~~  
~~You should read this!~~  
~~We think these texts are essential.~~  
~~These texts need to be seen in context of their time.~~  
~~Would this be of any use?~~  
~~Will you read this please?~~  
~~Please do not take their advice literally!~~  
~~We hope you find time to have a look at this.~~  
...

**→** Jan Tschichold, [The Form of the Book, Essays on the Morality of
Good Design](http://learning.north.londonmet.ac.uk/epoc/tschichd.htm),
Hartley & Marks Publishers, 1991  
**→** [Josef
Muller-Brockmann](http://www.filterfine.com/resources/jmb/biblio.htm),
Grid Systems in Graphic Design, Niederteufen 1981
