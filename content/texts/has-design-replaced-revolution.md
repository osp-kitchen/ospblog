Title: Has re-design replaced revolution?
Date: 2008-09-17 11:26
Author: Femke
Tags: Texts, Design philosopy, Reading list, Tools
Slug: has-design-replaced-revolution
Status: published

![latour
network]({filename}/images/uploads/ant.gif "ant"){: .float }Philosopher of science, [Bruno
Latour](http://www.bruno-latour.fr), opened the recent [Networks of
Design conference](http://www.networksofdesign.co.uk) with a keynote
address: *A Cautious Prometheus? A Few Steps Toward a Philosophy of
Design*. In his lecture, Latour linked the growing importance of design
with his idea that "*matters of fact*" have become "*matters of
concern*".

<!--more-->  
Provocatively presenting [Peter
Sloterdijk](http://www.petersloterdijk.net/) as philosopher of design,
he states:

> The great importance of Sloterdijk’s philosophy (and I think the major
> interest of a designer’s way of looking at things) is that it offers
> another idiom. The idiom of matters of concern reclaims matter,
> matters and materiality and renders them into something that can and
> must be carefully redesigned.

In his conclusion, Latour challenges designers "*to draw things
together*", shifting the meaning of design from *re-looking* to a
practice which gathers scientist, technicians etc. around the table:

> What is needed instead are tools that capture what have always been
> the hidden practices of modernist innovations: objects have always
> been projects; matters of fact have always been matters of concern.
> The tools we need to grasp these hidden practices will teach us just
> as much as the old aesthetics of matters of fact —and then again much
> more.

Download the paper here:
<http://www.bruno-latour.fr/articles/article/112-DESIGN-CORNWALL.pdf>
