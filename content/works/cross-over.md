Title: Cross-over
Date: 2008-06-02 20:08
Author: OSP
Tags: Works, Printing + Publishing, Release
Slug: cross-over
Status: published

[image/index.php?level=album&id=21](http://ospublish.constantvzw.org/image/index.php?level=album&id=21)

Hot off the press: **[CROSS-over, Kunst, media en technologie in
Vlaanderen](http://www.bamart.be/pages/detail/nl/2102)**.  
Published by BAM / Lannoo and designed by OSP :-)
