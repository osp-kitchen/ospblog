Title: 500 (printed!)
Date: 2008-07-05 14:59
Author: OSP
Tags: Works, Printing + Publishing, Scripting
Slug: 500-printed
Status: published

![](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/pzi-catalogue/large/822-p1020288.JPG)  
<small>500 unique books installed in [Worm](http://wormweb.nl),
Rotterdam.  
Giant pixel image by Gordo Savicic and Danja Vassiliev.</small>

More images: <http://ospublish.constantvzw.org/image/?level=album&id=23>
