Title: New book cover approved
Date: 2008-08-21 19:16
Author: Harrisson
Tags: Works, In the pipeline, Inkscape
Slug: new-book-cover-approved
Status: published

We're currently finishing the layout of a new book: FLOSS+Art  
Here is a preview of the cover, designed with Inkscape using the "clone
tile" function. Font is Not-Courier Sans...

[![]({filename}/images/uploads/cover_flossart5_web.png "cover_flossart5_web"){: .alignnone .size-thumbnail .wp-image-568 }]({filename}/images/uploads/cover_flossart5_web.png)

FLOSS+Art critically reflects on the growing relationship between Free
Software ideology, open content and digital art. It provides a view onto
the social, political and economic myths and realities linked to this
phenomenon.

<!--more-->

Topics include: digital art licensing,  
copying and distributing under open content models,  
the influence of FLOSS on digital art practices,  
the use of free software to produce art and  
the art of producing free software,  
FLOSS as an embedded political message in digital art,  
paradoxes and limitations of open licenses for digital art,  
FLOSS as a way to quote and embed other artworks in  
the making of new works, definitions and manifestos  
for a free software art…

With contributions from:

Fabianne Balvedi  
Florian Cramer  
Sher Doruff  
Nancy Mauro Flude  
Olga Goriunova  
Dave Griffiths  
Ross Harley  
Martin Howse  
Shahee Ilyas  
Ricardo Lafuente  
Ivan Monroy Lopez  
Thor Magnusson  
Alex McLean  
Rob Myers  
Alejandra Maria Perez Nuñez  
Eleonora Oreggia  
oRx-qX  
Julien Ottavi  
Michael van Schaik  
Femke Snelting  
Pedro Soler  
Hans Christoph Steiner  
Prodromos Tsiavos  
Simon Yuill

Compiled and edited by Aymeric Mansoux and Marloes de Valk

This publication is made possible with support from:  
- the Digital Research Unit at the University of Huddersfield  
- Piet Zwart Institute, the Willem de Kooning Academy  
- Constant  
- OpenMute  
- GOTO10
