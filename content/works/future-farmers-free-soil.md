Title: Future Farmers Free soil
Date: 2009-06-04 12:24
Author: Yi
Tags: Works, Scribus, Tools
Slug: future-farmers-free-soil
Status: published

<span
class="style11">**[![bar-and-bus]({filename}/images/uploads/bar-and-bus.gif "bar-and-bus"){: .alignleft .size-full .wp-image-2842 width="143" height="82"}]({filename}/images/uploads/bar-and-bus.gif)  
**A journey through the history and currents of free education,
counter-institutional movements and the economy of information in
Silicon Valley + beyond.<span class="style22">  
</span>  
[Free Soil](http://www.futurefarmers.com/tour2/index.html) presents a
[bus tour](http://www.futurefarmers.com/superfund/gazette/tour.html),
exhibition, outdoor film/video festival & on-site exchange in
conjunction with the 2nd Biennial 01SJ Global Festival of Art on the
Edge in San Jose, California. This tour takes inspiration from the
spirit of counter cultural activities prevalent in this region in the
late 1960’s, namely the activities of the Mid-Peninsula Free University,
Homebrew Computer Club,draft-resisters and the back-to-landers. <span
class="style33">These prototypical models of alternative education
formed in reaction to the growing influence of the military-industrial
complex on American universities. Courses were taught on topics ranging
from intentional communities and  sand-casting candles, to Maoist
political theory , “To Be Gentle,” and "The Art of Giving Away
Bread."</span></span>

<span class="style11"><span class="style33">...</span></span>Join 30
artists, scientists, curators on a 1.5 day long journey that includes
small walks, lectures, mini-workshops and other particpatory events. The
Tour lasts 7 hours and includes lunch, drink and snacks.

<span class="style33">As part of the Free Soil Bus Tour Project a reader
will be collated and printed by this occasion. This reader is primarily
a resource for people engaged in experimental/alternative learning
projects. [Marthe](http://www.ooooo.be/indexhibit/) is now working hard
on this reader by trying [Scribus](http://www.scribus.net/). And OSP is
willing to get on the bus at last stop, to share the joy of this journey
in coming weeks, someday in sunny June.  
</span>
