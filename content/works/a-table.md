Title: A table!
Date: 2009-03-11 14:02
Author: Yi
Tags: Works, Cooking, Digital drawing, In the pipeline, Recipe
Slug: a-table
Status: published

Un petit livre de cuisine en préparation... il s'agit un livret de
recettes créées par les habitants de chez
[Puerto](http://www.archipel.be/Onzedeelwerkingen/Puerto/tabid/1749/Default.aspx?PageContentID=1897&PageContentMode=).
Une association qui offre un soutien aux personnes qui sont en
difficulté entre autres en leur proposant un repas équilibré à prix
réduit. 3  services (entrée | plat | dessert)  pour moins de 4 euro par
personne,  pour un total d'une quinzaine de personnes environs. Un vrai
challenge! Le livret sera illustré non seulement par les les mains du
graphiste mais aussi par celles des habitants . Une  session de dessin a
été organisée le 23 février après diner.

[![complet\_s\_final1]({filename}/images/uploads/complet_s_final1.png "complet_s_final1"){: .alignnone .size-full .wp-image-2132 }]({filename}/images/uploads/complet_s_final.png)

En attendant que les légumes soient découpés,  le four préchauffe...  on
espère pouvoir vous faire les partager bientôt avant l'été.

[![post1]({filename}/images/uploads/post1.png "post1"){: .alignnone .size-full .wp-image-2132 }]({filename}/images/uploads/post.png)
