Title: Crash test: Travail mobile
Date: 2006-09-13 09:30
Author: Femke
Tags: Works, Inkscape, Printing + Publishing
Slug: crash-test-travail-mobile
Status: published

![travail\_screen.jpg]({filename}/images/uploads/travail_screen.jpg){: #image126}  
<small>Lay-out in Scribus</small>  
[Download low-res PDF](http://www.geuzen.org/download/travail_low.pdf)  
Finished! Sketched and produced a seven page contribution to
[Open](http://www.skor.nl/set-635-nl.html?lang=en) (Dutch bi-monthly on
art in public space) + inside cover. Images were prepared in Gimp;
pattern assembled in Inkskape and document lay-out in Scribus
(v1.3.3.2); all on Ubuntu.  
<!--more-->  
- Importing outlines from Gimp, re-using them with multiple images /
layers in Inkskape: flawless; selection editor + svg export in Gimp
works well with vector options in Inkskape. Had some problems with
transparency in PDF-export from Inkskape.  
- EPS export from Scribus, after a few trials and errors flawless too;
including transparency of layers.  
- Lack of interaction with EPS-export is frustrating; default is to crop
the document along its margins so all you can do is set margins to zero
before export.  
- EPS can only export one page at the time...  
- Bleed is automatically cut off on export too, but making the document
3mm larger on each side, is a quick workaround.
