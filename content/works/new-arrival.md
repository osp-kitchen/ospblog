Title: New Arrival
Date: 2008-10-23 09:42
Author: OSP
Tags: Works, Not-Courier sans, Printing + Publishing
Slug: new-arrival
Status: published

[![]({filename}/images/uploads/img_0857.jpg "img_0857"){: .alignnone .size-medium .wp-image-1155 }]({filename}/images/uploads/img_0857.jpg)

Just arrived from the printer: *Routes + Routines*, a booklet designed
by OSP's Yi Jiang and Ludivine Loiseau documenting Peter Westenberg's
*[Hasseltse
Netwerkwandelingen](http://data.constantvzw.org/site/spip.php?article532)*.
