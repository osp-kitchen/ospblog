Title: Dingbats in a monkey
Date: 2010-02-25 18:33
Author: ludi
Tags: Fonts, Foundry (blog), News, Type, dingbats, Fontforge, Libre Fonts, Standards + Formats, Unicode
Slug: dingbats-in-a-monkey
Status: published

[![]({filename}/images/uploads/DLF-poster.png "DLF-poster"){: .alignleft .size-medium .wp-image-4062 }]({filename}/images/uploads/DLF-poster.png)

The Dingbats Liberation Fest adventure continues in Nancy from March 25
to May 6.

Invited by the [my.monkey](http://www.mymonkey.fr/) gallery, OSP
présentera quelques uns de ses travaux et ouvrira un nouvel atelier
Dingbats Liberation Fest into the grid.

<!--more-->  
Le projet de fonte collaborative Dingbats Liberation Fest propose de
redessiner les caractères Dingbats et Miscellaneous Symbols d'Unicode.  
Après 2 premiers workshop à
[Bruxelles](http://ospublish.constantvzw.org/live/dingbat-liberation-fest-populated-fonts)
(festival VJ12) et
[Utrecht](http://ospublish.constantvzw.org/live/dingbat-liberation-fest-ii)
(CASCO), the font already gathers more than 70 ! characters.  
Passez ajouter votre version !

![]({filename}/images/uploads/sofar.png "sofar"){: .alignleft .size-medium .wp-image-4062 }

Parmi nos (re)découvertes, les noms de caratères Unicode en français :  
http://www.unicode.org/fr/charts/charindex.html  
❈ : GROS ÉTINCELLEMENT  
❛ : GUILLEMET DE FANTAISIE EN FORME DE GROSSE VIRGULE SIMPLE CULBUTÉE
