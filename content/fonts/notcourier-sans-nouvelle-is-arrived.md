Title: NotCourier-sans nouvelle is arrived
Date: 2008-09-07 15:52
Author: ludi
Tags: Fiche, NotCourierSans, Type, Courier, Font, Libre Fonts
Slug: notcourier-sans-nouvelle-is-arrived
Status: published

A new release of the NotCourier-sans is now available
[here](http://openfontlibrary.org/media/files/OSP/309 "here"), with its
bold.  
The OSP frog, the IJ, ij, numero and trademark ligatures are now encoded
in the discretionary ligatures.  
[![]({filename}/images/uploads/picture-3-300x88.png "NotCourier-sans-Bold preview"){: .alignnone .size-medium .wp-image-621 width="300" height="88"}]({filename}/images/uploads/picture-3.png)[  
]({filename}/images/uploads/picture-1.png)
