Title: Update your NotCourier !
Date: 2009-05-17 18:23
Author: ludi
Tags: Fonts, Foundry (blog), News, NotCourierSans, Type, Works, Libre Fonts, Not-Courier sans, Type
Slug: update-your-notcourier
Status: published

[![notcouriersans\_111]({filename}/images/uploads/notcouriersans_111.png "notcouriersans_111"){: .alignleft .size-medium .wp-image-2629 }]({filename}/images/uploads/notcouriersans_111.png)  
A new version of the impolite NotCourierSans is available on the [Open
Font Library](http://openfontlibrary.org/media/files/OSP/411) :
NotCourierSans 1.1

<!--more-->

As reminder, NotCourierSans, is a re-interpretation of Nimbus Mono whose
design began in Wroclaw at the occasion of the Libre Graphics Meeting
2008.  
For more detailed information explore the files included in the font
package (FONTLOG.txt) or go through the Font Info section (Font Log and
Comment) in the font (open the font file in FontForge and go to Font
Info in the Element menu).

This NotCourierSans 1.1 has been expanded by a work on cyrillic glyphs.  
[Paulo Silva](http://nitrofurano.linuxkafe.com) aka nitrofurano, is
programmer and graphic designer in Porto. He removed serifs from
cyrillic characters, removed all kerning pairs and replaced the repeated
glyphes with references (accented characters and alike).

I took the opportunity to clarify bonus glyphs position.  
NotCourierSans 1.1 contains 2 ornamental glyphs encoded in the [private
use
characters](http://en.wikipedia.org/wiki/Mapping_of_Unicode_characters#Private_use_characters):  
- in U+E000, the OSP frog mascot  
- in U+E001, the 75 ligature added during an OSP workshop in [Le
75](http://www.le75.be), École Supérieure des Arts de l'Image, on
Wednesday 17 December.  
These sugars are accessible through the Ornament Open Type features.  
You can test it in [Fontmatrix](http://fontmatrix.net)

Super thanks to Nicolas Spalinger who helped me to build a fresh new
package following [the new Open Font Library model
package](http://oflb.open-fonts.org) (foo-open-font-sources-1.0.tar.gz)

Super thanks to [Pierre Marchand](http://www.oep-h.com) for his support
and advices, particularly for the Font Info and special glyphs
encoding.  
Very soon a new post about [Fontmatrix](http://fontmatrix.net) and how
to make a font talkative.

merci [LGM 2009](http://www.libregraphicsmeeting.org/2009/?lang=en) for
the possibility and idees around fonts that it sows.

Look at the recent appearances of NotCourierSans
[here](http://ospublish.constantvzw.org/news/the-ecstasy-of-influence),
[here](http://ospublish.constantvzw.org/news/jaz) ou
[ici](http://ospublish.constantvzw.org/type/le-sale-boulot-with-notcourier).

Please update your .fonts folder !
