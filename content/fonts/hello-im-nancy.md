Title: Hello I'm NANCY ♥
Date: 2010-03-28 14:29
Author: Femke
Tags: Foundry (blog), Works, expo, Recipes, Standards + Formats, Type, Unicode
Slug: hello-im-nancy
Status: published

3 days, 327 kilometers, 6 liters of [Cube Cola
Libre](http://sparror.cubecinema.com/cube/cola/) and 54 commits later
...  
Please meet [NANCY](http://ospublish.constantvzw.org/nancy/#nancy) (a
new OSP-software project), download the [Dingbat Liberation font
(DLFo)](http://ospublish.constantvzw.org/nancy/#download) and much more
at:
[http://ospublish.constantvzw.org/nancy](http://ospublish.constantvzw.org/nancy/)

[![](http://ospublish.constantvzw.org/image/plog-content/thumbs/public-appearance/dingbats-liberation-fest-3/large/1330-osp25.jpg)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1330)  
Re-thinking Miscellaneous Symbols: *Flêche grasse à pointe arondie vers
la droite*?

<!--more-->[![](http://ospublish.constantvzw.org/image/plog-content/thumbs/public-appearance/dingbats-liberation-fest-3/large/1274-p1070343.jpg)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1274)  
Taking a fragment of the UTF-8 plane into our hands

[![](http://ospublish.constantvzw.org/image/plog-content/thumbs/public-appearance/dingbats-liberation-fest-3/large/1307-p1070386.jpg)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1307)  
Serving liters of Cube Cola Cuba Libre

[![](http://ospublish.constantvzw.org/image/plog-content/thumbs/public-appearance/dingbats-liberation-fest-3/large/1306-p1070439.jpg)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1306)  
The début of a new OSP-software project: NANCY ♥

[![](http://ospublish.constantvzw.org/image/plog-content/thumbs/public-appearance/dingbats-liberation-fest-3/large/1294-p1070410.jpg)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1294)  
The OSP-frog made it all the way to Nancy
