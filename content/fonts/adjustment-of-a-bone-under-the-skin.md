Title: Adjustment of a bone under the skin
Date: 2009-06-25 13:20
Author: Pierre
Tags: OSP-DIN, Texts, Type, Berlin, calligraphy, DIN, letters, Thoughts + ideas, Type
Slug: adjustment-of-a-bone-under-the-skin
Status: published

Last LGM and its typographic excitements has brought the [DIN - Das Ist
Norm - Loch Ness project](http://ospublish.constantvzw.org/type/din-4)
to the surface of the Saint Laurent river again in a discussion with
Denis Jacquerye from [Deja Vu](http://dejavu-fonts.org/). Back in
Brussels, we meet Denis in the temporary OSP Studio at Rue de la Senne
to begin to define on what criteria and with which tool to work (name
dropping : stroke parts - stroke fonts in FontForge - svg fonts in
Inkscape - Metafont - ...). So we browse through some of the pictures we
brought back from the DIN archive.

From the different versions we have seen in the archives, it seems that
the main DIN letters models are based on solo strokes drawn on a grid at
small size. It was the regular usage for most of the texts in the
engineering environment. The thickness of the tool used (pencil, drawing
pen, ball nose mill) defined the boldness of the strokes and the round
or less round shape of their extremities, like flesh on bones.  
<!--more-->  
Later this was also applied to larger lettering, so strokes became
surfaces and the drawing began to be defined by the contour, by its
skin. Simple geometric extrapolation from strokes were operated, using
the unit of the grid as unit for the thickness of the stroke, to
normalize sizes. In the oblique letters, the angle of the shape at the
end of strokes became angled and goes farther than the regular width,
defined by the grid. So, as these letter parts could be less open in
their 'fill' version than in their 'stroke' one, the core was moved a
little towards the inside of the glyph to fit in the grid.

That shift from calligraphy to typography is traditionaly hidden in the
progressive adaptations by generations of letterers. But in the case of
the DIN lettering, as a norm, the movement must be described in detail.
And that effort produced the beautiful figure that appeared before
Harrisson and me two year ago on a screen of the library of the DIN
Institute...

[![din-skin-bones]({filename}/images/uploads/din-skin-bones.jpg "din-skin-bones"){: .alignnone .size-medium .wp-image-3114 }]({filename}/images/uploads/din-skin-bones.jpg)
