Title: We are not here to be correct
Date: 2010-11-16 17:15
Author: OSP
Tags: NotCourierSans, derivatives, Not-Courier sans, Open Fonts
Slug: we-are-not-here-to-be-correct
Status: published

Ana Carvalho and Ricardo Lafuente from [Manufactura
Independente](http://hacklaviva.net/) have made PropCourier Sans, the
display and identity font for [Libre Graphics
Magazine](http://libregraphicsmag.com/) ... a super derivative of our
own
[NotCourierSans](http://ospublish.constantvzw.org/foundry/notcouriersans/)
("We are not here to be polite")!  
[![]({filename}/images/uploads/propcouriersansspecimen-thumb-500x707-265.png "propcouriersansspecimen-thumb-500x707-265")]({filename}/images/uploads/propcouriersansspecimen-thumb-500x707-265.png)  
Download hopefully available \*soon\* :-)

Via:
<http://www.adaptstudio.ca/blog/2010/11/while-you-were-out-i-made-you-a-type-specimen.html>
