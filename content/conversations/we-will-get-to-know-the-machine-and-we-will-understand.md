Title: We will get to know the machine and we will understand
Date: 2009-08-26 14:37
Author: Femke
Tags: Conversations, Free Software Community, History, Tools
Slug: we-will-get-to-know-the-machine-and-we-will-understand
Status: published

Before starting a fresh new OSP-season, first a post long due:

This conversation with Juliane de Moerlooze was recorded March 2009 in
the context of [Female Icons](http://www.geuzen.org/female_icons), a
project by [De Geuzen](http://www.geuzen.org) but I think OSP-readers
might like to read it as well?

![Juliane](http://www.geuzen.org/female_icons/wp-content/uploads/juliane_small.jpg)

“*when you hear people talk about women having more sense for the
global, intuitive and empathic… and men are more logic… even if it is
true… it seems quite a good thing to have when you are doing math or
software?*"

Juliane is a Brussels based computer scientist, feminist and Linux user
of the first hour. She studied math, programming and system
administration and participates in the
[Samedies](http://samedi.collectifs.net/) (a group of women maintaining
their own server). In February 2009, she was voted president of the
[Brussels Linux user group](http://www.bxlug.be).

Download interview:
[juliane.odt]({filename}/images/uploads/juliane.odt)
