Title: Listen to F/LOSS
Date: 2010-09-16 09:38
Author: Femke
Tags: Conversations, Inkscape, Python, Radio, resources
Slug: listen-to-floss
Status: published

[![]({filename}/images/uploads/podcast_5_3.jpg "podcast_5_3"){: .alignnone .size-full .wp-image-4968 }](http://twit.tv/FLOSS)

At [FLOSS-weekly](http://twit.tv/FLOSS) you can find a collection of
130+ longer interviews with Free Software developers, including some
involved in our favourite projects:

\#11: [Python](http://twit.tv/floss11) (Guido van Rossum: "*If you give
the same task to different programmers, they'll come up with different
solutions. When programmer B at some point has to maintain the code of
programmer A, it is tempting to rewrite the code instead, because it
would not be the same solution programmer B would have chosen*")  
\#52: [Processing](http://twit.tv/floss52) (Ben Fry: "*Working in code,
changes the type of things I can look at*")  
\#76: [Inkscape](http://twit.tv/floss76) (Jon Cruz: *"Vector graphics
are the shapes themselves*")  
\#81: [OpenStreetmap](http://twit.tv/floss81) (Steve Coast: "*Maps are
never complete. They are always changing*" )
