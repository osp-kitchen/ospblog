Title: Visual Culture and Médor at Europe Refresh
Date: 2014-09-29 15:51
Author: Colm
Tags: Conversations, News, Tools, Works, Bruxelles-Paris, crouwd funding, Europe Refresh, financement collaboratif, Les Halles, Médor, Visual Culture
Slug: visual-culture-and-medor-at-europe-refresh
Status: published

![](http://www.halles.be/website/images/dbfiles/4445/small/Halles-Europe-Refresh.png){: .alignnone }

[![IMG\_20140912\_162834]({filename}/images/uploads/IMG_20140912_162834-e1411825705936.jpg){: .alignnone }]({filename}/images/uploads/IMG_20140912_162834.jpg)

Once again [Les Halles](http://www.halles.be/en/ "Les Halles") are
housing the 2014 edition of «Le salon du financement participatif»
called [Europe Refresh](http://www.halles.be/en/150/europe-Refresh) and
it looks like it's going to be a couple of exciting weekends!

This year, to maximize the chances of fully funding the projects, the
Salon will be set up for a weekend in Brussels then a [weekend in
Paris](http://www.carreaudutemple.eu/2014/06/13/europe-refresh).

We're very happy to be attempting two big projects this year: [Visual
Culture](http://www.halles.be/en/europerefresh/106/Visual-Culture-a-tool-for-design-collaboration)
and [Médor](http://www.halles.be/en/europerefresh/62/M%C3%A9dor).

 

**Visual Culture**

is the name we've given to the tool that we're building to share and
publish any (design) project with the possibility to see and retrieve
any previous version. Its current form is what constitutes OSP website's
home page, and it makes up the exploratory system enabling navigation
into each repository OSP uses.

http://osp.kitchen/tools/visualculture/

 

### Médor

is not a dog! It's a trimestrial, Belgian magazine of inquiries and
stories, 128 pages long. *Médor* contains long-term investigations,
reports and portraits focused on Belgium. *Médor* digs the heart of
issues. It inquires and it is persistent. It takes the time needed to be
further, beyond appearances. It seeks to understand the facts and to
give opinions about uncovered truths. *Médor* launched this week and is
planning several methods to get the project fully financed. See the
current progress on the website:

https://medor.coop/

 

See you there?

 
