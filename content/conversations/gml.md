Title: GML
Date: 2010-07-23 10:41
Author: Femke
Tags: Conversations, Archiving, Digital drawing, Standards + Formats
Slug: gml
Status: published

[![]({filename}/images/uploads/gml.png "gml"){: .alignnone .size-medium .wp-image-4766 }]({filename}/images/uploads/gml.png)  
Yesterday [Constant](http://www.constantvzw.org) met with [Evan
Roth](http://evan-roth.com/) to discuss [gestures and
standards](http://graffitianalysis.com/gml/), [confessions and
F/LOSS](http://www.blendernation.com/blender-graffiti-analysis/),
[archiving and collaboration](http://000000book.com/).

More soon.
