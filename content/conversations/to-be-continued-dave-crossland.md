Title: To be continued: meeting Dave Crossland
Date: 2007-11-18 23:13
Author: OSP
Tags: Conversations, Type, History
Slug: to-be-continued-dave-crossland
Status: published

[![]({filename}/images/uploads/dave1.jpg)]({filename}/images/uploads/dave1.jpg){: .float}
[![]({filename}/images/uploads/dave2.jpg)]({filename}/images/uploads/dave2.jpg){: .float}We
nearly missed our train back to Brussels while meeting Dave Crossland.
At the station we talked about the history of font editing software,
about the 'free font movement' and everything that could become possible
once fonts and font editing software are free. We were also excited
about how (and why!) Dave wants to open up the fine art of typography to
a larger public. To be continued with an interview hopefully soon. In
the mean time, here you can read his weblog:
<http://understandinglimited.com/archives/>
