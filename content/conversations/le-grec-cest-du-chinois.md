Title: Le grec, c'est du chinois
Date: 2007-05-25 14:56
Author: Harrisson
Tags: Conversations, Type, Culture of work, LGM 2007, Standards + Formats
Slug: le-grec-cest-du-chinois
Status: published

**Conversation avec Pierre-Luc Auclair (Déjàvu) et Nicolas Spalinger
(OFL)**

[![pierre\_luc.jpg]({filename}/images/uploads/pierre_luc.jpg)]({filename}/images/uploads/pierre_luc.jpg "pierre_luc.jpg")[![nicolas.jpg]({filename}/images/uploads/nicolas.jpg)]({filename}/images/uploads/nicolas.jpg "nicolas.jpg")  
[  
SIL\_deja\_vu.mp3](http://ospublish.constantvzw.org/documents/sound/SIL_deja_vu.mp3)
(27 mn - 25 Mb)

Extrait d'une conversation avec Pierre Luc Auclair, graphiste et
collaborateur dans le projet de police [Déjà
vu](http://dejavu.sourceforge.net/wiki/index.php/Main_Page), et Nicolas
Spalinger, bénévole de la [SIL](http://www.sil.org/), équipe fontes
libres Debian et Ubuntu, OFLB.  
En français dans le texte, avec quelques questions posées en anglais.  
Ou l'on parle de polices de caractère libres, de travail typographique
collaboratif, d'apprentissage et de licences & standarts.

Liens:  
[Déjà vu](http://dejavu.sourceforge.net/wiki/index.php/Main_Page),
[SIL](http://www.sil.org/), [OFL](http://scripts.sil.org/OFL), [Gentium
font](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&item_id=Gentium),
[Andika
font](http://www.sil.org/computing/catalog/show_software.asp?id=119),
[Typophile](http://www.typophile.com/)

<!--more-->

Interview: Projet Dejavu et typographie libre et collaborative

P-L: Pierre-Luc Auclair (Typographe Dejavu, Graphiste)  
N: Nicolas Spalinger (Bénévole de la SIL – Equipe fontes libres Debian &
Ubuntu – Open Font Library)  
H: Harrisson  
F: Femke

H: Tu travailles sur la fonte Déjavu (<http://dejavu.sf.net>) c'est ça?

P-L: Oui c'est ça

H: Qui est un projet entièrement open source?

P-L: Oui, c'est ça.

H: Et ça fait déjà un moment que cette fonte existe, non?

P-L: Ca fait... Je ne pourrais pas dire depuis combien de temps
exactement, ce qui est sûr c'est qu'avant c'était une fonte de Bitstream
(<http://www.bitstream.com>) qui est sortie ou plutôt qu'ils ont donné
au domaine du libre. Et depuis ce temps-là, des gens ont ajouté des
fonctionnalités, des choses comme ça-là, plusieurs caractères, des
nouveaux languages, ajusté le kerning
(<http://fr.wikipedia.org/wiki/Kerning>), dans le fond ajouté des
éléments à la police au complet pour la rendre utilisable.

H: OK donc rajouter des éléments?

P-L: Oui.

H: Chez Bitstream, quelle a été la motivation de lâcher des fontes?

P-L: Alors ça je ne sais pas.

Nicolas: Ca été un partenariat avec la fondation GNOME
([http://foundation.gnome.org](http://www.gnome.org/press/releases/bitstreamfonts.htm),
<http://www.gnome.org/press/releases/bitstreamfonts.html>) si je peux
m'immiscer dans la conversation.

H: Oui, oui, c'est bien.

P-L: Moi je ne suis pas au courant de ça.

N: Il y a des négociations qui ont été faites entre la fondation GNOME
et Bitstream le fondeur, avec des gens comme Jim Gettys
(<http://en.wikipedia.org/wiki/Jim_Gettys>) qui était membre du bureau
de la fondation. L'idée c'est d'avoir une famille globale avec
suffisamment de...

P-L: ...suffisamment de caractères pour supporter toutes les langues
dans Linux avec une seule famille...

N: oui, avoir une famille suffisamment vaste, complète.

H: En faire une fonte système?

N: Enfin oui, système au sens où ça serait la fonte par défaut. Et donc
ça a pris des mois quand même pour faire la négociation, entre les
termes que la fonderie voulait et ce que la fondation GNOME essayait
d'arracher comme droits, de modification notamment. Et donc moi j'ai
suivi ça un petit peu. Parce que en même temps nous, dans le cadre de la
SIL (<http://scripts.sil.org>), on réfléchissait aussi à la meilleure
stratégie pour faire ça. L'idée c'était d'avoir quelque chose d'assez
riche, de bonne qualité, avec du hinting - des instructions - qui soit
dédié à un affichage correct sur le bureau.

H: Vous avez eu le choix entre différentes fontes ou bien c'est
Bitstream qui vous a proposé juste celle-là?

N: Alors je sais pas vraiment, j'étais pas directement impliqué dans le
cadre de la fondation GNOME, j'ai rejoint le projet après.

P-L: Ah oui, il connaît plus ça que moi.

N: Ha ha, non!

N: Je sais qu'ils ont approché d'autres fonderies, mais c'est Bitstream
qui a offert ce qui était le plus intéressant.

H: Ah oui, donc il a eu une négociation?

N: Oui il y a eu une négociation qui a eu lieu. Donc il y a vraiment de
la part du fondeur une stratégie... lls veulent s'acheter une bonne
image, ce qui est légitime et ils veulent aussi protéger quelque part
leur nom. Le coeur de la négociation c'était avoir des droits de
modification, pour que les gens de la communauté, les typographes de la
communauté puisse la modifier, l'étendre, mais que la réputation de
Bitstream en tant que fonderie soit plus ou moins protégée. Donc il y a
un espèce de point central, de « nexus », qui a été créé entre le droit
de modification et le nom même attaché à la police, voilà.

H: Et quand on voit la license, c'est en effet toujours un copyright
Bitstream avec droit de modification (<http://www.gnome.org/fonts/>)

N: Voilà, c'est un droit de modification, donc quand la modification a
lieu, le faut que le nom soit changé, et dans le nom même de la police
il y a le nom de la fonderie, ce qui est assez fréquent malgré tout.
Donc le nom c'était Bitstream Vera et ensuite il y a les variantes. Il y
a eu une grosse « press release » puisque c'est un gros pas en avant. Et
donc ensuite, il y a toute une équipe qui s'est mis à travailler sur ça,
il y a eu plusieurs branches de Vera et, au bout d'un certain temps, la
branche la plus importante – Dejavu - a re-mergé les travaux qui ont été
faits dans les autres branches. Sur le site de Dejavu il y a
l'historique un petit peu des autres branches qui ont été faites
(<http://dejavu.sourceforge.net/wiki/index.php/Bitstream_Vera_derivatives>).
Il y a eu une branche par exemple pour les caractères en Gallois, et
pour d'autres pays en d'Europe par exemple. Aussi du travail autour des
capacités OpenType (<http://en.wikipedia.org/wiki/OpenType>)...

P-L: oui des capacités Opentype...

N: ...pour les polices intelligentes, ce genre de chose, etc. Ce qui
était impressionnant, moi, j'ai observé un peu le projet Dejavu, j'ai
pas vraiment contribué, mais c'est la rapidité avec laquelle le projet a
avancé.

P-L: et dans le fond, c'est probablement aussi parce qu'il y a une
release tous les mois.

N: Oui c'est basé sur un rythme de release actif, tout ce qui est prêt
est disponible. Il y a aussi une roadmap bien définie, les outils sont
là: outil de révision de contrôle, mailing-list, wiki. L'initiateur du
projet c'était Stepan Roh (<http://www.srnet.cz/~stepan/en/>), et
ensuite d'autres personnes.

P-L: Je pourrais pas vraiment donner de noms.

N: Au bout d'un moment il a un peu lâché le projet et a fait d'autres
choses mais il a mis en place l'infrastructure, mais là, depuis quelques
temps, il est revenu. Et les leaders actuels du projet c'est Denis
Jacquerye
(http://dejavu.sourceforge.net/wiki/index.php/Denis\_Jacquerye) et Ben
Laenen (http://dejavu.sourceforge.net/wiki/index.php/Ben\_Laenen), je
crois, mais il y a plein de monde, plein d'autres gens, la liste des
contributeurs est assez vaste. Ben Laenen a d'ailleurs sorti une police
musicale, c'est celle qu'on à montré hier. (Euterpe:
<http://www.openfontlibrary.org>)

P-L: Ben moi je les connais avec leurs nicks IRC: lui c'est eimai.

N: Oui eimai et moyogo.

H: Elle est assez bien dessinée en plus, elle est assez jolie.

N: Ce qu'ils ont fait aussi c'est travailler sur un certain nombre de
scripts de gestion des fichiers sources. Donc l'outil de base, c'est
fontforge (<http://fontforge.sourceforge.net>), donc je ne pense pas
qu'il y en ait qui utilisent des logiciels restreints, normalement tout
le monde utilise fontforge.

P-L: Oui toute la source doit rester au format fontforge.

N: Pour faire la construction.

P-L: Quand il a y des modifications pour faire l'import dans la branche
principale ça prend absolument le fichier source fontforge pour
l'insérer dedans.

N: Il y a aussi du travail aussi sur les instructions, tout ce qui est
du positionnement, les ligatures, ils sont assez en avance par rapport
aux autres projects collaboratifs. Moi j'ai vu aussi qu'il y avait des
scripts pour avoir les statistiques de la couverture Unicode
([ttp://www.unicode.org](http://www.unicode.org)) du SFD
(<http://fontforge.sourceforge.net/sfdformat.html>) par exemple.

P-L: Ca aussi c'est sur le wiki

N: A chaque fois qu'il y a une release, dans le tarball il y a ce quiest
couvert par bloc, donc il y a un pourcentage de couverture Unicode par
bloc et on voit que ça monte vite, il y a ce qui est prévu par la suite,
il y a un changelog. C'est assez riche au niveau descriptif et c'est
très ouvert comme approche. Et donc l'utilisation de subversion
(<http://subversion.tigris.org>/) comme dépôt de sources pour travailler
en commun dessus. Donc il y a un canal IRC
(<http://fr.wikipedia.org/wiki/Internet_Relay_Chat>) et plusieurs
mailing-lists
(<http://dejavu.sourceforge.net/wiki/index.php/Dejavu-fonts_mailing_list>).

P-L: Récemment il y avait quelqu'un qui essayait de refaire le script
Grec, je ne sais pas vraiment ce que ça a donné, je ne me suis pas tenu
au courant.

N: Alors ça j'ai pas suivi non plus.

P-L: Il avait beaucoup posté son travail sur Typophile, le site web
(<http://typophile.com/>).

N: C'est un site web communitaire, vous connaissez peut-être oui?

P-L: Là-bas il y a beaucoup de gens, beaucoup de professionnels de la
typographie, qui leur ont donné des exemples, des experts en typographie
qui ont donné leur avis sur comment le projet avançait pour le script
Grec. Il ont donné des conseils. Il y a eu un gros débat: est-ce qu'on
va plus vers une esthétique du style romain ou alors est-ce qu'on va
plus vraiment vers une esthétique grecque locale? Ca c'est toujours un
problème, c'est la même chose aussi j'imagine pour les scripts chinois
et les scripts japonais.

N: Connaître le degré de sensibilité culturelle pour un bloc particulier
Unicode de la police, ça c'est très très dur. Même les meilleurs
designers ne peuvent pas avoir une sensibilité suffisante. Plus la
police est riche, plus elle couvre de blocs, plus c'est dur justement.
Parce qu'il y aura forcément des tensions. Certains blocs sont communs à
différents styles. C'est là où ça devient dur d'avoir dans la même
police et mettons du cyrillique qui doit avoir un style pour une
communauté linguistique qui parle une une langue dans laquelle il y une
transcription cyrillique et les Russes qui préfèrent ça comme ça. C'est
là où ça devient complexe pour une famille aussi riche comme Dejavu. Ce
qui a été fait récemment, c'est une décision qui a été prise, c'est
d'avoir une sous-famille Dejavu qui s'appelle LGC, pour Latin Grec
Cyrillique pour éviter d'avoir tous les styles ensemble.

H: C'est d'enlever le latin des autres blocs.

N: Puisqu'ils sont quand même assez proches. Il y a du travail qui a été
fait au niveau des blocs Arabe et Hébreu.

P-L: il y avait des problèmes avec la baseline qui était pas la même.

N: Il y a des problèmes techniques et aussi des problèmes d'expression
de styles. L'avantage vraiment de Dejavu c'est que ça pousse le reste de
la « pile » logicielle autour des polices de caractères. Il y a certains
mainteneurs comme ceux de Pango (<http://www.pango.org>) ou de Firefox
(<http://www.mozilla.com/products/firefox/>) qui se disent, « Woouah les
typographes de Dejavu exposent encore nos bugs, ahh ».

P-L: Ca arrive assez assez fréquemment. Ha ha ha.

N: Ca prouve qu'il y a une activité assez formidable.

P-L: C'est vrai mais pour revenir aux locales (locl), j'avais beaucoup
de difficultés à me mettre dans la peau d'un typographe designer en
Grec, un typographe designer en Cyrillique, pour moi personnellement je
ne le lis pas, c'est du Chinois, je sais pas du tout ce que c'est. Pour
moi, c'est plus une icône qu'un caractère de texte.

N: C'est ça la difficulté en fait. Beaucoup de designers dans la SIL
(<http://scripts.sil.org/TypeDesign>) doivent s'entourer de gens qui
connaissent le script pour faire quelques chose de culturellement
approprié.

P-L: Parce que si on n'est pas natif dans le script, c'est difficile de
dire : est-ce que ça c'est une bonne légibilité dans le script ou est-ce
que c'est vraiment merdique.

N: C'est sûr.

H: Est-ce que vous avez des collaborateurs justement Arabes, Chinois ou
Japonais, des gens qui sont vraiment dans leur langue?

P-L: Des collaborateurs réguliers ou juste simplement des observateurs?

H: Les deux je dirais...

P-L: Probablement plus des observateurs.

N: Des gens qui rapportent des bugs sur la liste en disant «oh là! il
faut pas que ça soit comme ça !»?

P-L: Mais des typographes japonais chinois, je pense pas.

N: Des gens qui envoient des patchs?

H: Des gens qui dessinent carrément des caractères et autres?

N: Sur le site de Dejavu c'est indiqué plus ou moins leur status, si
leur travail a été repris via un merge ou s'il sont vraiment actifs.
C'est aussi ce qu'on essaie d'encourager dans le cadre de la logique de
l'Open Font licence (<http://scripts.sil.org/OFL>) avec le FONTLOG
(<http://scripts.sil.org/OFL-FAQ_web#00e3bd04>), cet espèce de changelog
dédié à la typographie et au design de polices, c'est vraiment de
décrire les modifications parce que la plupart des polices propriétaires
qu'on a, il y a pas de descriptions, c'est une release finale et voilà.

P-L: Tandis que dans Dejavu c'est un travail constant, mais ça crée
aussi des problèmes, si quelqu'un crée un texte avec la police, si tu
changes la taille des caractères, si tu changes l'espace entre les
lettres, ça va nécessairement tout changer. Donc est-ce qu'on fait un
changement pour le mieux dans un certain caractère ou on le garde pour
garder le support antérieur?

N: Ca c'est la décision du changement. Ce que je voulais dire, c'est le
fait que les changements soient décris très précisément c'est une bonne
chose, parce que souvent dans les fontes propriétaires, on ne sait pas
ce qui se passe. Voilà, c'est la nouvelle version si ça marche vous êtes
content mais si ça marche pas venez nous voir, et donc le changelog est
vraiment précis: c'est à dire que telle ou telle personne a ajouté tel
bloc ou a fait ce changement-là.

F: (I will ask my questions in English otherwise it takes too long, And
you speak French). This way of working is quite different than the way
than historically typographers have worked. There seems to be a history
of say a lonely man working on their internal piece, and the way you
describe the process of designing Dejavu is completely opposite. So, how
do you think this is visible or legible in the typeface itself or in the
practise of doing typeface design?

N: Il y a un fait fondamental, je pense, c'est la vitesse à laquelle les
modifications sont faites. Mais je sais qu'en discutant avec Victor
Gaultney (<http://www.sil.org/~gaultney>), il y a des expérimentations
qui ont été faites, en tous cas dans les écoles de dessin de polices, de
typographies parfois ils mettent comme ça des étudiants ensemble en
disant « travaillez sur une police et ce soir vous nous montrerez ce que
vous avez fait ensemble ». Mais le fait d'utiliser ça de manière
distribuée via un gestionnaire de révision de contrôle, un canal IRC ou
une mailing-list c'est quelque chose qui se fait que depuis quelques
mois, quelques années, avec Dejavu et puis d'autres projets que la SIL
lance (<http://scripts.sil.org/OFL_fonts>). Mais c'est vrai
qu'initialement c'est quand même un art personnel, élitiste.

P-L: Mais quand même, j'ai lu assez souvent que les typographes, il y a
en plusieurs qui ne sont pas vraiment excellents pour faire l'espace
entre les lettres, mais ils sont très bons pour faire le lettrage.
Certains vont faire le lettrage, un autre va aider à faire l'espacement
entre les lettres. Donc c'est souvent un travail collaboratif mais
peut-être pas au même niveau que dans Dejavu.

N: Pas commun, c'est-à-dire, je fais mon truc, je te le passe et tu fais
ton truc.

H: C'est pas une fragmentation des tâches?

P-L: C'est plus une ligne de montage que de la collaboration.

N: D'accord, je vois.

F: I'm also curious, because many type designers if you hear them speak
about their typefaces, they link it - consciously or subconsciously to
handwriting - to expression of let's say the style of a person, in a
way, like the way something is drawn, which is a very individual
expression, and then if you think about the way you work on a typeface,
you completly blow that up in a way. Or not?

N: Je pense qu'on retrouverait les styles communs des différents
contributeurs quand même, c'est un peu la transparence d'un système de
gestion de sources, on peut rajouter quelque chose mais les autres
peuvent aussi le modifier aussi donc il faut qu'il se crée une certaine
culture des modifications. C'est pour ça qu'il y a ce concept de
roadmap, je ne pense pas qu'on perde vraiment la partie style.

F: Non, ce n'est pas perdre, ce qui m'intéresse c'est le changement dans
la pratique de faire une lettre, de faire une courbe ensemble.

P-L: Mais il y a quand même le style du caractère qui s'impose aussi
quand tu fais ce que tu fais, tu peux pas dire moi je fais le caractère,
un script d'une certaine façon, dans le fonds c'est un peu comme ce
qu'on a vu dans le documentaire Helvetica
(<http://www.helveticafilm.com/>) hier soir. Il y en avait un qui disait
qu'il y avait une espèce de systèmisation du caractère, donc les courbes
et toute les parties du lettrage vont avoir une connotation dans une
autre lettre. Parce que sinon si tu ne fais pas ça comme ça évidemment,
ça va avoir l'air dépareillé.

...

N: Ca serait intéressant de voir un fichier README d'une police libre
comme Gentium (<http://scripts.sil.org/Gentium>), pour voir quels sont
les conseils qui sont donnés à ceux qui veulent envoyer des patchs, en
terme de formats, en terme de style, en terme de recherche, etc. Le
designer quand même reste maître, même si c'est toujours un projet
ouvert, il reste maître de la légitimité d'un patch.

F: Maître?

H: Et vous avez des formations de typographie ou de graphisme?

N: Non, pas vraiment, mais ça m'intéresse.

P-L: Je suis designer graphique moi-même, mais j'ai pas eu de formation
académique, mais c'est sûr que je lis beaucoup, probablement plus que la
majorité des étudiants de design. Dans le fond, c'est sûr que j'aimerais
avoir une éducation en tant que telle mais en travaillant on en apprend
aussi.

N: C'est quand même beaucoup de choses à apprendre...

P-L: Mais c'est dur d'apprendre seul et de s'établir soi-même un plan
d'apprentissage, d'avoir d'autres personnes qui apprennent avec toi, il
faut que tu trouves tout toi-même.

P-L: Mais dans un plan communautaire c'est plus facile d'évoluer.

N: C'est pour ça qu'on a comme projet avec Dave Crossland
(<http://understandingdesign.co.uk/>) et d'autres de rendre public les
compétences par rapport à la typographie, ce qui était enseigné dans
quelques écoles très très prestigieuses par une poignée de gurus, il
faudrait que ça soit disponible plus largement.

H: Est-ce qu'il y a une entraide comme ça entre les designers sur les
techniques de dessin, sur la manière de procéder? Graphique, je dirais,
spécialement graphique?

P-L: C'est sûr qu'actuellement tout le monde à sa façon de faire, son
procédé, si vous allez sur Typophile, les gens sont très ouvert pour
aider les débutants qui commencent en design typographique, si vous avez
des questions sur n'importe quoi, eux ils sont là pour vous aider. Il y
a des professionnels qui travaillent chez Adobe, qui ont travaillé sur
la spécification OpenType eux-mêmes et qui peuvent vous dire exactement
ce dont vous avez besoin.

N: Il faut juste savoir poser sa question de la bonne façon.

H: En respectant leur hiérarchie?

N: Non, non un peu de diplomatie, c'est normal.

P-L: Le domaine typographique est encore beaucoup dans l'espèce de
mentalité apprenti/maître, donc c'est souvent l'apprenti qui essaye de
se mettre au niveau du maître.

F: For me, as I look at open source projects, and the different ways
collaboration is organised, I find this again, this contrast with this
stark hierarchy of master and...

P-L: Mais je crois pas que ça soit une hiérarchie explicite, mais plutôt
une hiérarchie implicite, souvent celui qui contribue le plus c'est
aussi celui lui qui connaît le plus donc c'est lui qui va aider le plus.

N: C'est la méritocratie quelque part, c'est aussi un plaisir pour celui
qui connaît les choses de les partager. Mais c'est vrai qu'il y aura
toujours ceux qui sont très bons, qui connaissent bien leur domaine et
qui ont beaucoup d'expérience et ceux qui commencent juste. Mais il y a
un échange. Il y a un plaisir d'échanger les compétences et les
connaissances.

H: Et si tu es dans une tradition humaniste, d'effacement de prétention
graphique pour un intérêt commun...

N: Pour moi personnellement il y a un but: qu'il y ait plus de systèmes
d'écriture qui soient disponibles, que la qualité soit là, que les
barrières qui empêchent certaines personnes d'accéder à l'écrit et à ce
que l'écrit peut donner comme indépendance, comme autodétermination,
etc, que ces barrières puissent s'effondrer. Il y a des recherches qui
ont été faites en terme de lisibilité, par rapport à ceux qui apprennent
à lire et à écrire par exemple. Plus la fonte est agréable à lire, plus
les gens ont envie d'apprendre à lire. Notamment dans les contextes ou
c'est des adultes qui apprennent à lire quand il y a des campagnes
d'alphabétisation par exemple dans les pays en voie de développement. Il
y a vraiment une recherche, si on fait ça avec telle fonte, les résultat
seront vraiment médiocres mais si on fait ça avec une autre fonte, il y
aura un intérêt, il y aura une passion, il y aura beaucoup plus de gens
qui réussiront l'examen d'alphabétisation. Un des projets de la SIL
c'est une police dédiée justement à l'alphabétisation, qui s'appelle «
Andika »
([http://scripts.sil.org/Andika](http://scripts.sil.org/Gentium)) c'est
du Swahili et ça veut dire « écrire ». Il y a une vrai dissociation qui
est faite entre certains caractères qui sont peut-être trop proches les
uns des autres surtout quand on commence à lire et à écrire il faut
qu'il y ait le moins d'ambiguïté possible. C'est un design original mais
c'est vraiment une fonte avec une richesse suffisante pour que ça soit
disponible dans des langues qui utilisent des scripts complexes,
certaines langues africaines aussi où il y a ce type de glyphe. Donc les
designers attendent des spécialistes en alphabétisation d'avoir des
retours pour améliorer encore le tir. C'est aussi une fonte qui a des
variantes, comme ce qui commence à être fait aussi je crois dans Dejavu
pour avoir des variantes en fonction du contexte. Parce qu'il y a des
gens qui préfèrent un certain type de « a » avec un certain style, etc.

P-L: Donc c'est des « alterns »

N: Double story a, single-story a. Les ligatures, la manière dont les
chiffres sont affichés là. Vraiment dans un but d'alphabétisation
initiale mais comme c'est un design original il y a beaucoup de gens qui
commencent à l'utiliser aussi pour du design comme ça.

H: Comme fonte de travail?

N: Oui, c'est une Sans Serif (<http://fr.wikipedia.org/wiki/Sans-serif>)
qui est un peu le pendant de Gentium, parce c'est fait par les mêmes
designers. Il y a eu moins d'efforts par rapport aux inscructions pour
l'écran. C'est plus directement pour l'impression mais bon si les
linguistes peuvent trouver ça agréable aussi quand ils travaillent sur
l'écran, c'est mieux. Mais ça prend plus d'énergie pour les instructions
etc. Et puisque que c'est libre les gens peuvent y contributer s'il en
ont besoin.

Je sais que certains leader de Déjavu réfléchissent aussi à la
légitimité de passer à l'Open Font licence, mais ça ne s'est pas encore
fait parce que en amont, Vera est encore sous ce copyright-là. Mais en
fait on est en train de négocier, en tous cas il y a une réflexion qui
se fait avec Bitstream et GNOME, pour qu'un nouvelle version de Vera sur
laquelle ils ont travaillé sorte sour license OFL. Ensuite, comme Dejavu
est un dérivé, les développeurs, les designers se rassembleront et
voteront pour voir s'ils passent à l'OFL ou pas, parce que actuellement
en terme de license il y a Vera et les dérivés de Vera sont licence Vera
plus domaine public ou plus quelque chose d'autre. C'est pas vraiment
défini quel est le statut des dérivés. Il y a vraiment une richesse sur
laquelle on peut bâtir, mais on ne sait pas trop selon quelles règles.
c'est un peu le flou. Donc l'idée de l'OFL c'est de clarifier la manière
dont les dérivés fonctionnent. Le designer original de Vera s'appelle
Jim Lyles (<http://www.myfonts.com/person/lyles/jim/>), on est en
contact avec lui.

F: C'est donc c'est lui qui va décider?

N: Oui lui et sa boîte et ses directeurs. D'ailleurs dans la
documentation qui est livrée avec chaque nouvelle release, chaque
nouvelle sortie de Dejavu, on voit dans les statistiques de progression
ce qui est original, ce qui a été fait par le designer de Bitstream et
ce que les nouveaux designers ont ajouté. Donc on voit la progression.

H: Donc il y a un archivage à chaque fois?

N: Oui c'est vraiment décrit. Il y a peu de projets sont aussi décrits.

\[...\]

Là voilà le changelog, donc chronologiquement inversé. On voit, la
première version qui a été donnée dans le cadre du travail à Reading, à
l'Université de Reading, voilà quelques bugs qui sont réglés. C'est
aussi la logique de dire « voilà les problèmes qui ont été réglés. Il
existe des problèmes. » C'est pas comme beaucoup de logiciels
propriétaires qui disent « il y a jamais eu de problèmes. Mais si vous
achetez la version suivante il y aura plein de nouvelles fonctionnalités
» et on vous dit pas tout ce qui a été corrigé. Et là, la source aussi
qui est mise avec, ce qui est aussi important pour les polices libres.
La définition d'une source pour une police c'est quelque chose d'assez
vaste, ça peut être la base de donnée des glyphes, ça peut être les
comportements contextuels, OpenType ou Graphite
(<http://graphite.sil.org>), ça peut être les instructions, ça peut être
un guide pour le design, c'est-à-dire voilà les choix qui ont été faits,
ça peut être de la documentation des scripts pour la construction, pour
les statistiques, etc. Voilà aussi « Fix some duplicates» etc. Voilà: «
Anyone can create their own modified version ».

F: « using a different name »...

N: L'idée c'est de permettre le branchage: il y a le tronc commun, plein
plein de branches peuvent pousser à partir de ce tronc-là, mais il ne
faut pas que les branches se prennent pour le tronc. C'est possible
qu'une branche puisse avoir plus de fonctionnalité que le tronc mais il
ne faut pas que les choses se mélangent. Que les gens qui choisissent
des fontes puissent bien identifier d'où ça vient. D'où l'intérêt aussi
du changelog, c'est tout dans la description et dans la transparence.

F: Mais il y a déjà des branches?

N: Oui oui, il y a déjà plusieurs branches. Il y a des gens qui ont
travaillé sur une version Hébreu, il y a des gens qui ont travaillé sur
des langues du Népal je crois, des langues Indiennes aussi, en partant
du design de Gentium.

F: So the original design remains but then other language sets are
developped. Did you see say design versions, for example people that
have adjusted all glyphs or decided to make it like Harrison did a
Courrier or Sans-serif, a kind of style changes? C'est possible de faire
ça?

N: Oui, c'est tout-à-fait possible, ça serait un changement plus
complet, c'est-à-dire qu'on pourrait prendre quelque chose d'existant et
repartir plus ou moins de zéro.

P-L: Tant qu'à y être, c'est aussi bien de redémarrer à partir de zéro
mais avec une autre idée de ce qu'on peut faire avec cette police.

N: Si le style est vraiment différent c'est plus vraiment un dérivé.
L'idée c'est aussi de pouvoir aussi peut-être réutiliser les scripts de
statistiques et de construction, les choses qui font partie de cette
source de fonte qui peuvent aussi être re-utilisées. Voilà les conseils
qui sont donnés aux contributeurs, c'est dit que les branches peuvent
exister et que le mainteneur original va continuer à travailler sur le
tronc, « develop the canonical version », c'est un peu ça. « We warmly
welcome contributions » voilà les conseils qui sont donnés, en terme de
format, pour envoyer un patch, en terme de source, voilà qu'est-ce qui
est recommandé. Pour les attributions aussi, ça c'est quelque chose qui
va plus dans le coeur du mécanisme de license, on a le droit d'ajouter
son copyright aux modifications que l'on fait. On hérite le copyright du
tronc initial, on peut rajouter le bout qui couvre sa contribution
personnelle, bien sûr on n'enlève pas le copyright ancien, mais si on
veut reverser quelque chose au tronc ça se fait selon les règles de
celui qui maintient le tronc. Dans le cas de Gentium, par exemple, le
choix a été fait de remettre le copyright au nom de l'ONG en question.
Bon les gens décident au coup par coup. Et donc voilà quand c'est dit
là, c'est comme le fichier plans du projet Dejavu, quels sont les trucs
sur lesquels on a déjà plus ou moins travaillé, si c'est déjà prêt c'est
pas vraiment utile que vous y consacriez du temps puisque c'est déjà
fait on va peut-être utiliser ce qu'on a déjà. Mais voilà ce sur quoi on
n'a pas encore travaillé, voilà si ça vous intéresse, travaillez dessus,
posez-nous vos questions, et on l'intégrera peut-être.

Ici on parle du travail sur le Cyrillique par exemple, un petit
avertissement aussi: le format peut changer, le mécanisme de
construction peut aussi changer. Il y a aussi la partie Acknowlegements,
une espèce de liste qui peut grandir et qui est structurée. Savoir qui a
fait quoi, qui a contribué et où et d'avoir les détails. C'est dans la
même logique de ce qu'on disait hier dans la présentation pour les
métadonnées, c'est utile pour un designer d'exposer ses données, de
laisser un peu sa trace quoi. Ce qu'on veut faire c'est que pour les
utilisateurs finaux on puisse aller facilement sur le site du designer,
sur le site de son organisation peut-être, et de voir ce qu'il a fait
dans ce cadre-là. Le fait d'apparaître c'est une bonne chose pour le
designer, dans la logique de ne pas garder les choses secrètes et sous
scellé, le fait de faire les choses en transparence c'est aussi
bénéfique, c'est de la bonne pub, la construction d'une bonne
réputation.

*Transcrit par Nicolas Spalinger avec quelques corrections mineures pour
clarifier les phrases et ajout des URLs pour un meilleur contexte et une
meilleure compréhension du mouvement des fontes libres.  
Merci à Harrisson et Femke pour leur intérêt pour la typographie libre
et collaborative et leur travail de couverture du Libre Graphics Meeting
2007 à Montréal.  
Cette transcription est © 2007 Nicolas Spalinger et placée sous licence
CC-BY-SA.*
