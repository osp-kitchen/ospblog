Title: I think the ideas behind it are beautiful in my mind
Date: 2007-07-08 10:42
Author: Femke
Tags: Conversations, Type, Fontforge, LGM 2007
Slug: i-think-the-ideas-behind-it-are-beautiful-in-my-mind
Status: published

**Interview with George Williams, Fontforge developer**

> (...) I think the ideas behind it are beautiful in my mind -- and in
> some sense I find the user interface beautiful. I'm not sure that
> anyone else in the world does, because it's what I want, but I think
> it's beautiful. <small>(George Williams, May 2007)</small>

For those who prefer reading over listening, enjoy this text version of
the audio interview with George Williams (developer of
[FontForge](http://fontforge.sourceforge.net/)) we [published
earlier](?p=221).

<!--more-->  

<small>**OSP:**</small> [We](http://ospublish.constantvzw.org/)'re doing
these interviews, as we're working as designers on OpenSource

<small>**G:**</small> OK

<small>**OSP:**</small> With OpenSource tools, as typographers, but
often when we speak to developers they say "well, tell me what you
want," or they see our interest in what they are doing as a kind of
feature request or bug report

<small>**G:**</small> (laughs) Yes

<small>**OSP:**</small> Of course it's clear that that's the way it
often works, but for us it's also interesting to think about these tools
as really tools, as ways of shaping work, to try and understand how they
are made or who is making them. It can help us make other things. So
this is actually what we want to talk about. To try and understand a bit
about how you've been working on FontForge. Because that's the project
you're working on.

<small>**G:**</small> OK

<small>**OSP:**</small> And how that connects to other ideas of tools or
tools' shape that you make. These kind of things. So maybe first it's
good to talk about what it is that you make.

<small>**G:**</small> OK. Well... [FontForge](http://fontforge.sf.net/)
is a font editor.

I started playing with fonts when I bought my first Macintosh, back in
the early 80s *(actually it was the mid-80s)* and my father studied
textual bibliography and looked at the ways the printing technology of
the Renaissance affected the publication of Shakespeare's works. And
what that meant about the errors in the compositions we see in the
copies we have left from the Renaissance. So my father was very
interested in Renaissance printing (and has written books on this
subject) and somehow that meant that I was interested in fonts.

I'm not quite sure how that connection happened, but it did. So I was
interested in fonts. And there was this program that came out in the 80s
called Fontographer which allowed you to create PostScript and later
TrueType fonts. And I loved it. And I made lots of calligraphic fonts
with it.

<small>**OSP:**</small> You were... like 20?

<small>**G:**</small> I was 20\~30. Lets see, I was born in 1959, so in
the 80s I was in my 20s mostly. And then Fontographer was bought up by
MacroMedia who had no interest in it. They wanted FreeHand which was
done by the same company. So they dropped Fon... well they continued to
sell Fontographer but they didn't update it. And then OpenType came out
and Unicode came out and it (*Fontographer)* didn't do this right and it
didn't do that right... And I started making my own fonts, and I used
Fontographer to provide the basis, and I started writing scripts that
would add accents to latin letters and so on. And figured out the Type1
format so that I could decompose it -- decompose the Fontographer output
so that I could add my own things to it. And then Fontographer didn't do
Type0 PostScript fonts, so I figured that out.

And about this time, the little company I was working for, a tiny little
startup -- we wrote a web html editor -- where you could sit at your
desk and edit pages on the web -- it was before FrontPage, but similar
to FrontPage. And we were bought by AOL and then we were destroyed by
AOL, but we had stock options from AOL and they went through the roof.
So... in the late 90s I quit.

And I didn't have to work.

And I went off to
[Madagascar](http://info.bio.sunysb.edu/rano.biodiv/index.html) for a
while to see if I wanted to be a primatologist. And... I didn't. There
were too many leaches in the rainforest.

<small>**OSP:**</small> (laughs)

<small>**G:**</small> So I came back, and I wrote a font editor instead.

And I put it up on the web and in 'late 99, and within a month someone
gave me a bug report and was using it.

<small>**OSP:**</small> (laughs) So it took a month

<small>**G:**</small> Well, you know, there was no advertisement, it was
just there, and someone found it and that was *neat*!

<small>**OSP:**</small> (laughs)

<small>**G:**</small> And that was called PfaEdit (because when it began
it only did PostScript) and I... it just grew. And then -- I don't know
-- three, four, five years ago someone pointed out that PfaEdit wasn't
really appropriate any more, so I asked various users what would be a
good name and a French guy said "How 'bout FontForge?" So. It became
FontForge then. -- That's a much better name than PfaEdit.

<small>**OSP:**</small> (laughs)

<small>**G:**</small> Used it ever since.

<small>**OSP:**</small> But your background... you talked about your
father studying...

<small>**G:**</small> I grew up in a household where Shakespeare was
quoted at me every day, and he was an English teacher, still is an
English teacher, well, obviously retired but he still occasionally
teaches, and has been working for about 30 years on one of those
versions of Shakespeare where you have two lines of Shakespeare text at
the top and the rest of the page is footnotes.

And I went completely differently and became a mathematician and
computer scientist and worked in those areas for almost 20 years and
then went off and tried to do my own things.

<small>**OSP:**</small> So how did you become a mathematician?

<small>**G:**</small> (pause) I just liked it.

<small>**OSP:**</small> (laughs) "just liked it"

<small>**G:**</small> I was good at it. I got pushed ahead in high
school. It just never occurred to me that I'd do anything else -- until
I met a computer. And then I still did maths because I didn't think
computers were -- appropriate -- or -- I was a snob. How about that.

<small>**OSP:**</small> (laughs)

<small>**G:**</small> But I spent all my time working on computers as I
went through university. And then got my first job at JPL (Jet
Propulsion Laboratory) and shortly thereafter the shuttle blew up and we
had some (JPL is part of NASA) -- some of our experiments -- my little
group -- flew on the shuttle and some of them flew on an airplane which
went over the US took special radar pictures of the US. We also took
special radar pictures of the world from the shuttle (*SIR-A, SIR-B,
SIR-C*). And then our airplane burned up. And JPL was not a very happy
place to work after that.

So then I went to a little company with some college friends of mine,
that they'd started, created compilers and debuggers -- do you know what
those are?

<small>**OSP:**</small> Mm-hmm.

<small>**G:**</small> And I worked a long time on that, and then the
internet came out and found another little company with some friends --
and worked on HTML.

------------------------------------------------------------------------

<small>**OSP:**</small> So when, before we moved, I was curious about, I
wanted you to talk about a Shakespearian influence on your interest in
fonts. But on the other hand you talk about working in a company where
you did HTML editors at the time you actually started, I think. So do
you think that is somehow present... the web is somehow present in your
-- in how FontForge works? or how fonts work or how you think about
fonts?

<small>**G:**</small> I don't think the web had much to do with my --
well, that's not true. OK, when I was working on the HTML editor, at the
time, mid-90s, there weren't any Unicode fonts, and so part of the
reason I was writing all these scripts to add accents and get Type0
support in PostScript (which is what you need for a Unicode font) was
because I needed a Unicode font for our HTML product.

To that extent -- yes-s-s-s.

It had an effect. Aside from that, not really.

The web has certainly allowed me to distribute it. Without the web I
doubt anyone would know -- I wouldn't have any idea how to "market" it.
If that's the right word for something that doesn't get paid for. And
certainly the web has provided a convenient infrastructure to do the
documentation in.

But -- as for font design itself -- that (the web) has certainly not
affected me.

Maybe with this creative commons talk that Jon Phillips was giving,
there may be, at some point, a button that you can press to upload your
fonts to the [Open Font Library](http://openfontlibrary.org/) -- but I
haven't gotten there yet, so I don't want to promise that.

<small>**OSP:**</small> (laughs) But no, indeed there was-- hearing you
speak about cchost, that's the--

<small>**G:**</small> Mm-hmm.

<small>**OSP:**</small> software we are talking about?

<small>**G:**</small> That's what the Open Font Library uses, yes.

<small>**OSP:**</small> Yeah. And a connection to FontForge could change
the way, not only how you distribute fonts, but also how you design
fonts.

<small>**G:**</small> It -- it might. I don't know ... I don't have a
view of the future.

I guess to some extent, obviously font design has been affected by
requiring it (*the font*) to be displayed on a small screen with a low
resolution display. And there are all kinds of hacks in modern fonts
formats for dealing with low resolution stuff. PostScript calls them
hints and TrueType calls them instructions. They are different
approaches to the same thing. But that, that certainly has affected font
design in the last -- well since PostScript came out.

The web itself? I don't think that has yet been a significant influence
on font design, but then -- I'm no longer a designer. I discovered I was
much better at designing font editors than at designing fonts.

So I've given up on that aspect of things.

<small>**OSP:**</small> Mm-K, because I'm curious about your making a
division about being a designer, or being a font-editor-maker, because
for me that same definition of maker, these two things might be very
related.

<small>**G:**</small> Well they are. And I only got in to doing it
because the tools that were available to me were not adequate. But I
have found since -- that I'm not adequate at doing the design, there are
many people who are better at designing -- designing fonts, than I am.
And I like to design fonts, but I have made some very ugly ones at
times.

And so I think I will -- I'll do that occasionally, but that's not where
I'm going to make a mark.

Mostly now --

I just don't have the --

The font editor itself takes up so much of time that I don't have the
energy, the enthusiasm, or anything like that to devote to another major
creative project. And designing a font is a major creative project.

<small>**OSP:**</small> Well, can we talk about the major creative
project of designing a font editor? I mean, because I'm curious how --
how that is a creative project for you -- how you look at that.

<small>**G:**</small> I look at it as a puzzle. And someone comes up to
me with a problem, and I try and figure out how to solve it. And
sometimes I don't want to figure out how to solve it. But I feel I
should anyway. And sometimes I don't want to figure out how to solve it
and I don't.

That's one of the glories of being one's own boss, you don't have to do
everything that you are asked.

But -- to me -- it's just a problem. And it's a fascinating problem. But
why is it fascinating? -- That's just me. No one else, probably, finds
it fascinating. Or -- the guys who design FontLab probably also find it
fascinating, there are two or three other font design programs in the
world. And they would also find it fascinating.

<small>**OSP:**</small> Can you give an example of something you would
find fascinating?

<small>**G:**</small> Well. Dave Crossland who was sitting behind me at
the end was talking to me today -- he sat down -- we started talking
after lunch but on the way up the stairs -- at first he was complaining
that FontForge isn't written with a standard widget set. So it looks
different from everything else. And yes, it does. And I don't care.
Because this isn't something which interests me.

On the other hand he was saying that what he also wanted was a paragraph
level display of the font. So that as he made changes in the font he
could see a ripple effect in the paragraph.

Now I have a thing which does a word level display, but it doesn't do
multi-lines. (or it does multi-lines if you are doing Japanese
(*vertical writing mode*) but it doesn't do multi-columns then. So it's
either one vertical row or one horizontal row of glyphs.

And I do also have a paragraph level display, but it is static. You
bring it up and it takes the current snapshot of the font and it
generates a real truetype font and pass it off to the X windows
rasterizer -- passes it off to the standard linux toolchain (*freetype*)
as that static font and asks that toolchain to display text.

So what he's saying is "OK, do that, but update the font that you pass
off every now and then." And "Yeah, that'd be interesting to do. That's
an interesting project to work on." Much more interesting than changing
my widget set which is just a lot of work and tedious. Because there is
nothing to think about. It's just "OK, I've got to use this widget
instead of my widget." My widget does exactly what I want -- because I
designed it that way -- how do I make this thing, which I didn't design,
which I don't know anything about, do exactly what I want?

And -- that's dull.

For me.

<small>**OSP:**</small> Yeah, well.

<small>**G:**</small> Dave, on the other hand, is very hopeful that
he'll find some poor fool who'll take that on as a wonderful
opportunity. And if he does, that would be great, because not having a
standard widget set is one of the biggest complaints people have.
Because FontForge doesn't look like anything else. And people say "Well
the grey background --" It used to have a grey background, now it has a
white background "is very scary."

I thought it was normal to have a grey background, but uh... that's why
we now have a white background. A white background may be equally scary,
but no one has complained about it yet.

<small>**OSP:**</small> Try red.

<small>**G:**</small> I tried light blue and cream. One of them I was
told gave people migraines -- I don't remember specifically what the
comment was about the light blue, but

<small>**(someone from InkScape):**</small> Make it configurable.

<small>**G:**</small> Oh, it is configurable, but no one configures it.

<small>**(InkScaper):**</small> Yeah, I know.

<small>**G:**</small> So...

<small>**OSP:**</small> So, you talked about spending a lot of time on
this project, how does that work, you get up in the morning and start
working on FontForge? or...

<small>**G:**</small> Well, I do many things. Some mornings, yes, I get
up in the morning and I start working on FontForge and I cook breakfast
in the background and eat breakfast and work on FontForge. Some mornings
I get up at 4 in the morning and go out
[running](http://sbrunning.org/george/) for a couple of hours and come
back home and sort of collapse and eat a little bit and go off to yoga
class and do a pilates class and do another yoga class and then go to my
pottery class, and go to the farmers' market and come home and I haven't
worked on FontForge at all.

So it varies according to the day.

But yes I...

There was a period where I was spending 40, 50 hours a week working on
FontForge, I don't spend that much time on it now, it's more like 20
hours, though the last month I got all excited about the release that I
put out last Tuesday -- today is Sunday. And so I was working really
hard -- probably got up to -- oh -- 30 hours some of that time. I was
really excited about the change. All kinds of things were different -- I
put in python scripting, which people had been asking for -- well, I'm
glad I've done it, but it was actually kind of boring, that bit -- the
stuff that came before was -- fascinating.

<small>**OSP:**</small> Like?

<small>**G:**</small> I -- are you familiar with the OpenType spec? No.
OK. The way you... the way you specify ligatures and kerning in OpenType
can be looked at at several different levels. And the way OpenType wants
you to look at it, I felt, was unnecessarily complicated. So I didn't
look at it at that level. And then after about 5 years of looking at it
that way I discovered that the reason I thought it was unnecessarily
complicated was because I was only used to Latin or Cyrillic or Greek
text, and for Latin, Cyrillic or Greek, it probably is unnecessarily
complicated. But for Indic scripts it is not unnecessarily complicated,
and you need all those things. So I ripped out all of the code for
specifying strange glyph conversions. You know in Arabic a character
looks different at the beginning of a word and so on? So that's also
handled in this area. And I ripped all that stuff out and redid it in
the way that OpenType wanted it to be done and not the somewhat
simplified but not sufficiently powerful method that I'd been using up
until then.

And that I found, quite fascinating.

And once I'd done that, it opened up all kinds of little things that I
could change that made the font editor itself bettitor. Better.
Bettitor?

<small>**OSP:**</small> (laughs) That's almost Dutch.

<small>**G:**</small> And so after I'd done that the display I talked
about which could show a word -- I realized that I should redo that to
take advantage of what I had done. And so I redid that, and it's now,
it's now much more usable. It now shows -- at least I hope it shows --
more of what people want to see when they are working with these
transformations that apply to the font, there's now a list of the
various transformations, that can be enabled at any time and then it
goes through and does them -- whereas before it just sort of -- well it
did kerning, and if you asked it to it would substitute this glyph so
you could see what it would look like -- but it was all sort of --
half-baked.

It wasn't very elegant.

And -- it's much better now, and I'm quite proud of that.

It may crash -- but it's much better.

<small>**OSP:**</small> So you bring up half-baked, and when we met we
talked about bread baking.

<small>**G:**</small> Oh, yes.

<small>**OSP:**</small> And the pleasure of handling a material when you
know it well. Maybe make reliable bread -- meaning that it comes out
always the same way, but by your connection to the material you somehow
-- well -- it's a pleasure to do that. So, since you've said that, and
we then went on talking about pottery -- how clay might be of the same
-- give the same kind of pleasure. I've been trying to think -- how does
FontForge have that? Does it have that and where would you find it or
how is the...

<small>**G:**</small> I like to make things. I like to make things that
-- in some strange definition are beautiful. I'm not sure how that
applies to making bread, but my pots -- I think I make beautiful pots.
And I really like the glazing I put onto them.

It's harder to say that a font editor is beautiful. But I think the
ideas behind it are beautiful in my mind -- and in some sense *I* find
the user interface beautiful. I'm not sure that anyone else in the world
does, because it's what I want, but I think it's beautiful.

And there's a satisfaction in making something -- in making something
that's beautiful.

And there's a satisfaction too (as far as the bread goes) in making
something I need. I eat my own bread -- that's all the bread I eat
(except for those few days when I get lazy and don't get to make bread
that day and have to put it off until the next day and have to eat
something that day -- but that doesn't happen very often).

So it's just -- I like making beautiful things.

<small>**OSP:**</small> OK, thank you.

<small>**G:**</small> Mm-hmm.

<small>**OSP:**</small> That was very nice, thank you very much.

<small>**G:**</small> Thank you. I have pictures of my pots if you'd
like to see them?

<small>**OSP:**</small> Yes, I would very much like to see them.

![](documents/bowlweb.gif){: }

<small>Transcription: George Williams</small> :-)
