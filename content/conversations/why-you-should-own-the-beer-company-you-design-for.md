Title: Why you should own the beer company you design for
Date: 2007-12-21 01:26
Author: Femke
Tags: Conversations, Culture of work
Slug: why-you-should-own-the-beer-company-you-design-for
Status: published

**Interview with Dmytri Kleiner**

[![]({filename}/images/uploads/kleiner.JPG){: .float}]({filename}/images/uploads/kleiner.JPG)OSP
met [Venture
Communist](http://www.telekommunisten.net/venture-communism) Dmytri
Kleiner late night (thank you Le Coq for the soundtrack!) after his talk
[InfoEnclosure-2.0](http://data.constantvzw.org/site/spip.php?article140),
in a bar. We wanted to ask him what his ideas about peer production
could mean for the practice of designers and typographers.

<div class="clear">

</div>

<!--more-->Referring to [Benjamin
Tucker](http://flag.blackened.net/daver/anarchism/tucker/index.html),
[Yochai Benkler](http://www.benkler.org/) and [Marcel
Mauss](http://en.wikipedia.org/wiki/Marcel_Mauss), Kleiner explains how
to prevent leakage at the point of scarcity through operating within a
total system of worker owned companies. Between fundamentals of media-
and information economy, we talk about free typography and what it has
to do with nuts and bolts, the problem of working with estimates and why
the people that develop Scribus should own all the magazines it enables.

Also speaking is his wife Franziska Kleiner, editor for a German
publishing company.

→ [kleiner.mp3](http://ospublish.constantvzw.org/documents/kleiner.mp3)
\[29.10" | 16.7MB\]

<http://www.telekommunisten.net/>
