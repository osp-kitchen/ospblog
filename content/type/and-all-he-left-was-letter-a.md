Title: And all he left was letter A...
Date: 2007-02-28 11:14
Author: admin
Tags: Type, Piracy
Slug: and-all-he-left-was-letter-a
Status: published

*A little note on [Appropriation and Type
text](http://ospublish.constantvzw.org/?p=183).*

Gutemberg printed the first book with movable type in 1455. Maybe
copyright wasn't existing at the time, but there was, and there is
still, a fight for the first inventor of the printer machine in Europe.
Castaldi is alleged to have created movable type in 1442 (inspired by
the use of glass letters made in Venice and used by scribes to print
large first letter on page. Gutemberg knew that invention from Faust,
one of his student.)

One mythical story about the creation of printing press is a robbery.
Laurens Janszoon Coster was a important citizen of Dutch city Haarlem.
He discovered the movable type while playing with his grandchildren,
cutting pieces of wood in shapes of letters. Realizing the possibilities
of this, Coster improved the system with good ink and metal letters, and
soon started to print books. Business florishes, worksmen are employed.
But while the Coster family were at church on Christmas Eve 1441,
employee Johann broke into the printing office and stole presses and
types. He fled to Mainz, where he immediately sat an office and print.
Some version tells it was Johannes Fust, the partner of Gutenberg (and
who scrooge him later on).

[![coster.jpg]({filename}/images/uploads/coster.jpg)]({filename}/images/uploads/coster.jpg "coster.jpg")

Fust was formerly often confused with the famous magician Dr Johann
Faust, who, though an historical figure, had nothing to do with him.

Not to mention the fact that the first printed font was an imitation of
monastic script blackletter.

[![sample.jpg]({filename}/images/uploads/sample.jpg)]({filename}/images/uploads/sample.jpg "sample.jpg")

Read in "The Secret History of Letters", Simon Loxley, ed. I. B. Tauris,
2006
