Title: The status of Utopia
Date: 2008-05-20 08:45
Author: Femke
Tags: Type, Libre Fonts, Licenses
Slug: the-status-of-utopia
Status: published

![utopia]({filename}/images/uploads/utopia1.jpg)  
![utopia]({filename}/images/uploads/utopia2.jpg)  
![utopia]({filename}/images/uploads/utopia3.jpg)  
On October 11, 2006 Adobe granted members of the Tex User Group the
right to use, modify and distribute the Utopia typeface:

`Adobe Systems Incorporated ("Adobe") hereby grants to the TeX Users Group and its members a nonexclusive, royalty-free, perpetual license to the typeface software for the Utopia Regular, Utopia Italic, Utopia Bold and Utopia bold Italic typefaces, including Adobe Type 1 font programs for each style (collectively, the "Software") as set forth below.`  
<http://tug.org/fonts/utopia/LICENSE-utopia.txt>

Curiously enough, Adobe also granted TUG members the right to sublicense
the font so a few months later, Karl Berry (director of TUG) offered to
'any and all interested parties' the right to use Utopia:

`The agreement below gives the TeX Users Group (TUG) the right to sublicense, and grant such sublicensees the right to further sublicense, any or all of the rights enumerated below.  TUG hereby does so sublicense all such rights, irrevocably and in perpetuity, to any and all interested parties.`

Open Font Library listmembers are currently looking into how to clarify
the status of Utopia and possibly re-publish it under an Open Font
License. In the mean time you can already enjoy the font, which can be
downloaded in
[.pfb](http://www.postscript.org/FAQs/language/node39.html) format from
the TUG website:  
<http://tug.org/fonts/utopia>
