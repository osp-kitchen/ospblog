Title: The Open Font License
Date: 2006-02-27 21:34
Author: Femke
Tags: Type, Licenses
Slug: the-open-font-license
Status: published

\[check [The politics of
typography](http://ospublish.constantvzw.org/?p=54) before applying
SIL-OFL\]

![](http://ospublish.constantvzw.org/blog/wp-content/OFL_logo_rect_color.png){: width="88" height="31"}  
The SIL-OFL is a free license specifically developed for (multi-lingual)
fonts. These are the four freedoms guaranteed through the Open Font
License (similar to other Free licenses):  
*\* **Use**: the freedom to use font software for any purpose. (freedom
0)  
\* **Study and adaptation**: the freedom to study how font software
works, and adapt it to your needs (freedom 1). Access and rights to the
source code is a precondition for this.  
\* **Redistribution**: the freedom to redistribute copies of the font
software so you can help your neighbor (freedom 2).  
\* **Improvement and redistribution of modifications**: the freedom to
improve the font software and release your improvements (freedom 3), so
that the community benefits. Access and rights to the source code is a
precondition for this.*  
Applying an Open License to your font is made easy with the Open Font
License. See this
[FAQ](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&item_id=OFL-FAQ_web)
to find out how it works.
