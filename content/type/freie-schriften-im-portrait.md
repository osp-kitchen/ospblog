Title: Freie Schriften im Portrait
Date: 2006-07-15 15:00
Author: Femke
Tags: Type, Libre Fonts
Slug: freie-schriften-im-portrait
Status: published

For those who read German, type designer and -critic Gerrit van Aaken
published quite a few well-researched essays on familiar open source
typefaces such as **Gentium** and **Vera** plus a few surprises such as
**Kaffeesatz** and **Union** (not under an open licence, but distributed
by the Danish government for use in documents relating to Danish
culture. Interesting concept...)

[http://praegnanz.de/essays](http://praegnanz.de/essays/)
