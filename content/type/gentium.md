Title: Gentium
Date: 2006-02-12 19:28
Author: Femke
Tags: Type, Libre Fonts
Slug: gentium
Status: published

\[If you consider using Gentium, check [The politics of
typography](http://ospublish.constantvzw.org/?p=54)\]

![gentium
font]({filename}/images/uploads/2006/02/Gentium.png)

"Gentium was driven by the need for a free, attractive, legible,
high-quality font for extended Latin (and Greek and Cyrillic) use.
Nothing else was available that was suitable for publishing use, so I
decided to give it a try." (from: [interview with Victor
Gaultney](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&item_id=Gentium_interview),
designer of Gentium)
