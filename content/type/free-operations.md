Title: Free Operations
Date: 2007-11-02 11:56
Author: Femke
Tags: Type, Print Party, Workshops + teaching
Slug: free-operations
Status: published

**2 day workshop**  
Werkplaats Typografie, Arnhem (20 + 21 November, 2007)

What permutations between typeface, typesetting and text can you
imagine? How to design through scripting and can you read differently
with computer manipulations?

Full brief: <!--more-->

> Design could not exist without a larger eco system of cultural works
> around it. Even the most commercial of design practices are nourished
> by historical or contemporary artworks, films, photographs, lay-outs,
> images, ideas produced by others; at the same time, design feeds into
> culture.
>
> For culture to grow, it seems counter productive to fix it in place
> with restrictive copyright licenses that prevent good ideas from what
> they do best: to spread. Open content licenses such as the Free Art
> License, Creative Commons, General Public License were invented to
> subvert intellectual property laws in order to keep culture in
> circulation. They are an unfortunately necessary asset when 'public
> domain' has become the exception to the rule.
>
> OSP (Open Source Publishing) is a small design research team from
> Brussels, involved in various aspects of the publishing cycle. From
> typography to editorial work, OSP tests out in practice how graphic
> design could work differently, using Free Software and copyleft
> licenses. We try to think out loud about what other tools are possible
> and what is possible with other tools; to demonstrate to ourselves and
> our colleagues the potentialities and limitations of Free Software,
> how they can be tools to think with and how they can be put to work in
> professional design environments.
>
> For the \*Free Operations\* workshop, we will work exclusively with
> content that is in the public domain, with fonts that expressly allow
> for modifications and redistribution, and we will process those
> materials using open source tools.
>
> Point of departure is Project Gutenberg, the first and largest single
> collection of free electronic books. Including Max Havelaar, Flatland
> and Dracula, the project brings together many classic texts. Once
> retyped and corrected, they are than re-entered in to the public
> domain as digital files. Besides an immense and valuable library of
> literary works, The Gutenberg project makes a searchable database
> available of text files, paragraphs, words and letter combinations.
>
> What permutations between typeface, typesetting and text can you
> imagine? How to design through scripting and can you read differently
> with computer manipulations?
>
> \*Free operations\* starts with an installparty, adding FontForge and
> Inkscape to your harddrives. When these two pieces of software are
> strung together, they make other ways of doing design possible. We
> will learn how to use the command line or console to communicate
> differently with your computer, and experiment with new ways of
> processing text through pattern matching using regular expressions.
>
> Free Operations proposes a two day sampler of what Open Source methods
> could mean for design in general and typography in particular. It is a
> way to think about authorship, software and distribution in relation
> to your practice, and an invitation to explore new territory.
