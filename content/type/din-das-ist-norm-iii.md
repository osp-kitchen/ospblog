Title: DIN - Das Ist Norm - III
Date: 2007-06-25 14:38
Author: Harrisson
Tags: Type, Works, Design Samples, Libre Fonts, Standards + Formats
Slug: din-das-ist-norm-iii
Status: published

Schablonenschrift A - Din1451 - August 1949  
Inkscape drawing.

[![din1456\_malschablonen\_sample2.png]({filename}/images/uploads/din1456_malschablonen_sample2.png)]({filename}/images/uploads/din1456_malschablonen_sample2.png "din1456_malschablonen_sample2.png")
