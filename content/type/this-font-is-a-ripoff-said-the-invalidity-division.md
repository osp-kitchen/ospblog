Title: This font is a ripoff, said the Invalidity Division
Date: 2006-04-05 09:40
Author: nicolas
Tags: Type, Licenses
Slug: this-font-is-a-ripoff-said-the-invalidity-division
Status: published

*Invalidity Division*, sounds like science-fiction, doesn't it?
*Registered community design* is not bad either. Time to start a jargon
file...

> \[...\]this time the dispute is over fonts; specifically Segoe, one of
> the typefaces Microsoft wants to use in Vista <font color="gray">(the
> new Windows, "bringing clarity to your world")</font>. Microsoft filed
> its "registered community design" for the font back in January of
> 2004, paid the required fee, and everything was great—until December.
>
> Just days before the end of 2004, Heidelberger Druckmaschinen AG
> sought a "declaration of invalidity" from the Invalidity Division
> (yes, that is it's real name) of the Office for Harmonization in the
> Internal Market. As the owner of the Linotype brand, Heidelberger
> Druckmaschinen claimed that Microsoft's "new" font was a blatant
> ripoff of Linotype's own Frutiger LT 45 Light, which has been sold by
> the company for years. \[...\]
>
> "The typefaces of both designs have the same stroke thickness. The
> ratio from cap-height to descender height is equal. The proportion of
> character height to character pitch is identical. The type face in the
> specimen text does not show any differences."  
> So they threw Microsoft's application out and ordered the company to
> pay all the fees incurred by Heidelberger Druckmaschinen.

Read online:
[http://arstechnica.com/news.ar...](http://arstechnica.com/news.ars/post/20060404-6517.html)
