Title: Coming Soon: Print Party
Date: 2006-06-21 21:11
Author: Femke
Tags: News, Print Party
Slug: printing-party-coming-soon
Status: published

![P](http://ospublish.constantvzw.org/blog/wp-content/oneletter/_p_2.jpg "P"){: width="90" height="90"}
![P](http://ospublish.constantvzw.org/blog/wp-content/oneletter/_p_3.jpg "P"){: width="90" height="90"}
![P](http://ospublish.constantvzw.org/blog/wp-content/oneletter/_p_4.jpg "P"){: width="90" height="90"}
![P](http://ospublish.constantvzw.org/blog/wp-content/oneletter/_p_5.jpg "P"){: width="90" height="90"}  
Friday July 7 16:00: **Printing Party** at
[Quarantaine](http://www.quarantaine.biz) (Brussels), with Scribus
get-together and festive booklet-printing demo plus Open Cola cocktails.
More details in a few days!

<small>Find more P s with open content license at:  
<http://www.flickr.com/search/?q=p+oneletter&m=tags&l=deriv>  
(Advanced search &gt; Only search within Creative Commons-licensed
photos)</small>
