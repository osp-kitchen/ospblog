Title: A journey to Vietnam and an open call
Date: 2011-03-01 10:09
Author: Stephanie
Tags: News, LGM 2010, Vietnam, Workshops + teaching
Slug: a-journey-to-vietnam-and-an-open-call
Status: published

[![Hong-Phuc Dang inviting LGM in Vietnam during the closing session at
LGM 2010
Brussels]({filename}/images/uploads/lgm_hong-phuc.jpg "Hong-Phuc Dang inviting LGM in Vietnam during the closing session at LGM 2010 Brussels"){: .alignnone .size-medium .wp-image-5902 }](http://ospublish.constantvzw.org/news/a-journey-to-vietnam-and-an-open-call/attachment/lgm_hong-phuc)

At [LGM 2010 in
Brussels](http://ospublish.constantvzw.org/news/au-revoir-lgm-2010), we
met Hong-Phuc Dang, a young Vietnamese woman full of energy, willing to
organize events around Libre Graphics in Asia in addition to her
involvement in [FOSS/ASIA](http://fossasia.org/), a meeting around Free
and Open Source Software.

We can't believe it's been almost a year of planning and that the trip
is actually happening in a month!

We will be 5 to leave for this adventurous exchange in the Vietnamese
region of the Mekong, making a small tour of some cities, schools,
design studios, working with Asian designers on 2 projects. The first
one is to draw a typeface directly from the mix of our two cultures and
not from the Occidental one adapted to the Oriental one, as it is often
the case. The second project, using the first one of course, will be a
publication around cartography, geography, cultures and customs.

Hong-Phuc Dang and Mario Behling, the team we're joining there, are
organizing an exhibition of design works done with Free, Libre and Open
Source software. This is an open call, and you are more than welcome to
send your proposals to them: dhppat\[at\]gmail.com or
mb\[at\]mariobehling.de.
