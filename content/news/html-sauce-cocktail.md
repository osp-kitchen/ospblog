Title: HTML sauce cocktail
Date: 2015-03-19 15:10
Author: Colm
Tags: News
Slug: html-sauce-cocktail
Status: published

We just finished a great workshop week [@HEAR](http://hear.fr)
Strasbourg on the topic of printing HTML.

![](http://comgraph.hear.fr/wp-content/uploads/2015/03/bcb-1-1024x679.jpg){: .alignnone width="100%" height="auto"}

Printing HTML is not a new idea, but in the last few years we've been
compiling sets of tools and scripts to properly use HTML as a base for
proper layout. This idea has taken on several flavors over different
projects, the first serious one being the [Balsamine program for the
2013-2014 season](http://osp.kitchen/work/balsamine.2013-2014/).

Since that first successful experiment, the HTML print workflow has
extended and been used to control print outputs within
[Ethertoff,](http://osp.kitchen/tools/ethertoff/) and *in situ* used for
both the [Relearn 2013 publication](http://relearn.be/2013/index.html#)
and [V/J14 'Are you being Served?'](http://vj14.constantvzw.org/#) book.
Another successful and notable use is [~~*this* ye~~ars Balsamine
program](http://osp.kitchen/work/balsamine.2014-2015/), displaying all
sorts of crispy vector images along side of a more complex layout.

All this preamble culminates in the assembly of all these tools in a
[boilerplate starter tool](http://osp.kitchen/tools/html2print/), simply
named **html2print**. Hosted in parallel on [Git
OSP](http://osp.kitchen/tools/html2print/) &
[Github](https://github.com/osp/osp.tools.html2print) the kit aims to
introduce you to the idea of making printable documents from Html, CSS
and javascript.

![](https://raw.githubusercontent.com/HEAR/HTML_sauce-cocktail-workshop-OSP/master/Snapshots/1.png){: .alignnone width="100%" height="auto"}

We've found many advantages to working this way, so we made it the
object of this workshop in Strasbourg with the students from the
Communication Graphique department of the school.

Head on over to the [blog post on Sürkrüt (comgraph
blog)](http://comgraph.hear.fr/2015/03/html-sauce-cocktail/) and the
[schools GH](https://github.com/HEAR/HTML_sauce-cocktail-workshop-OSP)
to find all the fun creations and source files.

Miam!
