Title: decodeunicode.org project
Date: 2006-03-22 12:34
Author: Harrisson
Tags: News, Type, Unicode
Slug: decodeunicodeorg-project
Status: published

![decodunicod.png](http://ospublish.constantvzw.org/blog/wp-content/decodunicod.png){: #image79}

<http://decodeunicode.org>

> Is an independent online-platform for digital type culture, initiated
> by the Department of Design at the University of Applied Sciences in
> Mainz, Germany.
>
> The project is supported by the Federal Ministry of Education and
> Research (BMBF) and has the objectives of creating a basis for
> fundamental typographic research and facilitating a textual approach
> to the characters of the world for all computer users.
>
> The website uses freely available data of the Unicode Standard 4.0.1 ©
> The Unicode Consortium, 1990 – 2003, especially UnicodeData.txt
>
> (Some rights reserved. All texts and images on the website are
> protected by a Creative Commons License. You may reuse and
> redistribute them for any purpose other than commercial use.)
