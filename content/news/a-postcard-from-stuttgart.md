Title: A Postcard from Stuttgart
Date: 2009-05-30 09:27
Author: Femke
Tags: News, Digital drawing, Reading list
Slug: a-postcard-from-stuttgart
Status: published

[![p1050932]({filename}/images/uploads/p1050932.jpg "p1050932"){: .alignright .size-medium .wp-image-2797 }]({filename}/images/uploads/p1050932.jpg)

I'm in Stuttgart for a week to teach a
[workshop](http://snelting.domainepublic.net/corsets). In the school
library, I read Eye Magazine. <!--more-->

The current issue *[A New Golden
Age?](http://www.eyemagazine.com/feature.php?id=166&fid=746)* has many
familiar observations about the way typography is going through exciting
times. Sybille Hagman brings up typography as teamwork and the need to
divide tasks between experts. Paul Carlos: 'Unexpected intersections and
amalgations' and Nicole Dotin: 'The web (...) pushed all these
innovations further than they would have gone on their own' but the
[Free Font
Movement](http://lists.w3.org/Archives/Public/www-font/1998JanMar/0026.html)
as we know it, is sadly nowhere mentioned. Editor Deborah Littlejohn
seems only one small step away though from speaking about Libre Fonts,
when she summarizes typedesigners' view on the situation:

> Their description of our time as one of consolidation, efficiency,
> historicity and neutrality show a field still grappling with the
> ramifications of typography's democratised tools. From this
> perspective, we get a sense of why Modernism's default aesthetic
> habits die hard. Considering the reverberating 'echo chamber' created
> by constant software upgrades and new utility platforms, type
> designers are out of breath just keeping up.

\[caption id="attachment\_2799" align="alignright" width="400"
caption="April Greiman, sketch for: Does it make sense? (Design
Quarterly \#133, 1986) "\][![April Greiman, sketch for: Does it make
sense? (Design Quarterly \#133,
1986)]({filename}/images/uploads/p1050938.jpg "p1050938"){: .alignright .size-medium .wp-image-2797 }]({filename}/images/uploads/p1050938.jpg)\[/caption\]

I don't think it is nostalgia only, that blows me away when
re-discovering April Greimans early experiments with the mutable scale
of digital imagery. I haven't seen her book *Hybrid Imagery* since I was
in school myself I think.

Greiman is a Californian designer who started to explore digital imagery
as early as 1984. *Hybrid Imagery* is an account of her love affair with
'The Mac' as she affectionally calls the blend of software and hardware
she was discovering at the time. Naive, of course, but some of her
observations resonate:

> The "undo" function allows you to take back something you just did,
> without a trace; or, with another click, restore it. The traditional
> way of thinking would call this a great way to correct mistakes. In
> fact, you learn to think of it as means to attempt them.

> You are always working in a state of visual perfection. The first word
> of your first vague thought is in high-resolution 12 pt. Univers 55
> type or whatever. Even raw beginnings have a finished look.

A presentation by [Aram Bartholl](http://www.datenform.de), later that
night at the Akademie, shows that 25 years later, we still are coming to
terms with similar questions of location and relation.
