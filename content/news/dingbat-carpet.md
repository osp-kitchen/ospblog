Title: Dingbat Carpet
Date: 2009-11-20 09:35
Author: OSP
Tags: News, dingbats, V/J12
Slug: dingbat-carpet
Status: published

![dingbat]({filename}/images/uploads/dingbat.jpg "dingbat"){: .alignright .size-medium .wp-image-3655 }  
<small>Ludi draws a giant 26FD Unicode dingbat (from the recent [5.2
character
additions](http://www.unicode.org/versions/Unicode5.2.0/#Character_Additions))</small>

More images: <http://gallery.constantvzw.org/main.php?g2_itemId=29200>
