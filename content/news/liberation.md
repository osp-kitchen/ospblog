Title: Liberation
Date: 2007-09-17 21:59
Author: Harrisson
Tags: News, Libre Fonts
Slug: liberation
Status: published

Thanks to Red Hat®, a 3 major new sets of fonts under GPL+exception
license.  
Named "Liberation" serif, sans and mono, those types are designed to
subset the hegemony of proprietary helvetica, times and monospace
courier new because metric compatible. It means distance between letters
are the same, lenght of documents won't change if helvetica is
substituted by liberation sans font.

> "Using these fonts does not subject your documents to the GPL--it
> liberates them from any proprietary claim"

Well cutted and with a very nice overall ambiance, the sans cut is
particularly interesting, and much much nicer than another substitute
for helvetica: the Arial. It is clearly affiliated to the Akzident
Grotesk, but looks more neutral in the use. The serif cut is very far
from the Times, and got formal similarities with the Vera Serif. But it
gives a good contemporary, simple and elegant aspect to long readable
texts, as far we tested it.

[![300px-font\_comparison\_-\_liberation\_sans\_to\_arialsvg.png]({filename}/images/uploads/300px-font_comparison_-_liberation_sans_to_arialsvg.png)]({filename}/images/uploads/300px-font_comparison_-_liberation_sans_to_arialsvg.png "300px-font_comparison_-_liberation_sans_to_arialsvg.png")

[![450px-font\_comparison\_-\_liberation\_serif\_to\_times\_new\_romansvg.png]({filename}/images/uploads/450px-font_comparison_-_liberation_serif_to_times_new_romansvg.png)]({filename}/images/uploads/450px-font_comparison_-_liberation_serif_to_times_new_romansvg.png "450px-font_comparison_-_liberation_serif_to_times_new_romansvg.png")

[![425px-font\_comparison\_-\_liberation\_mono\_to\_courier\_newsvg.png]({filename}/images/uploads/425px-font_comparison_-_liberation_mono_to_courier_newsvg.png)]({filename}/images/uploads/425px-font_comparison_-_liberation_mono_to_courier_newsvg.png "425px-font_comparison_-_liberation_mono_to_courier_newsvg.png")

[The wikipedia article](http://en.wikipedia.org/wiki/Liberation_fonts)

**rpm font files and true type font files:**  
<https://www.redhat.com/promo/fonts/>
