Title: Design for the common good
Date: 2006-11-25 00:08
Author: Femke
Tags: News
Slug: design-for-the-common-good
Status: published

This week the [Dutch Design Foundation
Premsela](http://www.premsela.org) organised 'For the common good', a
conference around [Pierre
Bernhard](http://www.stedelijk.nl/oc2/page.asp?PageID=1475) (Grapus, Ne
Pas Plier), who has been awarded the prestigious Erasmus price.

Design critic Hugues Boekraad introduced the work of Bernard with a plea
for reflexive design; in his perspective designers have the
responsibility to build bridges between the private and the public,
between the particular and the general. Communication between government
and citizens, cultural production, cultural heritage... Pierre Bernhard
than showed early Grapus designs for and with the French communist
party, followed by a more recent Ne Pas Plier project on children's
human rights. From the third presentation by
[Thonik](http://www.thonik.nl/) (Thomas Widdershoven), a radical
re-design of the Dutch [SP](http://www.sp.nl) (socialist party), it was
clear that times have changed. Using advertising strategies in bold red
typography, and avoiding complicated messages, their design can only be
measured in terms of succes. [VanDeJong](http://www.vandejong.nl/) ended
the afternoon with presenting a long term educational project
[Anno](http://www.anno.nl/), aimed at making Dutch history accessible
for everyone. "It doesn't teach but it gives a fun experience".

![log SP
old]({filename}/images/uploads/logo_sp.gif){: #image149}
**  =**
![tomaat.png]({filename}/images/uploads/tomaat.png){: #image149}  
<!--more-->  
HB = Hugues Boekraad  
PB = Pierre Bernard  
DK = Dingeman Kuilman (managing director of Premsela)  
CS = Carine de Smedt (design theoretician (?))  
TW = Thomas Widdershoven  
PJ = Pjotr de Jong

(caution - these are my notes -- not a transcription!)

DK: Piere, could you respond to the work of Thonik and VanDeJong... do
you think there are differences, comparisons to make?  
PB: It is hard to compare -- time is different -- what we did than is
today not possible; we need to think quick, and we risk not to think
deep enough. To go to the symbol quickly is necessary -- we are under a
lot of narration (tv, advertisement) -- with these kind of images you
can stop those stories and start to think for yourself.  
DK: It seems public and private communication strategies are
exchangeable. Is that true?  
PB: I think it was always like that... what we as Grapus tried to do
(but those were different times) was to develop ideas between one
election and another. The goal was not only to win. Deep politics is not
only about electing people in parliament, but also to understand what
the reality of the everyday is about.  
HB: In the campaign for the SP I think there is an absence of floating
images. It is all typography and code with a symbol added. I like the
symbol, turning the tomato from a negative symbol into a positive one...
but in Grapus' work you'll always find a zone of free imagination. A
space to identify with. This is not possible with symbols; those images
are already stabilized.  
TW: I agree with both your analyses... Now I will need to convince my
client to want this too ...  
PB: Well, I think the general aestetics of the red ... it is red and
white and forms ... functions in a way as a floating image too.  
HB: I was referring to the horizontal quality of the communication. PB
works with reciprocal exchange -- where the language of the city hall
and the political party are mixed -- to bring in the language of the
people you are talking to, in to the rhetoric of the design. Using the
rhetoric of ordinary life.  
DK: Is it political work?  
CS: It is, all of the work always is. It is a great demonstration of the
power of graphic design ... what it can be. Cultural commissions are
currently treated like commercial. It is obvious that there is a
difference between commercial and public commission. The question is...
is the public domain not everywhere? If everything is politic... can we
equal it to the every day life... can we apply those ideas to every
field?  
PJ: Everyone should take their responsibility... not just the
government. This time of receding governments is a great chance for
other institutions to play a role.  
DK: is there a difference between commercial and 'public' clients  
PJ: Most of our clients are public institutions ... our way of working
is the same... we are looking for the real message...  
HB: I do not agree. In a democracy the difference between public and
private is holy. The public domain is the space of the law. The space of
the common, of language. It requires a different way of working. In
Holland ... the succes of graphic design ... developed in state owned
corporations. Did they really differ in logic, aestetics? are they
different? Or did it introduce the management models of the private
sphere in public institutions... made way for later privatization of the
public sphere? On a theoretical and practical level you need to make a
sharp distinction.  
DK: what is the relevance of dialogue for a graphic designer?  
PB: Communication is a dialogue. With mass media... you need a lot of
technique... and the main actor is the market. Even when they work with
a graphic designer... it can be difficult. They think as commercial
people ... as if their visitors are clients ... they are sure it is a
good model. It is only for people who know. It is important to believe
in the public, that they are able to understand messages and to feel the
same things as we are able to feel. In commercial terms, the only
possible response is buying, not a dialogue.  
DK: How do you balance dialogue and propaganda?  
CS: in France we need to look at the great models we had. To understand
the process... society has changed... we have to find new ways... we
must fight for graphic design to exist ... a thinking form of graphic
design  
TH: we always tried to put commercial values in the cultural...
branding, but playing with it. We try to bring in cultural values in
'commercial' fields. If the propaganda works, than you create space to
play.  
PB: In the Netherlands you have a chance to make things happen, because
you have a graphic design culture... we had great postermakers once...
but advertising is dominant since the 70's. What you describe ... the
equilibrium between propaganda and information ... it is in fact graphic
design as Popart.
