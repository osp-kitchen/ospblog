Title: Impossible choice
Date: 2010-10-10 22:35
Author: Femke
Tags: News, Bugs, competition, Tools
Slug: impossible-choice
Status: published

Packt-publishing, big on books about Open Source, now wants us to '[Vote
for our favorite Open Source Graphics Software to
win](https://www.packtpub.com/open-source-awards-home/vote-open-source-graphics-software)'.
It is a clever campaign I guess: data about popular applications should
be useful in deciding what books will sell. Packt gives away one
proprietary e-reader to a random voter and I only hope there is also a
generous donation to the winning projects (no mention of any on the
Packt site).  
What a strange idea that I would want to choose between Inkscape,
Blender, Scribus, Gimp and jmonkeyengine. How counterproductive to
assume competition between complementary projects. Imagine asking a
carpenter whether she likes her hammer better than her screwdriver?

![]({filename}/images/uploads/win.png "win"){: .alignnone .size-full .wp-image-5044 }
