Title: Document Freedom Day
Date: 2011-03-27 18:20
Author: OSP
Tags: News, campaigns, Standards + Formats
Slug: document-freedom-day
Status: published

[![]({filename}/images/uploads/Dfd_leaflet_front_bw-2011.png "Dfd_leaflet_front_bw-2011"){: .alignnone .size-full .wp-image-6026 }](http://documentfreedom.org/)

Next Wednesday March 30th = Document Freedom Day (DFD), a global day for
document liberation.  
Look here for events in your country:
<http://documentfreedom.org/2011/events/>
