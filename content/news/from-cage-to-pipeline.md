Title: from cage to pipeline
Date: 2010-11-14 10:57
Author: ludi
Tags: News, Tools, books, In the pipeline, LGM 2010, Printing + Publishing, Tools
Slug: from-cage-to-pipeline
Status: published

\[gallery link="file"\]

H E Y ! Libre Graphics Magazine \#1 is out !  
Bravo Ana, ginger, Ricardo + the whole team and participants.  
Follow the details about the production process and order soon your copy
from their
[blog](http://libregraphicsmag.com/libre_graphics_magazine_blog/progress_blog).
