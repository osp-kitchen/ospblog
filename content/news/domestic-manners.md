Title: Domestic manners
Date: 2006-03-16 10:54
Author: nicolas
Tags: News, Type, Libre Fonts
Slug: domestic-manners
Status: published

![Domestic
Manners](http://ospublish.constantvzw.org/blog/wp-content/domestic-manners.jpg)

<http://www.dustismo.com/>

Domestic Manners, by Dustin Norlander.

"This font is basically my handwritting. Why anyone would want to use my
crappy handwritting for anything, I can't say. It would be a good way to
forge a note from me I guess. Anyway, use it for whatever you want, its
released under the GPL so change it if you need to. The name come from
the book Domestic Manners of the Americans, by Fanny Trollope."

![20041024-Dustismo\_Roman.jpg](http://ospublish.constantvzw.org/blog/wp-content/20041024-Dustismo_Roman.jpg)

Dustin Norlander has also released a series of font under the GPL
license including Dustismo Roman, a standard serifed roman.

"It contains all the characters you should ever need, accents and
special characters. I created this to be used with linux, as there was
definitely a lack of quality fonts available for linux. Since then
Bitstream released their Vera serif and sans-serif typeface under an
open source style license--oh well. I like this one, its good for
whatever you need. Released under the GPL so go ahead and include it in
your own personal Linux variation."
