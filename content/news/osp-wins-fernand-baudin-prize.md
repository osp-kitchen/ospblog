Title: OSP wins Fernand Baudin Prize
Date: 2009-12-17 21:04
Author: OSP
Tags: News, Awards, context, Scribus
Slug: osp-wins-fernand-baudin-prize
Status: published

**Tracks in Electr(on)ic Fields**, published by Constant and designed by
OSP with ConTeXt and Scribus, has been awarded one of 9 prizes for

<p>
<center>
</p>
*The Most Beautiful Book of Brussels and Wallonia*

✵ ✯ ✵

<p>
[![Screenshot-vj10-cover-recto.pdf]({filename}/images/uploads/Screenshot-vj10-cover-recto.pdf-75x52.png "Screenshot-vj10-cover-recto.pdf"){: .alignright .size-thumbnail .wp-image-3709 width="75" height="52"}]({filename}/images/uploads/Screenshot-vj10-cover-recto.pdf.png)
[![page11]({filename}/images/uploads/page11-52x75.png "page11"){: .alignright .size-thumbnail .wp-image-3709 width="75" height="52"}]({filename}/images/uploads/page11.png)
[![page317]({filename}/images/uploads/page317-52x75.png "page317"){: .alignright .size-thumbnail .wp-image-3709 width="75" height="52"}]({filename}/images/uploads/page317.png)
[![page77]({filename}/images/uploads/page77-52x74.png "page77"){: .alignright .size-thumbnail .wp-image-3709 width="75" height="52"}]({filename}/images/uploads/page77.png)

</center>
</p>
More about the prize: <http://www.prixfernandbaudinprijs.be>

About the design and production process (also included in the
publication):

-   [The Making
    Of]({filename}/images/uploads/makingof.pdf)

Related posts:

-   Episode 1: [Designing with
    LaTeX](http://ospublish.constantvzw.org/works/designing-with-latex)
-   Episode 2: [Lions and
    Tulips](http://ospublish.constantvzw.org/tools/lions-and-tulips)
-   Intermezzo:
    [Entretemps-Ondertussen-Meanwhile](http://ospublish.constantvzw.org/tools/entretemps-ondertussen-meanwhile)
-   Episode 3: [Pelgrimage to
    Pragma](http://ospublish.constantvzw.org/tools/pelgrimage-to-pragma)

Sources: <http://ospublish.constantvzw.org/sources/vj10>

We are so proud!
