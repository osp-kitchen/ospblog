Title: Watch this thread: LGM site and logo proposal
Date: 2009-10-01 10:33
Author: OSP
Tags: News, LGM 2010, Watch this thread
Slug: watch-this-thread-lgm-site-and-logo-proposal
Status: published

[![sketch]({filename}/images/uploads/sketch.jpg "sketch"){: .alignright .size-medium .wp-image-3459 }]({filename}/images/uploads/sketch.JPG)

After heated debates, intensive collaborative working sessions and many
hesitations, OSP feat. [Alexandre Leray](http://alexandreleray.com/)
finally proposed a new identity for the upcoming [Libre Graphics
Meeting](http://www.libregraphicsmeeting.org/) to the Libre Graphics
community.

Since the next meeting will take place in in our home town Brussels, we
felt that it was important to at least try to shift the way our
favourite tools are represented. We wrote to the Create mailinglist:

<!--more-->

> Our main design concern was about how to represent digital tools in
> their diversity, from Desktop Publishing to 3D modeling, from
> typography to batch processing etc., showing by this means the
> multiplicity of the projects gathered at the Libre Graphics Meeting.
> This is the role assigned to the background, a static SVG composition
> of some of the most significant digital tools from the libre projects.
> This background, present on all the pages, zooms and pans randomly on
> each page loading, thanks to a small javascript. This gives to the
> readers unexpected exploration of the territories of that "map of
> creative tools".

[![detail]({filename}/images/uploads/detail.png "detail"){: .alignright .size-medium .wp-image-3459 }]({filename}/images/uploads/background.png)

<small>Dynamic background in SVG: unexpected explorations of the "map of
creative tools"</small>

> The logo is a play with the M of Meeting, shifting between 2D and 3D
> perception; between static and movement. 3 squares for a flag. A flag
> for a meeting. A flag for a place, and for a space. A typographic flag
> for an italic M. 3 squares for pixels. 15 degrees rotated squares for
> vectors. 3 squares of progressive sizes to evoque movement. 3 sizes
> overlapping for 3D optical effect. 3 windows floating. The typeface
> used, is OSP-DIN.

[![planche]({filename}/images/uploads/planche1.png "planche"){: .alignright .size-medium .wp-image-3459 }]({filename}/images/uploads/planche1.png)

<small>Logo proposal: A flag for a Meeting. A flag for a place, and for
a space</small>

> As the background is very present, we decided to base mainly the
> website on text. It is set with Dave Crossland's open typeface
> Cantarell, available for download at Open Font Library. This humanist
> sans-serif font was designed for on screen reading therefore it is
> very legible even on small sizes. This choice was made to promote the
> Open Font Library, the GPL and Fontforge with which was made the font,
> but also the new @font-face CSS rules newly (re-)introduced in Firefox
> 3.5 (among other web browsers). Not to mention that Dave is a very
> active Create member and a professional type designer.
>
> We picked up the yellow color as a way to keep the foreground legible.
> The latter is made of two colors, a navy blue and a brown, to
> differentiate two levels of information. We also decided to "mute" the
> partners logos by converting them to a single color in order to keep
> the whole homogeneous.

It's no easy task ;-) to design for such a divergent mix of developers,
free software activists, artists and designers but we're determined to
find a constructive middle ground and keep the energy of our proposal
alive.

Watch this thread and give [your 2
cents](http://en.wikipedia.org/wiki/My_two_cents_%28idiom%29):  
<http://lists.freedesktop.org/archives/create/2009-September/thread.html#2056>  
[http://lists.freedesktop.org/archives/create/2009-October/thread.html\#2063  
](http://lists.freedesktop.org/archives/create/2009-October/thread.html#2063)

Logo proposal: <http://ospublish.constantvzw.org/documents/LGM2010/logo>
(please install OSP-DIN)  
Website sketch: <http://lgm.alexandreleray.com>
