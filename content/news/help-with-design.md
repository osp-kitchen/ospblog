Title: Help with design
Date: 2008-08-12 23:41
Author: Harrisson
Tags: News, Thoughts + ideas
Slug: help-with-design
Status: published

Bob Sutor, Vice President of Standards and Open Source at Industrial
Business Machines, sees no alternative to Linux in the next 10 years.
But there is a need of more graphic designers to help with design.  
[Link](http://www.heise-online.co.uk/news/IBM-sees-no-alternative-to-Linux--/111281)
