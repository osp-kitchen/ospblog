Title: Contour book one
Date: 2010-03-05 15:29
Author: ludi
Tags: News, Tools, Works, books, cartography, Digital drawing, Drawing, Handmade, Recipe
Slug: contour-book-one-2
Status: published

\[gallery orderby="rand"\]  
Last sunday we drawed a book.  
**675 417 km2 pour Luce**

contour lines of France  
scale 1:385142  
interval : 30 m  
strokes : 0.05 pt  
16 binded sections  
cover : green cardboard 300 g + clothed back  
single copy

Sur base des élévations au pas de 250m proposées au téléchargement par
l'I.G.N. France. Calcul des courbes de niveaux et sortie Postscript
opérés par GRASS (r.contour & ps.map). Conversion PDF par Ghostscript et
imposition Podofoimpose.
