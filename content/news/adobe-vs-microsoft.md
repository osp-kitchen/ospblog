Title: Exit Save-As-PDF for Microsoft users?
Date: 2006-06-06 21:43
Author: Femke
Tags: News, Licenses, Printing + Publishing
Slug: adobe-vs-microsoft
Status: published

Microsoft has recently announced that their 2007 release of Office will
not support the "save-as-PDF"-option anymore. This might be bad news for
designers' favorite file-format. PDF could become rather exotic when
Microsoft users decide en masse to
"[Save-As-XPS](http://www.microsoft.com/whdc/xps/default.mspx)" instead.

<!--more-->  
Adobe inc. owns the patents for creating and reading PDF-files, by now
one of the most widely used formats to exchange documents between users
on different platforms and programmes.

> Adobe desires to promote the use of PDF for information interchange
> among diverse products and applications.  
> Accordingly, the following patents are licensed on a royalty-free,
> nonexclusive basis for the term of each patent and for the sole
> purpose of developing software that produces, consumes, and interprets
> PDF files that are compliant with the Specification  
> <small><http://partners.adobe.com/public/developer/support/topic_legal_notices.html></small>

Adobe does not charge Scribus or Open Office for implementing PDF export
in their packages, probably simply because these package do not make
money off their product, and also: many day-to-day users might result in
additional users of their high-end, and expensive products (Although
apparently Adobe does not charge Apple for including this
functionality?).

Because Microsoft is making lots of money with Office, and the ability
to do PDF export adds to the value of the package, Adobe has tried to
make Microsoft (users) pay for this functionality already for a while.
But in turn Microsoft simply decided to exclude the feature from their
2007 release.

> In that sense, I can see Adobe's side, even though I don't believe
> that any file format, simply a way of storing someone else's content,
> should be protected.  
> <small>(Gregory Pittman, Scribus mailinglist)</small>

It looks like Adobe will have trouble forcing Microsoft to pay because
of Anti-trust laws and exactly because of them not sueing any other free
and or open source use of their tool. It does give Microsoft the perfect
excuse though, to move on with their own XPS-standard after all.

<http://www.theregister.co.uk/2006/06/05/microsoft_adobe_legal_spat/>  
<http://www.itwire.com.au/content/view/4509/53/>
