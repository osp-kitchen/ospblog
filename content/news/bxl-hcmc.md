Title: BXL -> HCMC
Date: 2011-04-02 20:29
Author: Stephanie
Tags: News
Slug: bxl-hcmc
Status: published

After a long journey, we eventually arrived 24 hours later to Ho Chi
Minh City: 17.00 here is 12.00 there. To honour the tradition, a group
picture in front of our gigantic plane!

[![]({filename}/images/uploads/P10508141.jpg "P1050814"){: .alignnone .size-medium .wp-image-6071 }]({filename}/images/uploads/P10508141.jpg)
