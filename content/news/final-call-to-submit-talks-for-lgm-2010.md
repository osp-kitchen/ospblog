Title: Final call to submit talks for LGM 2010
Date: 2010-04-21 15:06
Author: OSP
Tags: News, LGM 2010
Slug: final-call-to-submit-talks-for-lgm-2010
Status: published

**Deadline: Saturday May 1**  
Please submit your talk for LGM 2010 at:
<http://create.freedesktop.org/wiki/Conference_2010/Submit_Talk>

Here is the latest press-release:  
`San Francisco and Brussels, Thursday 15 April 2010 – The Libre Graphics Meeting is an annual working conference for the free software graphics application community. Developers from the full spectrum of graphics applications — image editors, photography, 3-D and 2-D animation, vector art, graphic design, typography – collaborate with each other on interoperability, push the state of the art in application functionality and user experience, and get important face-to-face interaction with users.`  
<!--more-->  
`LGM 2010 will take place between May 27 and 30th at De Pianofabriek in Brussels, Belgium. Developers from GIMP, Inkscape, Blender, Krita, Scribus, Hugin, the Open Clipart Library, the Open Font Library, and other open source projects are scheduled to appear. Technical talks will showcase new work in digital asset management, natural-media simulation, and internationalized font design. The program will also emphasize real-world usage of open source graphics software in professional publishing houses, multimedia production, and both the secondary education and art school classroom. Developers, users, or community members who would like to give a talk at LGM 2010 are encouraged to submit a proposal by following the instructions at http://libregraphicsmeeting.org. The deadline for submissions is May 1.`
