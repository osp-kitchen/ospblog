Title: Tales of Interrogated Type (Parallel Publishing)
Date: 2009-03-02 01:26
Author: OSP
Tags: News, Standards + Formats, Workshops + teaching
Slug: tales-of-interrogated-type-parallel-publishing
Status: published

<iframe  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=-2.6,44.2,6.9,51.6&amp;layer=mapnik&amp;marker=44.84380,-0.57247" style="border: 1px solid black"></iframe>  
<small>[View Larger
Map](http://www.openstreetmap.org/?lat=47.900000000000006&lon=2.1500000000000004&zoom=6&layers=B000FTFTTT&mlat=44.84380&mlon=-0.57247)</small>

OSP is preparing for a long drive: Pierre, Yi, Ivan and Femke leave
Brussels Tuesday morning to be later joined by another
[Pierre](http://www.oep-h.com/) (our special guest), Harrisson and
Ludivine (now in Paris producing a tape/vinyl version of their worldmap
for [Cinéma du Réel](http://www.cinereel.org/)), in Bordeaux. There
we'll meet an interdisciplinary team of students and work on a series of
small publications in response to the conference '[Edit! norms, formats,
supports](http://www.rosab.net/edit/)'.

<!--more-->  
*Workshop description*

### Tales of Interrogated Type (Parallel Publishing)

In the publishing business, the term 'parallel publishing' is used to
describe the production of the same content or information in more than
one medium (typically paper and screen). In this workshop, OSP would
like to test out how to work collaboratively on a document that is
produced simultaneous with a conference, and how its content might
change in the transformation from live event to publication.

Together with participating students, we will set up a temporary
workspace (virtual and physical) from where we can respond to themes
brought up in the conference. As a point of departure, we will bring
with us some of our favourite anecdotes about the creation, production
and distribution of 'engineered' typography. During the workshop we
would like to gather a collection of stories and tales that interrogate
the practice of norms, formats and supports. Performative exercises,
experiments, interviews and visual comments will be gathered in small
publications, launched every conference day.

with:

<ul>
<li>
Pierre Huyghebaert (BE; typographer, designer, cartographer)

</li>
<li>
Yi Jiang (BE/CN; designer, illustrator)

</li>
<li>
Ivan Monroy Lopez (NL/MEX; mathematician, programmer, writer)

</li>
<li>
Femke Snelting (BE/NL; designer, artist)

</li>
<li>
Pierre Marchand (FR; programmer, typographer)

</li>
<li>
Harrisson (BE/FR; designer)

</li>
<li>
Ludivine Loiseau (BE/FR; typographer, designer)

</li>
<li>
Nicolas Malevé (ES/BE; programmer, data activist)

</li>

