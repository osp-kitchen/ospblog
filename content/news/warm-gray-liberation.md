Title: Warm Gray Liberation
Date: 2009-04-01 19:05
Author: OSP
Tags: News, Colors, Licenses, Watch this thread
Slug: warm-gray-liberation
Status: published

![warmgray]({filename}/images/uploads/warmgray.png "warmgray"){: .alignright .size-full .wp-image-2338 }

From the CBUD-mailinglist:

`Subject: RE: FW: Licensee Application From: "DUURKOOP, JULIA"  Date: Wed, 1 April 2009 11:20:25 -0400 To: "Daniel Fary" `

`Dear Daniel:`

`Thank you for your inquiries and follow-up e-mails. After consulting with executives, engineers and lawyers at the Company, we have decided to insert a series of gray tones to the PANTONE MATCHING SYSTEM. The Company has decided to extend the Pantone® Warm Gray Series in order to create more awareness for Color Universal Design as practiced by the members of your organization because we have understood that gray tones play an important role in a colorblind barrier-free color pallet. To allow the integration of the Pantone® Warm Gray Series in various colorblind proofing and document preparation softwares, we have decided to release this series in the Public Domain.`

`The current Pantone® Warm Gray Series will be extended with 20 additional intermediate tones. Compatible with our patented numbering system, we will add Pantone® Warm Gray 13 and Pantone® Warm Gray 17 between Pantone® Warm Gray 1 and Pantone® Warm Gray 2, Pantone® Warm Gray 23 and Pantone® Warm Gray 27 between Pantone® Warm Gray 2 and Pantone® Warm Gray 3 and so on.`

`Please be reminded that your organization is not authorized to use any other PANTONE Trademarks, copyrights and/or other intellectual property in connection with any of your products or services unless a formal license agreement is entered into between our companies.”`
