Title: Spinning SVG
Date: 2009-09-18 08:03
Author: Femke
Tags: News, animation, SVG
Slug: spinning-svg
Status: published

![Screenshot]({filename}/images/uploads/Screenshot.png "Screenshot"){: .alignright .size-full .wp-image-3417 }

Can't help but re-blog: friend & neighbour [Michael
Murtaugh](http://automatist.org) is experimenting with SVG animation,
using [svgweb](http://code.google.com/p/svgweb/) to make [this
cat](http://openclipart.org/people/Anonymous/Anonymous_Architetto_--_Gatto_02.svg)
spin :-)

[http://automatist.org/blog/?p=139](http://automatist.org/blog/)
