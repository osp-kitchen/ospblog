Title: In the mail
Date: 2010-08-03 11:02
Author: OSP
Tags: News, Further reading
Slug: in-the-mail
Status: published

In the mail today:  
\[portfolio\_slideshow\]  
From Taipei (Taiwan): *[Freesouls. Captured and
released](http://freesouls.cc/)*. Joi Ito, 2008  
From Weimar (Germany): *[Lorem Ipsum: Zentralorgan der Freien Klassen
Kommunikation](http://www.uni-weimar.de/fkk)*. 2010

<small>Thank you Christopher + Martin :-)</small>
