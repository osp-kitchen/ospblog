Title: Text engine القطع الأثرية
Date: 2010-11-15 19:07
Author: Pierre_M
Tags: News, Code, Scribus, Scribus Re-loaded
Slug: text-engine-%d8%a7%d9%84%d9%82%d8%b7%d8%b9-%d8%a7%d9%84%d8%a3%d8%ab%d8%b1%d9%8a%d8%a9
Status: published

On the way to a new text engine, the nice things that will be lost.  
As of revision 15877, Scribus displays severals paragraphs, fully shaped
and featured, but remains uncertain of where to put them. The
Scribus/OIF project goes on, aiming, among other things, to make graphic
designers a bit less nervous.

\[gallery\]
