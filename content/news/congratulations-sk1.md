Title: Congratulations sK1!
Date: 2008-09-03 09:12
Author: Femke
Tags: News, sK1
Slug: congratulations-sk1
Status: published

With a staggering 1463 points (runners up Typo3 and OpenLieroX ended on
respectively 1269 and 572 points), the [Open Source prepress project
SK1](http://sk1project.org/) has well deserved it's ticket to the
upcoming [Hackontest event](http://www.hackontest.org/) in Zurich, 25-26
September.  
[![]({filename}/images/uploads/sk1.jpg "sk1"){: .alignnone .size-full .wp-image-602 }](http://www.openexpo.ch/openexpo-2008-zuerich/hackontest/)  
We're *very* happy to hear that the sK1 team will have a chance to
demonstrate their enthusiasm for Libre Graphics at the [Swiss Open
Source Software Conference & Exposition](http://www.openexpo.ch/) :-)
