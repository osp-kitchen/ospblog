Title: Gender Art Net - Folding step
Date: 2010-04-12 23:42
Author: ludi
Tags: News, Works, Design Samples, Digital drawing, In the pipeline, Inkscape, Presentations, Printing + Publishing, SVG, Workshops + teaching
Slug: gender-art-net-folding-step
Status: published

![]({filename}/images/uploads/P1120184-600.jpg "P1120184-600"){: .aligncenter .size-medium .wp-image-4417 }  
[Gender Art Net](http://genderartnet.eu/emerge/) poster is now ready for
folding.  
Project presentation and workshop - Berlin - April 16-17 \_2010 (more
coming)  
1000 posters - A2  
recto black - verso CMYK  
1 plis horizontal - 4 zig zag + 1 roulé  
<!--more-->

![]({filename}/images/uploads/P1120187-600.jpg "P1120187-600"){: .aligncenter .size-medium .wp-image-4417 }
