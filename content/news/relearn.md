Title: Relearn
Date: 2011-11-12 17:32
Author: OSP
Tags: News
Slug: relearn
Status: published

Un essai en plusieurs parties, écrit pour
[∆⅄⎈](http://www.bat-editions.net/∆⅄⎈.html "∆⅄⎈"), la nouvelle revue des
[éditions BAT](http://www.bat-editions.net/ "Éditions BAT"). 5 ans après
le début d'OSP, il est temps de faire un point sur les influences
sociales des logiciels libres sur notre pratique du design.

Lisible sur la plateforme f-u-t-u-r-e.org:
[f-u-t-u-r-e.org/r/02\_OSP\_Relearn\_FR.md](http://f-u-t-u-r-e.org/r/02_OSP_Relearn_FR.md)

Aussi disponible comme fichier
pdf: [Relearn]({filename}/images/uploads/osp-bat-10-10-1.pdf)

![](http://www.bat-editions.net/%E2%88%86%E2%85%84%E2%8E%88/BAT_%E2%88%86%E2%85%84%E2%8E%88_N1-2-web.jpg "∆⅄⎈"){: .alignnone }

Le texte est écrit à plusieurs mains, sur plusieurs plates-formes, et
résulte de discussions avec des personnes supplémentaires aux
signataires finaux:

[![]({filename}/images/uploads/superia_400_6_bij_6_jul_tot_sept_2011_10.jpg "superia_400_6_bij_6_jul_tot_sept_2011_10"){: .alignnone }]({filename}/images/uploads/superia_400_6_bij_6_jul_tot_sept_2011_10.jpg)

 

 
