Title: The Tomorrow Book / Printing Party 0.1
Date: 2006-06-02 22:35
Author: Femke
Tags: News, Print Party, Printing + Publishing, Scribus
Slug: the-tomorrow-book-printing-party-01
Status: published

[![printing on
demand](http://ospublish.constantvzw.org/blog/wp-content/_DSC00539.JPG "printing on demand"){: }](http://ospublish.constantvzw.org/blog/wp-content/DSC00539.JPG)<small>Production
line set up for the scanning, lay-out (in Scribus of course!), printing
and binding of *The Continuous Present*</small>

On May 18-19 *The Continuous Present* was printed-on-demand at the Jan
van Eyck Academie in Maastricht. This publication was developed in the
context of *The Tomorrows Book*, an ongoing investigation into the
possible futures of reading, publishing and designing books.  
*Tomorrows Book* according to Harrisson:

-   Books are coded ensembles of codes (language,
    typography, softwares…).
-   Independance of thought depends on independance of the codes.
-   Tomorrows book depends on this independance.

