Title: Printing Party at Wizard of Os: Art & Copyright
Date: 2006-09-10 20:56
Author: Femke
Tags: News, Print Party, Printing + Publishing
Slug: printing-party-at-wizard-of-os-art-copyright
Status: published

Wednesday September 13, 10:00 – 20:00  
Tesla, Klosterstraße 68-70, Berlin  
<http://wizards-of-os.org/index.php?id=2806>  
Workshop Organised by **Cornelia Sollfrank** and **Nicolas Malevé**  
Printing Party by **Harrisson** & **Pierre Huyghbaert  
**

> FREEdom and OPENness – anything but marketing and ideology? Sharing,
> really? Culture from and for the Commons. One day before WOS4, an
> international group of artists, programmers and theoreticians will
> meet for a concentrated exchange of experiences within a workshop
> situation.  
> The programme of the day includes a general discussion about
> terminology, it will address questions of authorship, it offers the
> possibility for knowledge transfer in a section about free tools for
> artistic and cultural production and discusses their meaning for the
> quality of an artwork and it tries to evaluate the practice of
> applying open licences to works of art.  
> The meeting will be concluded by demonstrating a new model of open
> publishing (print on demand) as well as the use of free tools in
> design and publishing during the final “Printing Party.”
