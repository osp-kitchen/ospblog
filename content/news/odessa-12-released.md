Title: Odessa 12" released!
Date: 2007-09-17 21:27
Author: Harrisson
Tags: News, Works, Design Samples, Inkscape, Music
Slug: odessa-12-released
Status: published

No it's not a new software, it's a record OSP contributed to, using
inkscape and gimp... Proud to announce another printed matter on a great
maxi.

[![odessa.png]({filename}/images/uploads/odessa.png)]({filename}/images/uploads/odessa.png "odessa.png")  
<!--more-->  
0704 - DOXA- ODESSA

“DONNA”  
THREE HIGHLY AMAZING TRACKS. A VOCAL TECHNO TRACK WITH ODESSAS BEAUTIFUL
VOICE, AND TWO OTHER HOT AND SMASHING TECHNO PRODUCTIONS. THE EXTENDED
SIDE IS REALLY WORTH LISTENING TO!  
(quote from Kompakt website)

<http://www.myspace.com/odessatechno>
