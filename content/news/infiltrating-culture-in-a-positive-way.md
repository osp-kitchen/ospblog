Title: Infiltrating culture in a positive way
Date: 2010-11-06 14:33
Author: OSP
Tags: News, in the news, make art, Thoughts + ideas
Slug: infiltrating-culture-in-a-positive-way
Status: published

[![]({filename}/images/uploads/article.png "article"){: .alignnone .size-medium .wp-image-5254 }]({filename}/images/uploads/article.png)

*The opening talk therefore sounds like a manifest, asking to valorize
Free Software, in order to "infiltrate culture in a positive way".
Pierre Marchand, member of the OSP collective, presenting a
collaborative ornamental font, supports this proposal totally: "In our
domain, the freedom of software is the single tool to completely share
culture. It is necessary, there would be nothing without it"*

<http://www.lanouvellerepublique.fr/LOISIRS/Festivals/Make-Art-prend-son-envol-et-libere-son-public>
