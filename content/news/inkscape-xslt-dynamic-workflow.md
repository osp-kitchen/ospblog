Title: Inkscape + xslt = dynamic workflow
Date: 2007-09-08 13:19
Author: nicolas
Tags: News, Inkscape, Scripting, xslt
Slug: inkscape-xslt-dynamic-workflow
Status: published

[Use Inkscape and XSLT to Create Cross-Platform Reports and
Forms,](http://www.linuxjournal.com/article/9283) an article by *Chad
Files* on the [Linux Journal website](http://www.linuxjournal.com) that
details a workflow to produce dynamic forms and reports for both print
and web using *Inkscape* and *Xslt*.

![Inkscape used to draw a template for a claim
form]({filename}/images/uploads/9283f2inline.png)

Description of the problem and requirements:

> Health-care claims are very intricate (Figure 1). Many boxes and
> boilerplate text have to be drawn. The conventional way to do this
> with a software application is to draw a series of lines using
> coordinates and lengths, and then lay the static and dynamic content
> on top of the newly drawn lines. \[...\]

<!--more-->

> Our requirements were as follows:
>
> \* We must be able to print high-quality versions of the claims.  
> \* Claims must be accessible from a Web browser.  
> \* The solution has to be programming language-independent. We use
> Python, PHP, Perl and Java. The images need to be created using any of
> these languages.  
> \* We must be able to convert the claim data and form into several
> different file formats, specifically PDF and PNG.  
> \* The entire solution must be platform-independent.

The solution:

> Basically, we would take an SVG image of the claim form and make it
> into an XSLT (eXtensible Stylesheet Language Transformation), because
> the SVG format is a special XML format. Then, we would pull the claim
> data from our database and convert it into an XML string. Using any of
> our languages, we could then take the XSLT and the XML and create an
> SVG image of the claim. This solution met all of our requirements. It
> was language- and platform-independent. We could print the SVG images
> and embed them into Web pages. Furthermore, SVG images can be
> converted into different file formats easily. Another nice feature of
> this solution is the small file size of the SVG images. If we wanted
> to archive the images, they would take a fraction of the space the old
> solution did. Because SVG images are text, not compressed binary, the
> files can be compressed and save even more space.

[Read the article](http://www.linuxjournal.com/article/9283) for the
details of the implementation and test the sample code.
