Title: Today at OSP
Date: 2011-02-25 00:05
Author: OSP
Tags: News, Libre Fonts, studio
Slug: today-at-osp
Status: published

[![]({filename}/images/uploads/P1110735.jpg "P1110735"){: .alignnone .size-medium .wp-image-5884 }]({filename}/images/uploads/P1110735.jpg)

Today OSP worked on a [brand new super git-repository
lay-out](http://git.constantvzw.org/?p=osp.super.git;a=summary), added
new fonts to [OSP-foundry](http://ospublish.constantvzw.org/foundry/),
advanced the DIN-project, made a start with the cover for *Tot Later* (a
novel by Ana Claese) and in between celebrated that our application for
the [Libre Graphics Research
Unit](http://ospublish.constantvzw.org/lab/) has been approved.
Champagne!
