Title: Warming up the house
Date: 2011-10-11 16:01
Author: Eric
Tags: News, Works
Slug: warming-up-the-house
Status: published

[![]({filename}/images/uploads/gallait_invitation_cover.png "gallait_invitation_cover"){: .alignnone .size-medium .wp-image-6754 }](http://ospublish.constantvzw.org/news/warming-up-the-house/attachment/gallait_invitation_cover)

[![]({filename}/images/uploads/gallait_invitation_back1.png "gallait_invitation_back"){: .alignnone .size-medium .wp-image-6754 }](http://ospublish.constantvzw.org/?attachment_id=6789)

*Housewarming October 14th 15:00 onwards / Constant Variable / Rue
Gallait 80 Brussels*

The 19th century town house at Rue Gallait 80 is now known as [Constant
Variable](http://variable.constantvzw.org/ "CONSTANT VARIABLE - Free Libre Open Source Software Arts Lab").
For three years, it will house an arts lab for free, libre and open
source software. Next to the Open Source Publishing studio, there is an
open video workshop and an open hardware workshop. The top floor houses
a residency.

Of course OSP had to take on the challenge to create Variable’s visual
identity!

### Context

Rue Gallait is in the north of Brussels, in Schaarbeek. Schaarbeek was
built by the rich Brussels bourgeoisie in the late 19th century—it’s
full of beautiful houses. The bourgeoisie left for the suburbs after the
second World War, and right now Turkish and Maroccan immigrants make up
a large part of the neighbourhoods population.

Outside Gallait 80 there is a [busy commercial
area](http://fr.wikipedia.org/wiki/Place_Liedts "Place Liedts") with a
sprawl of signage. The house itself has been in use by various
governmental and arts organisations and each phase has left a [trace of
interior
signage](http://git.constantvzw.org/?p=osp.work.gallait.git;a=blob_plain;f=fotos/IMG_5525.jpg;hb=HEAD)
and decoration, in that sense mirroring the outside world. For the
identity we have tried to work with this principle, by creating sticker
kits that can be used in various configurations over time.

### Crickx

It came quite natural to us to use stickers since we house the archive
of Madame Crickx. Madame Crickx est Schaerbeekooise. She used to have a
shop selling hand-cut vinyl adhesive letters for creating signage.
((Read up on Madame Crickx in this magazine article in pdf:
[Dutch](http://git.constantvzw.org/?p=osp.foundry.crickx.git;a=blob_plain;f=documentation/madame-crickx-NL-lo.pdf "Madame Crickx"),
[French](http://git.constantvzw.org/?p=osp.foundry.crickx.git;a=blob_plain;f=documentation/madame-crickx-FR-lo.pdf "Madame Crickx")))

[![]({filename}/images/uploads/IMAG2076-31.jpg "L'armoire-archive"){: .alignnone .size-medium .wp-image-6754 }]({filename}/images/uploads/IMAG2076-31.jpg)

[![]({filename}/images/uploads/IMAG20781.jpg "map of the house"){: .alignnone .size-medium .wp-image-6754 }]({filename}/images/uploads/IMAG20781.jpg)

We are using the Crickx stock for all the internal signage in the house.
OSP has been busy creating a digital font based upon Madame Crickx’s
letters. We used this for the logo of the house—and on the opening day
of the house, we’ll release the font to the world! ((The additional font
we use is [PropCourier
Sans](http://gitorious.org/libregraphicsmag/libregraphicsmag/trees/master/persistent/typeface "PropCourier Sans"),
Ana Carvalho and Ricardo Lafuente’s proportional remix of OSP’s own
NotCourier Sans.))

We made a logo that is printed on fluor-colored office stickers. The
logo is supported by smaller circular stickers “free”, “libre”, “open
source”, “software”, “arts lab”. The stickers have been put to use on
the invitation for the housewarming, and are finding there way onto
laptops and into different nooks and crannies of the house.

[![]({filename}/images/uploads/portra_160vc_6_bij_6_sept_2011_12.jpg "portra_160vc_6_bij_6_sept_2011_12"){: .alignnone .size-medium .wp-image-6754 }](http://ospublish.constantvzw.org/news/warming-up-the-house/attachment/portra_160vc_6_bij_6_sept_2011_12)

Stickering the invitations

So to see Crickx and the identity in action, to get a tour through the
rest of the house, and to partake in a PARTY—we expect you this friday
from 15:00 onwards Rue Gallait 80, Brussels!

[GIT
REPOSITORY](http://git.constantvzw.org/?p=osp.work.gallait.git;a=summary "Git Repository osp.work.gallait")
