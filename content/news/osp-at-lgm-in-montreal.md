Title: OSP at LGM
Date: 2007-04-10 11:29
Author: Femke
Tags: News, LGM 2007
Slug: osp-at-lgm-in-montreal
Status: published

For the [Libre Graphics Meeting](http://www.libregraphicsmeeting.org/),
taking place May 4-6 in Montreal, Canada, OSP members Harrisson and
Femke will speak about FLOSS and design: **Relaying Systems - Why
designers should be interested in FLOSS**. The programme is
[here](http://www.libregraphicsmeeting.org/program.html)!
