Title: Pre-announcement: print/pixel conference
Date: 2009-04-20 21:45
Author: OSP
Tags: News, Conference, LaTex, Printing + Publishing, Tools
Slug: pre-announcement-printpixel-conference
Status: published

(forwarded from the research programme [Communication in a digital
age](http://pzwart.wdka.hro.nl/mdma/), Rotterdam)

`PRINT/PIXEL International conference on the shifting relation between online and print publishing`

`Tuesday-Wednesday 12-13 May 2009 | 12-17:00 (public sessions), 20-22:00 (professional sessions)`

`The conference investigates the shifting relationship between online and print publishing. Both can no longer be separated, but complement each other. Yet few advanced comprehensive publication and design strategies beyond ad-hoc copy-paste do exist, and the recent crisis of print news media and advent of new technologies such as e-books and print-on-demand are about to upset the world of designers, editors and publishers.  Will XML, cross-media content management systems, ePub, networked on-demand publishing and generative design soon be everyone's vocabulary?  International cutting-edge designers, software developers, publishers and researchers - many of them being all of these at once - will give insight into their solutions and discuss the future of publishing.`

<!--more-->  
`Speakers a.o.: Open Publishing Lab Rochester Institute of Technology (USA), Simon Worthington (OpenMute publishers, UK), Petr van Blokland (graphic designer and typographer, NL), Gerrit Imsieke (Le-Tex publishing services, Germany), Lou Lichtenberg (Stimuleringsfond van de Pers, NL), media workgroup of NRC Handelsblad (NL), Alessandro Ludovico (Neural magazine and Mag.net cultural publishing project, IT), accompanied by a real-time newspaper project by Peter Zuiderwijk (NL) and students of the WdKA Minor Editorial Design, conference blogger Arie Altena (NL) and the CrossLab of the Willem de Kooning Academy.`

`The afternoon conference sessions can be freely attended.`

`The professional session in the evening is primarily meant for media and design professionals and will include concentrated versions of the afternoon presentations along with workgroup-like peer discussions.`

`Attendance fee: EUR 50/evening or EUR 80/two evenings, for members of non-profit organizations: EUR 30/evening, EUR 50/two evenings.`

`Locations: WdKA main building, Blaak 10, Rotterdam [public sessions] and Groot Handelsgebouw, at Rotterdam Centraal station [professional sessions]`

`For more information on the print/pixel conference, contact: Saskia Brandt Corstius, s.w.brandt.corstius at hro.nl`
