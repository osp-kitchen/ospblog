Title: BIS.ArtShaker
Date: 2009-02-01 18:49
Author: admin
Tags: News, Works, Design Samples, Digital drawing, Digitalisation, Gimp, Radio
Slug: bisartshaker
Status: published

![logo\_roger\_bk\_web]({filename}/images/uploads/logo_roger_bk_web.gif "logo_roger_bk_web"){: .alignleft .size-full .wp-image-1717 }

Our friend and [Dj Athome](http://www.djathome.org/) just recieved a new
logotype for his inspiring radioshow. We asked artist ROGER3000 to do
calligraphy for. Rough, spontaneous and good style ;) Digitalisation
using XSane Image Scanner, and image correction in Gimp.

BIS-ARTSHAKER is being played by DJ ATHOME aka HOPHEAD and guests every
wednesday 20:30-22:00 at Radio Panik 105.4 fm (Bruxelles) since 1997.
You can listen to it each week on
[STREAMING](http://www.radiopanik.org/ecouter/) or download the current
radio shows.  
[www.radiopanik.org](http://www.radiopanik.org)
