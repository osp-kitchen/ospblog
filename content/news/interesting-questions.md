Title: Interesting questions
Date: 2008-11-19 10:52
Author: Femke
Tags: News, Design philosopy, Education, Standards + Formats
Slug: interesting-questions
Status: published

Ever considered applying as researcher for the [Jan van Eyck
Academy](http://www.janvaneyck.nl/) in Maastricht (NL)?

**[Extrastatecraft](http://www.janvaneyck.nl/0_3_3_research_info/design_extrastatecraft.html)**,
a project led by [Keller Easterling](http://www.panix.com/~keller/) asks
interesting questions about the relation between protocol and practice.
The JvE offers great facilities (including financial support) for
extra-academic research and with the appointment of [Florian
Schneider](http://www.burundi.sk/monoskop/index.php/Florian_Schneider)
as 'advising researcher', the design department might be ready for your
F/LOSS energy.

"*How do organizations like the ISO (International Organization for
Standardization) or McKinsey determine management protocols? How do
construction networks, more than the singular creations of architects
and urbanists, disseminate materials and processes that determine how
the world is calibrated? How do markets and financial instruments create
templates that shape space?*"

Only a few more days left to apply!
