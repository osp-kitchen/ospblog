Title: LGM 2011 remix
Date: 2011-05-18 23:38
Author: John
Tags: News, Tools, LGM 2011
Slug: lgm-2011-remix
Status: published

It is difficult to believe that Libre Graphics Meeting 2011 is already
over. We spent 9 intense days in Montreal to work, talk, meet, present,
cook, interview, listen and cross the city by bike. The meeting provoked
many discussions about the future of Libre Graphics so we decided to
write this report in the form of a dialogue (but we took trains and
airplanes to different cities at different times, so it turned out more
like a collage).

\[caption id="attachment\_6408" align="alignleft" width="400"
caption="Happy to find Ana, Ricardo, Eric and sometimes ginger around
the kitchen
table"\][![]({filename}/images/uploads/p1120382.jpg "p1120382"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/p1120382.jpg)\[/caption\]  
<!--more-->  
\[john\] This year was my second Libre Graphics Meeting, with the first
being last year in Brussels, so I only have the two experiences to
contrast. I've never been to Montreal before other than through the
airport, so the place was fresh for me but I can only imagine what it
was like for some who have been here at other LGMs during the many times
it was hosted here.

\[caption id="attachment\_6403" align="alignleft" width="300"
caption="Toonloop workshop with Alexandre
Quessy"\][![]({filename}/images/uploads/P1120373.jpg "P1120373"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/P1120373.jpeg)\[/caption\]

\[ludi\] At pre-LGM I discovered Toonloop, a spontaneous stop motion
programme.  
In 30 mn, we managed to build a full color animated title.  
It was awesome to see the Singer machines collection.  
The Open Clip Art Famous People category grew not that bad and Spiro
made Eric organic.  
Yes ! Starting with practicing together is a really good entry.

\[caption id="alina" align="alignleft" width="300" caption="Alina
cutting perfectly normal pants"\]  
[![Alina cutting perfectly normal
pants]({filename}/images/uploads/P1120361.jpg "P1120361"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/P1120361.jpeg)\[/caption\]

\[femke\] Just like in 2007 and 2009, the meeting took place in the
Ecole Polytechnique. The campus is located on Mont Royal, half an hour
by Metro or bike from the city centre. The journey from the front door
to the actual meeting room includes several escalators, traversing vast
orange, green and drab pink hallways. For food and coffee there are four
cafeterias spread over different floors plus vending machines and racks
of sweet-dispensers (25c for a fistful of artificial coloring). The many
cork-board display-units placed around the the work spaces across the
main hall remained unused apart from our rather uninspired re-play of
the poster exhibition earlier in StudioXX. The view from the 6th floor
is breathtaking.

\[caption id="attachment\_6404" align="alignleft" width="400"
caption="Visiting the Biodome with Tom
Lechner"\][![]({filename}/images/uploads/p1030176.jpg "p1030176"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/p1030176.jpg)\[/caption\]

\[ludi\] Under the pretext of an iron transfer, on Monday we get to
visit the Biodome with Tom Lechner. The most interesting part of the
dome is the view from the outside. Walking around the building you
discover the decor's backside, composed by tubes, aeration, lights
supports - structures of micros worlds populated by fake stones and
trees and decorated with real animals. 2 days later we find back the
famous rodent laid out in Laidout.

\[caption id="attachment\_6405" align="alignleft" width="400"
caption="User feedback at the Scribus
meeting"\][![]({filename}/images/uploads/P1020607.jpg "P1020607"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/P1020607.jpg)\[/caption\]

\[caption id="attachment\_6445" align="alignleft" width="400"
caption="Scribus OIF in
progress"\][![]({filename}/images/uploads/Pierre-in-Scribus.jpg "Pierre-in-Scribus"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/Pierre-in-Scribus.jpg)\[/caption\]

\[femke\] Each year I come home from LGM convinced that bringing
designers and developers around the table is not only necessary, but
that it is very special and inspiring too. My enthusiasm for an occasion
to work together on the Libre tools we share, has obviously grown
stronger through the practice we developed as OSP in parallel.  
But also every year it is harder to accept that the community seems to
miss the opportunity to push for that potential while in the mean time
our expectations of LGM (and of the Libre tools it showcases), have
grown and expanded. I think this year I started to run out of patience.

\[caption id="attachment\_6414" align="alignleft" width="400"
caption="Dressed for
OSP"\][![]({filename}/images/uploads/IMG_4177.jpg "IMG_4177"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/IMG_4177.jpg)\[/caption\]

\[femke\] Maybe LGM has always been a happy accident that I am taking
too serious. The start of the Libre Graphics Research Unit obviously
addresses this but I am curious what other people in the audience
thought.

\[john\] This brings up the concept of 'vibe', that ethereal but
ever-important quality that can and does make or break events. The
metaphor I kept coming back to, trying to explain it to people at the
party on the last night, was of being trapped in the ribcage of a dead
horse. This is really different than the feeling of last year, where I
was completely invigorated to be in a packed audience almost all of whom
were running GNU/Linux. Last year I felt like I was in the midst of a
vibrant scene buzzing with energy. This year the vibe was, well, just
about the opposite.

\[caption id="claudia" align="alignleft" width="400"
caption="Interviewing Scribus' Claudia Krummenacher, usability
designer"\][![]({filename}/images/uploads/interview-Claudia.jpg "interview-Claudia"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/interview-Claudia.jpg)\[/caption\]

\[femke\] There was energy in the room, also this year, even with a
smaller group of attendants. But it felt undernourished and much of it
dispersed. The actual stuff of the meeting (practitioners of different
persuasion spending time together to figure out what matters for them in
the tools they use and develop) seemed taken for granted and so glue was
missing. Or were we missing Alexandre Prokoudine?

\[ludi\] Parallel talks, the interviews and the meetings we've been
searching for were each time enlightening. Thank you Tom, Claudia,
Asheesh.

\[caption id="attachment\_6447" align="alignleft" width="400"
caption="Schoolbus to the
cocktail."\][![]({filename}/images/uploads/P1030211.jpg "P1030211"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/P1030211.jpg)\[/caption\]

\[ludi\] The big group picture needs to be recomposed.  
See [Tom Lechner
sketches](http://www.flickr.com/photos/tomlechner/5724785577).

\[femke\] I missed the W3C.

\[john\] Dispersement.. You mentioned the location of the venue, its
distance from downtown. For me this meant that dispersion was
inevitable. When talking with someone who has chosen to stay near the
conference--usually a soundly reasonable decision to make on attending a
multiday event in a new city--on the long way down the escalators to the
entrance, how do you continue the conversation once you have actually
reached the doors? For instance, the nearest restaurants are a 20 minute
walk. The venue could have been advertised with a slogan, "Dispersement
inside."

\[ludi\] We could/should have taken the mic and say H E Y.

\[caption id="attachment\_6454" align="alignleft" width="400"
caption="Languages and Libre Graphics in
Africa"\][![]({filename}/images/uploads/p1030360.jpg "p1030360"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/p1030360.jpg)\[/caption\]

\[ludi\] When we dicussed with Erin Manning on Saterday, she blowed up
an idea SenseLAB practised at an event some times ago, distributing
oranges to the audience so that they had to get off their laptop to peel
it.

\[femke\] notes: sharing flight money -- \$25 media projects -- subtle
activism -- destruct things not people / things and people -- no routine
house -- free radicals -- can we afford to waste our time? -- breaking
habits -- collaborate not because we like each other -- flexible
collective economy -- obligation to the project, not the people --
oranges.

\[john\] This year highlighted specific things I'd love to see in the
future: More designers, more artists, more outreach. More design, more
art, more inner exploration. Active interventions, loud crazy parties
and quietly (or not) exuberant after-day conversations in cafes found
side by side to the venue.

[![]({filename}/images/uploads/John-emptying-the-clip.jpg "John-emptying-the-clip"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/John-emptying-the-clip.jpg)

\[ludi\] At the dinner/party on Friday evening, John emptied the clip
rapping on Libre rhymes.

\[caption id="attachment\_6416" align="alignleft" width="400"
caption="Pierre Marchand with his family of applications: Fonzie, Nancy,
FontMatrix, FuzzyRug, Litteraldraw and
more."\][![]({filename}/images/uploads/IMG_4257.jpg "IMG_4257"){: .size-medium .wp-image-6408 }]({filename}/images/uploads/IMG_4257.jpg)\[/caption\]

\[femke\] Remember we talked to Michael Terry in 2008 about the
possibility of connecting 'ingimp' data to it's manual? Three years
later he and his team have come up with amazing 'adaptable gimp' which
does exactly that, plus takes on the interface itself. Interesting to
think what data this new software can produce and than what they will do
next in a few years.

\[ludi\] I recharged my Libre batteries and have made ​​great meetings
and reunions but about the event itself I feel a bit like a firecracker
who took on water.

\[pierre\] Shared feelings. All in all I guess a rather decent
conference, good talks, good projects. On the side of the meetings I
felt something was missing, or not as high as it could have been.

\[john\] Considering the fact that I saw presentations on a set of tools
and approaches that, if successfully integrated, signal real innovative
developments emerging from the libre graphics community, there didn't
seem to be quite enough fireworks :)

If I started listing all the cool things I saw, we would be here all
night. Suffice it to say, this LGM felt like mostly potential and
without full realization. But hey, we use libre software everyday: that
means we should be used to it by now.
