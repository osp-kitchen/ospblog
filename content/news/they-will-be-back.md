Title: They will be back
Date: 2011-04-18 22:03
Author: OSP
Tags: News, LGM 2011, Open Fonts
Slug: they-will-be-back
Status: published

[![]({filename}/images/uploads/promise.png "promise"){: .alignnone .size-full .wp-image-6214 }](http://openfontlibrary.org)  
The [Open Font Library](http://openfontlibrary.org) promises to be back
in time for the [Libre Graphics Meeting
2011](http://www.libregraphicsmeeting.org/2011)!
