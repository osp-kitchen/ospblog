Title: Follow The Sound Festival
Date: 2009-09-08 12:18
Author: Harrisson
Tags: News
Slug: follow-the-sound-festival
Status: published

OSP was commissionned to design the Follow The Sound festival image. FTS
is a Free Jazz festival occuring in Antwerpen for 36 years.

We did a serie of 4 different posters and 2 flyers (one in Risograph,
the other one in offset)  
Typography and layout was done by OSP, and we asked
[RBDX](http://www.rbdx.com/) to do colored background for each support.

![FTSPOSTER]({filename}/images/uploads/FTSPOSTER.jpg "FTSPOSTER"){: .alignleft .size-full .wp-image-3358 }

<!--more-->

![posters\_FTS\_ok]({filename}/images/uploads/posters_FTS_ok.png "posters_FTS_ok"){: .alignleft .size-full .wp-image-3358 }

![posters\_FTS\_ok2]({filename}/images/uploads/posters_FTS_ok2.png "posters_FTS_ok2"){: .alignleft .size-full .wp-image-3358 }

![posters\_FTS\_ok3]({filename}/images/uploads/posters_FTS_ok3.png "posters_FTS_ok3"){: .alignleft .size-full .wp-image-3358 }
