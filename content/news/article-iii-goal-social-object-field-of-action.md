Title: Article III: Goal, social object, field of action
Date: 2015-06-05 10:41
Author: Colm
Tags: News
Slug: article-iii-goal-social-object-field-of-action
Status: published

This is an extract from our association statutes. Links at the end of
the page.

[![IMG\_20150605\_094109]({filename}/images/uploads/IMG_20150605_094109.jpg){: .size-medium .wp-image-7387 .aligncenter width="100%" height="auto"}]({filename}/images/uploads/IMG_20150605_094109.jpg)

The association OSP has as its main goal to propagate Free and Open
Source culture in Brussels and internationally. More specifically, OSP
asbl aims to stimulate the social movements of Free Culture and Free
Software in the field of graphic design.

Free Culture and Free Software challenge the excesses of copyright and
one of the ideas which underly copyright: the ideas of originality and
of artistic creation as a solitary act. Instead they propose a vision of
artistic creation which includes collaboration, exchange and creative
re-appropriation.

Free Culture chooses not to reject copyright outright, but to
appropriate and subvert it with its own means (also known as
“copyfight”). Participants in Free Culture choose to distribute artistic
works under a license which permits others many of the freedoms normally
lacking in traditional copyright licenses: the freedom to freely use the
works, modify them and redistribute the modified copies.

Free Software as a concept applies to computer software; an example of a
Free Software license is the GNU General Public License. Free Culture as
a concept applies to other cultural works such as designs, drawings,
compositions, texts. An example of a Free Culture license is the Free
Art License and  the Creative Commons Attribution Share-Alike license.

Free Culture and Free Software are part of what is more popularly known
as “Open Source”. More specifically Free Culture and Free Software are
“share-alike”: that is to say, a work of Free Culture and of Free
Software can only be used by others if the resulting work is also
licensed as Free. They can thus be seen as to be concerned with creating
an alternative ecology which features a different way of dealing with
intellectual property; an ecosystem that can exist next to but cannot
easily be fully appropriated by the existing privative regimes of
intellectual property.

While one can make Free Culture with proprietary software, and one can
use Free Software to produce creative works under traditional copyright
licenses, OSP asbl stimulates the use of Free Software to create Free
Culture in order to develop a coherent ecosystem of freedom. Indeed Free
Software, by its open nature, invites to understand the mechanisms of
digital tools to then manipulate them in a critical and constructive
way. Practices shape tools—tools shape practices.

OSP asbl pursues the realization of its goal by all means but more
specifically through three axis: pedagogy, research and graphic design.
OSP asbl gives workshops in the frame of traditional educational
institutions and organizes alternative events outside of that frame
which further question how Free Culture can change existing pedagogy. In
collaboration with cultural or educational institutions, OSP asbl is
able both to develop a theoretical framework and the open tools which
exemplify this position. By engaging in graphic design practice, both as
part of its own projects and those of others, the hypotheses from the
research are tested in practice, and new questions arise. Finally,
editing and publishing are a means to serve OSP asbl goal, as are public
events: print parties, workshops, performances, exhibitions and
lectures.

[![IMG\_20150605\_094000]({filename}/images/uploads/IMG_20150605_094000.jpg){: .size-medium .wp-image-7387 .aligncenter width="100%" height="auto"}]({filename}/images/uploads/IMG_20150605_094000.jpg)

[![IMG\_20150605\_094014]({filename}/images/uploads/IMG_20150605_094014.jpg){: .size-medium .wp-image-7387 .aligncenter width="100%" height="auto"}]({filename}/images/uploads/IMG_20150605_094014.jpg)

[![IMG\_20150605\_094042]({filename}/images/uploads/IMG_20150605_094042.jpg){: .size-medium .wp-image-7387 .aligncenter width="100%" height="auto"}]({filename}/images/uploads/IMG_20150605_094042.jpg)

<small>When depositing statutes to declare an association in Belgium,
you are intended to write these in the language used by the governmental
institution you are registrating with. In the case of OSP, we wanted to
register with both the Dutch and the French governments to have the dual
ASBL / VZW status. We have an English version because it was our middle
ground to sync beteen the french and english version. It's nice to use
the english one on the blog.</small>
