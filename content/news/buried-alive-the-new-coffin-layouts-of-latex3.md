Title: Buried alive! The new coffin layouts of LaTeX3
Date: 2012-03-19 12:37
Author: John
Tags: News, co-position, LaTex, LaTeX3, layout, TeX
Slug: buried-alive-the-new-coffin-layouts-of-latex3
Status: published

So on my way to the Visual Culture codebase today I happened upon a
rabbit hole that was too inviting not to descend into:

(Note that all names in this email are new to me as of today).

In response to a new LaTeX users questions about 'best practices' for
laying out a letterhead, Frank Mittelbach responded with a solution
using the new LaTeX3 technique of "coffins".^[1](#fn1){: #fnref1 .footnoteRef}^

![](http://i.stack.imgur.com/iMQZD.png)

After demonstrating the letterhead, he presents a sample of Tschichold
and a challenge for others to reproduce the sample 'without coffins'.

![Jan Tschishold specimen implemented in LaTeX3
coffins]({filename}/images/uploads/tschishold-coffin.png)

Yiannis Lazarides responded with a non-coffin version, though the
discussion did not extend very far because the code was not very
accurate as a replica.^[2](#fn2){: #fnref1 .footnoteRef}^

The rabbit hole itself was a thread asking why one would program in
LaTeX3 when one can avoid the encumberances of using TeX as a
programming language by using the `\directlua{: #fnref1 .footnoteRef}` macro (actually one of
only a few new TeX macros found in LuaTeX).^[3](#fn3){: #fnref1 .footnoteRef}^ Frank Mittelbach explains that this is a false dichotomy,
as LaTeX3 is designed to operate on four separate levels. The main work
on LaTeX3 has been in stabilizing it's underlying programming layer.
This has apparently been finalized and so real work can proceed on the
other layers. The coffins interface is not set, for instance:

> The implementation of coffins for expl3 is stable. However the  
> user-level interface isn't yet! Right now we have two alternatives,  
> one with a keyval system and one just with optional arguments, I used  
> the latter above. But both might get refinements, especially after we  
> get feedback from people actually "designing" with it. user-level
> interface isn't yet! Right now we have two alternatives, one with a
> keyval system and one just with optional arguments, I used the latter
> above. But both might get refinements, especially after we get
> feedback from people actually "designing" with it

There is no reason, ultimately, that the programming layer (this
cryptencoded 'expl3') cannot be rewritten in Lua if a situation would
warrant it. The flip side of this statement is that any similar venture
could build something like coffins purely out of Lua, perhaps with less
pain in the meantime. The flip side of *that* flip side is that LaTeX3
only require eTeX, which is supported by all major TeX interpreters in
use today, while LuaTeX may or may not reach it's stated 1.0 in 2012
goal.

<object ><param name="movie" value="http://www.youtube.com/v/CFPMQOtY6I0?version=3&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/CFPMQOtY6I0?version=3&amp;hl=en_US" type="application/x-shockwave-flash"  allowscriptaccess="always" allowfullscreen="true"></embed></object>

Thoughts
========

-   Finally seeing some Latex3 in action! The model of multiple layers
    is interesting, and their architectural choice to have a stable set
    of underlying calls which are then built upon in libraries was a
    smart choice, as demonstrated by their ability to rewrite the whole
    thing in Lua if there is call too.

-   I immediately wondered what the relevant code would look like in the
    newest beta of Context and am curious to dig into these 'coffins'.

-   I did not know that Håkon Wium Lie^[4](#fn4){: #fnref1 .footnoteRef}^
    wrote a Phd thesis on his work with CSS^[5](#fn5){: #fnref1 .footnoteRef}^. I read today that he takes a great deal from TeX's
    box model.

-   TeX StackExchange^[6](#fn6){: #fnref1 .footnoteRef}^ is providing a
    very productive space for enhancing and discussing
    typographic programming.

-   To wit, I liked the reminder of the 'eyes', 'mouth', and 'stomach'
    metaphor for the functioning of TeX that was mentioned in this
    response as to whether TeX^[7](#fn7){: #fnref1 .footnoteRef}^ provides
    a good foundation for learning programming. A spooky embodiment,
    with all this talk of coffins?

References
==========

<div class="footnotes">

------------------------------------------------------------------------

1.  <div id="fn1">

    </div>

    [Letterhead design with Latex3
    coffins](http://tex.stackexchange.com/questions/44065/is-there-no-easier-way-to-float-objects-and-set-margins/44159#44159)
    [↩](#fnref1){: #fnref1 .footnoteRef}

2.  <div id="fn2">

    </div>

    [Pauper's coffin solution in Latex
    2e](http://tex.stackexchange.com/questions/44241/latex3-and-paupers-coffins)
    [↩](#fnref2){: #fnref1 .footnoteRef}

3.  <div id="fn3">

    </div>

    [LaTeX3 versus pure
    Lua](http://tex.stackexchange.com/questions/45183/latex3-versus-pure-lua)
    [↩](#fnref3){: #fnref1 .footnoteRef}

4.  <div id="fn4">

    </div>

    [Håkon Wium Lie @
    Wikipedia](http://en.wikipedia.org/wiki/H%C3%A5kon_Wium_Lie)
    [↩](#fnref4){: #fnref1 .footnoteRef}

5.  <div id="fn5">

    </div>

    [PhD thesis: Cascading
    Stylesheets](http://people.opera.com/howcome/2006/phd/)
    [↩](#fnref5){: #fnref1 .footnoteRef}

6.  <div id="fn6">

    </div>

    [TeX @ StackExchange](http://tex.stackexchange.com/)
    [↩](#fnref6){: #fnref1 .footnoteRef}

7.  <div id="fn7">

    </div>

    [Does TeX/LaTeX give a headstart with other programming
    languages?](http://tex.stackexchange.com/a/42749)
    [↩](#fnref7){: #fnref1 .footnoteRef}

</div>
