Title: First 2011 work, Circular Facts website
Date: 2011-01-03 14:19
Author: OSP
Tags: News, Works, Unicode, Website, wordpress
Slug: first-2011-work-circular-facts-website
Status: published

During our current Scribus renderframe experiments day, Ivan shows us [a
customised Wordpress
template](http://ospublish.constantvzw.org/works/index.php?/projects/circular-facts/)
with a gorgeous index page in Unicode for the [Circular Facts
website.](http://www.circularfacts.eu)

[![Circular Facts website
index]({filename}/images/uploads/cf1.png "Circular Facts website index"){: .alignnone .size-medium .wp-image-5522 }]({filename}/images/uploads/cf1.png)
