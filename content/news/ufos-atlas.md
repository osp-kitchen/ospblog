Title: UFOs atlas
Date: 2011-10-08 15:36
Author: Pierre
Tags: News, Works, cartography
Slug: ufos-atlas
Status: published

As attempt to map visually theater activities accross european theater
institutions for the [Travelogue
project](http://www.arts-mobility.info/), we propose an atlas with three
different kind of visualisations.

[![]({filename}/images/uploads/IMAG1956.jpg "IMAG1956"){: .alignnone .size-medium .wp-image-6741 }]({filename}/images/uploads/IMAG1956.jpg)

[![]({filename}/images/uploads/IMAG1957.jpg "IMAG1957"){: .alignnone .size-medium .wp-image-6741 }]({filename}/images/uploads/IMAG1957.jpg)
