Title: LGM analogic planning
Date: 2009-04-30 14:45
Author: Pierre
Tags: News, Drawing, LGM, Paper, Planning
Slug: lgm-analogic-planning
Status: published

The preparation of [LGP 2009
Montreal](http://www.libregraphicsmeeting.org/2009) is on his way (OSP
is very excited!) and the LGM website offers [this fine
view](http://www.libregraphicsmeeting.org/2009/program.php) of the
planning exercice for the talks. Beautiful way of showing the still open
program.

\[caption id="attachment\_2513" align="alignnone" width="440"
caption="Paper made"\]![Paper
made]({filename}/images/uploads/program_draft1.jpg "Program draft LGM 2009"){: .size-full .wp-image-2513 }
