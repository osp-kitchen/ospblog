Title: Conversion is costly
Date: 2009-05-20 02:14
Author: Lauren
Tags: News, Conference, Printing + Publishing, Report, Rotterdam
Slug: conversion-is-costly
Status: published

### Notes from print/pixel

![](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/pixelprint/large/1064-p1050471.JPG)

Last week, OSP attended the print/pixel conference in Rotterdam, a two
day event gathering publishers, designers, marketeers and document
engineers to look at the ever shifting relation between digital and
paper publishing. ((The organisers kindly supported our trip up north.))

For an integral report, see blog-posts by [Jouke
Kleerebezem](http://www.nqpaofu.com) and [Arie
Altena](http://ariealt.net) here:
<http://blog.wdka.nl/communication-in-a-digital-age>. Our notes are
fragmentary.

<!--more-->  
<span class="Femke">Green</span> = <span class="Femke">Femke</span>  
<span class="Lauren">Red</span> = <span class="Lauren">Lauren</span>

“Pixels do not exist on screens only. And 'print' is a statement in
programming as well. Actually, we are printing pixels all the time.”
<span class="Femke">Florian Cramer starts with reassuring the audience
that although books will most likely never cancel websites and vice
versa, designers might be underprepared for the parallel publishing of
the future.</span>“Would **you** consider a career in e-book design?”

<span class="Femke">It was a bold move, to begin the conference with The
Reader, an e-book device by Sony (not available yet in Belgium or The
Netherlands). Marc Regeur, Product Manager New Business Sony Benelux,
supplied us with slogans:</span> 'Impact Through Innovation'<span
class="Femke">,</span> 'Electronic Ink Is The Heart of the Electronic
Reader'<span class="Femke">,</span> 'Proprietary Standards Are A
Hindrance To The Overall Success Of The eBook' <span class="Femke">,
followed by</span> 'We Are Solidifying The Standard By Making The Open
.epub Format Integrate Well With Adobe CS3' <span
class="Femke">and</span> 'We Add Value For The Consumer By Leaving Their
Choices Open. We Just Point Them In The Right Direction'.  
<span class="Femke">We were also reminded of Sony's first product, the
electric rice cooker.</span> (("As the war plants had closed down, there
was more electricity than was needed at the time. This surplus fed
Ibuka’s desire to produce items which were needed for everyday life. The
electric rice cooker, made by merely interlocking aluminum electrodes
which were connected to the bottom of a wooden tub, was a primitive
product. The result depended heavily on the kind of rice used and the
weight of the water. Tasty rice was a rarity, as the rice cooker
produced mostly undercooked or overcooked rice."
http://www.sonyinsider.com/2009/03/16/one-of-sonys-first-products-a-rice-cooker/))
<span class="Femke">And now they bring us The Reader?  
The horrifying thought of incompatible books... Amazon's Kindle uses
it's own proprietary format, Sony has decided to work with an open
format instead: .epub (we were confused at first because it was referred
to as the 'Adobe .epub standard'). Marc Reguer explains that</span>“Sony
did not want to make the same mistake as they did in the music
business”. <span class="Femke">The Reader is prepared for Digital Rights
Management like any other Sony device, but customers are allowed 64
copies of each of the files they buy. Also, Sony provides public domain
books on The Reader.</span>“Public domain? Well, that's basically
DRM-Free content”

<span class="Lauren">Afterwards Florian Cramer questions if the ebook
reader offers any self-publishing possibilities. Is the consumer limited
to a small selection of books? Or would it for instance be possible for
students to publish a graduation catalogue as an ebook. Could people
publish material for free on the internet and read them on ebooks?
Florian Cramer states that it is quite ironical that in the country with
such a large tradition in bookdesign there seems to be no opportunity
for students to actively engage with ebooks. Reuger answers that</span>
"one could publish content in a pdf, size A5 and potentially read it on
an ebook. But that would involve some form of piracy. Neither can we
accept student research projects into the technology of ebooks at the
moment. We are not able to give people this possibility. On a global
scale, The Netherlands is still a small country." <span
class="Lauren">So it seems that the technology of ebooks does have some
very tight limitations in opposition of what is promised. What is the
benefit for the consumer if one can only read a very tight selection (of
probably only best-sellers)?</span>

<span class="Lauren">The discussion follows with a marking question by
Allessandro Ludovico. Throughout the lecture Reuger underlines the
honourable effort of the Sony company by their free distribution of
ebooks for schools. Ludovico challenges this idea by asking if Sony
thinks about publishing old classics or important literatures</span>"As
a symbolic gesture to tradition of publishing?" <span
class="Lauren">Reuger answers that they would have to look at this from
a case to case basis, but that finally,</span>"it will remain the
responsibility of the publisher". <span class="Lauren">Obviously these
touchy subjects demonstrate Sony's ambiguous position vis-a-vis open
formats. In fact they seem as limited as their self-acclaimed
philanthropy.</span>

<span class="Lauren">This highly critical departure is followed up by a
deep investigation into the technology offered by Le-Tex for publishing
ebooks. Le-Tex historically provided typesetting services, production
editing and a specialist non-pdf-input. They produced an over all of
1500 pdf-based ebooks.</span>

<span class="Femke">Gerrit Imsieke from the German 'content engineering'
company Le-Tex services (not a surprise that the DIN institute is on
their list of clients) went into the details of the .epub format that is
developed by the International Digital Publishing Forum.</span> (("The
work of the IDPF will foster and promote the development of electronic
publishing applications and products that will benefit creators of
content, makers of reading systems and consumers."
http://www.openebook.org))

<span class="Lauren">Le-Tex offers two flavors in ebook-design. A
paginated and a reflowable design which is designed for small screens,
visual impaired and audio-listening. As people need to be able to
enlarge the type size it is not possible to use traditional page numbers
and the technology demands a new treatment of the traditional
book-format. This seems a challenge for designers to me. Funny that
Le-Tex did not quite find a suitable solution for this problem. It
becomes clear that typesetting for ebooks happens through a html+css
like structure which is of course a total different treatment than the
traditional typesetting profession. Here Le-Tex underscores the
important possibility of interactivity supported by ebooks. By making
items clickable, ebooks can be given different structures in opposition
to traditional books.</span>

<span class="Femke">Intriguing to discover this .epub standard which is
in fact a salad of 15 pre-existing standards: xml, svg, css... and how
the concept of the reflowable book creates a different paradigm for
design. Issues such as font encoding normalization and de-hyphenization
are clearly more pressing concerns for Iemseke than typographic
excellence. He explains that encoding errors can only be detected
visually and this makes conversion between media costly. Imsieke states
that the price per page is still too high</span> “even when done by
cheap students or outsourced to people in India”. <span
class="Femke">When asked later, he estimates that 30% of current
document engineering happens in India. A division of labour I would like
to investigate further.</span>

<span class="Femke">(Free Fonts are unmissable for e-publishing too:
e-books require font embedding and Le-Tex has therefore decided to work
with Linux Libertine.)</span>

<span class="Femke">Petr van Blokland delivers his exposé on the
similarities between programming and design with the usual
confidence:</span>“you should be the director of your workflow”, “you
can only survive when you define” <span class="Femke">and</span> “to
design a workflow means to break the iterative loop”.

<span class="Lauren">Blokland defines his design theory as non-linear
approach which iterates through possibilities and works by
exclusion.</span> "Only by actually trying out the possibilities you get
closer to the answer." <span class="Lauren">He states that</span>"for
graphic designers there is too much to choose from, there is no way to
find out the everbest solution. All the possibilites are there, there
are just too many options." <span class="Lauren">For Blokland a personal
query and a personal opinion is the only solution. It is hard to find
out the actual thread of his lentghy and meandering exposé. At first his
lecture almost seems a step-by-step-help plan which solves every
designers troubles. Luckily it becomes clear that for Blokland designers
should enrich their personal workflow by learning the language of the
tools and by engaging towards the development of own programs. This is
his solution for 'setting out strategies' and 'managing the option
overload'. I can completely relate to his statement that</span> "after a
while there is nothing more than your application allows you to do. By
learning that language you can go beyond the confined space." <span
class="Lauren">A good answer to this would exactly be to learn script
languages which do broaden a designer's workflow. Blokland also raises
some curiosity about his [Xierpa](http://xierpa.com/) project which will
be released soon under an open source license. An over all tool for
designing tools, designing databases and much more…</span>

<span class="Femke">Xierpa is another sophisticated workflow tool (or
software). It is Python based, includes amongst others a scheduler, a
bookbuilder and through an interactive visualisation, Xierpa can
translate a conversation with a client directly into a database. In
fact, the model IS the database.</span>

<span class="Femke">If design is to be considered conditional, what are
the kinds of conditions it proposes? When it sets its own rules ... what
conditions of work, what exclusion and inclusion does it produce, what
relationships does it propose, what laws? I've been reading *Rule or
Law*, an essay by Gerrit Noordzij, and try to bring up different types
of conditions.</span> ((“Tschichold (...) wanted his rules to be obeyed
without discussion. He addresses designers in a language for programming
robots. I am not the first to observe this, but I seem to be alone in my
conclusion that Tschichold’s rules obstruct design, undermine
civilization, and offend humanity.”
http://www.hyphenpress.co.uk/journal/2007/09/15/rule\_or\_law))

<span class="Lauren">His whole raisoné becomes clear when Blokland
answers Femke's question</span> ((“If you automate a workflow, at what
point do you think intervention is possible or even desired?")). "Time
can be held as a pragmatic issue when dealing with intervention. But
besides that design should always be conditional. Because if it is not,
than it is just production. It should always reflect a decision or a
condition."

<span class="Femke">[The Open Publishing Lab](http://www.opl.rit.edu)
presents a series of projects and ideas, trying to implement services
that</span> “enable” <span class="Femke">self-publishing, allowing
inexperienced people to</span> “share their stories”. <span
class="Femke">Their strategies range from a self-publishing advisor, a
social networking game to a full web-to-print solution: Page2Pub.
Page2Pub is a clever FireFox extension that allows users to select any
material published on line, strip and re-apply styling and than lay-out
the collaged content in a pdf, templates are provided. Page2Pub collects
content from the web and transforms it into well-formatted, ad-hoc
publications. Their approach is positivist, energetic and of
course</span> “consistent with open source” <span class="Femke">but also
strangely disinterested in both the content and the materiality of what
ends up being published. Wondering what</span> “Keyhole Markup
Languages” are? <span class="Femke">And whether anyone has already tried
out their wonderful idea to make a MRI-scan of a closed book?</span>

<span class="Femke">For the evening session, we move to the Orchid Room,
a proper business setting to discuss the matter with a professional
perspective. We are joined by an advertising professional from Germany
plus more designers and publishers.</span>

<span class="Lauren">While the whole theme of publishing through
different media is subjected to different opinions in the evening
discussion, it seems quite disappointing that there is no real
discussion going on. Some speakers tend to opinionate the whole
discussion without really answering the urgent questions. It often ends
up in frightening statements around the dominant rise of new media
through new technologies (operated by the OPL lab that funnily enough
contribute themselves to this evolution).</span>

<span class="Femke">Well... you could also say that each of them try to
deal with the situation at hand. It is interesting to see different
responses to The Financial Crisis, from Germany, the US or The
Netherlands. Whereas most participants have horror stories to tell about
the demise of the publishing industry, Dutch participants seem not too
impressed. Florian Cramer wonders whether</span> “this is what makes the
conference avant-garde, because here in The Netherlands the bad news has
not arrived yet”

<span class="Femke">The ease by which participants keep fantasizing
about the possibilities of 'de-materialized publishing' makes me
nervous. I try to ask another question. What formatting is going on they
think, and how could form and content remain interlinked? But it only
leads to more confusion</span> ((The people from OPL think I am talking
about quality standards. The next morning, we apologize to each other
for the misunderstanding but before I can clarify my question, Tona
Henderson suggests: “I have been discussing your question with my
colleagues and we thought that maybe we should think about a Dublin Core
for design”. That has me silent for a while, but it is actually an
interesting thought!)) <span class="Femke">I'm than surprised to hear
Alessandro Ludovico, avid collector of punk-zines, declare that content
exists before (and without?) form.</span>

<span class="Lauren">This little stir-up causes some speakers to follow
up this fact by stating that it is indeed necessary to rethink the forms
of liquid news. A slightly wrong side path about content being shaped by
the content is soon answered by Alessandro Ludovico who states that the
content stands on itself and derives its form from the medium.  
Thank you, Alessandro! Finally a useful statement. While the whole
discussion seemed a little dispersed a personal conclusion can be that
through the rise of reflowable, liquid news, it is a designer's
responsibility to research the new publishing media in order to find
useful solutions for publishing liquid news.</span>

![](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/pixelprint/large/1063-p1050512.JPG)

<span class="Femke">We're all exhausted after this but find ourselves a
warm welcome in [Het Poortgebouw](http://www.poortgebouw.nl), where
Kenny has prepared the guestroom and we're invited to wine and
conversation in the kitchen.</span>

<span class="Femke">The next morning starts with a presentation of the
work done at the media workgroup NRC Handelsblad, a 'quality newspaper'
from The Netherlands. We're presented with another intelligent workflow
that is developed from within the company and able to reflect different
working cultures within the company. It allows NRC to publish content in
13 parallel formats.  
</span>

![](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/pixelprint/large/1084-nrcmedia.jpg)

<span class="Lauren">Editorial sites, marketing sites, digital outlets
to media outputs such as phone, ebook, iphone, newsletter and journals
are all presented. NRC&Media tries to model new innovations in
publishing liquid news by researching the different formats. Of course
this does not happen flawless as NRC&Media is split up in many sections
each providing different newsfeeds. The problem of publishing liquid
news coherently is often aggravated because of the fact that formatting
content is at the same time telling a story too.</span> “There would not
be so many problems if you divide content and form strictly”. <span
class="Femke">Yet another version of the technocentric W3C mantra, even
sadder because it is so easy to understand that in difficult economic
times, when newspapers need to explode their product families and reduce
costs simultaneously, 'flawless integration' is a more profitable kind
of wishful thinking than the messiness of dealing with specific
qualities of each particular format. I don't mean that a pdf is not
reflowable and a mobile phone screen only 200 pixels wide. As Gerrit
Imsieke already mentioned,</span> “conversion is costly”. <span
class="Femke">The presentation ends with a Citizen Kane quote. Not sure
whether bitter irony was intended?</span>

> Thatcher: *Is that really your idea of how to run a newspaper ?!*  
> Kane: *I don't know how to run a newspaper, Mr. Thatcher. I just try
> everything I can think of.*

<span class="Lauren">This is followed up by Lou Lichtenberg from the
Netherlands Press Fund who raises the apocalyptic question</span> "Do
newspapers still have a future?" <span class="Lauren">and the
statement</span>"the news of the future is Facebook". <span
class="Lauren">One useful conclusion from this lecture can be that
papers need to be reinvented but it did not reach further.</span>

<span class="Femke">Simon Worthington presents Mute magazine, Mute-POD
and their 'feral' distribution system</span> [More is
More](http://www.moreismore.net). <span class="Femke">He is interested
in employing Amazon, Google and other mainstream systems in order to
make alternative forms of publishing and distribution possible. For
small scale publishers, books are actually the only kind of publishing
product that you can make a living from</span>.

<span class="Femke">Allessandro Ludovico is the current research fellow
at the Communication in a digital age programme. He presents a first
chapter of his forthcoming book. , asking what paper can do in
'non-material' times.</span>“Print is liberating”, <span
class="Femke">he sais; quoting André Breton that</span> “One publishes
to find comrades”. <span class="Femke">Print is for Ludovico about
fostering ideas, a viral but physical communication model. What follows
is a dazzling tour through radical examples of self-publishing, the
underestimated networked distribution system of the post, co-opted
sympathetic printers that made political zines possible, mimeographs and
dripping colors. I discover the Underground Press Syndicate
Directory</span>
((http://en.wikipedia.org/wiki/Underground\_Press\_Syndicate)) <span
class="Femke">and remember The Ranters</span>
((http://en.wikipedia.org/wiki/Ranters)). <span class="Femke">It ends
with the strangely encouraging statement that</span> “This mutation of
print will not be easy nor straightforward”. <span class="Femke">I'm
sorry that I have to leave before the final discussion begins.</span>

<span class="Lauren">At the end of the conference, Florian Cramer
challenges NRC&Media, Matthew Berner, Gerrit Imsieke, Alessandro
Ludovico, Simon Worthington and the OPL-ers for some last critical notes
on the whole pixel/print conference.  
The whole essence of publishing is questioned on the background of
networking media and self-publishing features such as those presented by
OPL. Publishing does no longer simply happen from content to consumer,
from A to B. Nor does it involve the traditional model from the
professional publisher to the amateur reader. Everyone can publish
content, everyone can publish books through POD-publishing, the strict
line between the professions no longer exist nor does it happen
unambiguously.  
The rising complexity of publishing media demands new innovations which
is for instance a goal of NRC&Media, the professional angle. But,
innovation can happen as easily through amateur initiatives, NRC&Media
discusses.</span>"There is definitely a distinction between amateurs and
professionals. Both have their advantages. Being an amateur can give a
sort of freedom which you don't have when working on larger scales as
professionals do. It opens up the space for personal approaches and
critical notes. Working on a small scale does give you this sort of
freedom." <span class="Lauren">The OPL lab happily receive this premise
by expressing their enthusiasm of projects being initiated even when
they are poorly programmed or designed. A same kind of anarchic
enthusiasm is aired, as appealing as Ludovico's lecture on a
constellation of fanzines. The pleasure of expression through the
contemplative act of publishing!</span>

<span class="Lauren">Next Florian Cramer asks the presenters which name
they would have given this conference as the name initially raised some
questions ((OSP for example proposed to call the conference print/vector
;-) )). While all think print/pixel is concise enough some interesting
answers are given. '**Publishing / programming**' offered by Le-Tex
involves the shift in technologies. Matthew Berner rethinks it from the
design angle and operates the term '**System Design**', as designing
nowadays is as much rethinking the system. OPL lab offers '**Print
2.0**', '**Integration conversions**' and '**Publishing, real soon
now**'. NRC handles the topic from quiet a pragmatical
perspective:</span>"We really have this practical problem of designing a
paper or different outputs on a small screen. One issue is scalability.
A lot of design solutions will need to answer the idea of scalability.
So — '**Perspectives on scalability**'?" <span class="Lauren">Finally
Alessandro Ludovico states that as being part of the lecture
organization it is hard to offer a critical answer.</span> "The only
thing I can add is a sort of urgency for printing, publishing. '**The
need for publishing**' — that could be a valuable title."

<span class="Lauren">Cramer brings up that these terms are all very
heavily related to webdesign. Is the web after all overpowering print?
Or how is the interaction between both? He states that Wikipedia as a
whole is a webdesign as it derives its structure from the content. This
is a total different approach than a traditional design-process. The
form now, is completely derived from the structure, interface,
programming… The whole idea of reflowable, liquid content is an idea
that ignores a fixed design. It does not involve formations that
correspond to the content but to the structure of the medium where it is
being published.  
The similar issue of customization brought up by the POD system (FC
mentions the [Piet Zwart Graduation catalogue designed by
OSP](http://ospublish.constantvzw.org/works/500-printed) as an act of
customization) is mentioned as a new print model. FC aims his question
at NRC and asks if NRC would use the option of letting the end-user
customize their own news. NRC answers that this whole idea of newspapers
having a wall of curators and filters is opposite treatment of the
profession.</span> "My editors have the idea that news has to be
accessible to all, and reach as much people as possible. Customization
is a process that a customer should do themselves. I think it is an
overrated concept."

<span class="Lauren">The mood is stirred with a last these that</span>
"this is a conference of almost perfect combinatorics" <span
class="Lauren">which is answered with loud laughter.</span> "The amount
of communality was so high that we did not really have to go deep into
certain subjects in order to understand each other… I would really want
to have this same group of people again, and I am sure that other topics
would be touched. But that it would be as interesting at least!"

\[Applause\]
