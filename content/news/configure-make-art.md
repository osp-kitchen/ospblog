Title: ./configure && make art
Date: 2010-10-26 20:00
Author: Alexandre
Tags: News
Slug: configure-make-art
Status: published

![](http://goto10.org/wp-content/uploads/goto10.org/2010/10/in-between_design_320x240.png "Make Art 2010 identity"){: .alignnone }

[The 2010 edition of the Make Art
festival](http://makeart.goto10.org/2010/?page=welcome&lang=en "Make Art Festival 2010")
will take place at Maison de l'Architecture in Poitiers, France, from
the 4th to 7th of November. This year theme is *in-between design:
rediscovering collaboration in digital art.*

OSP will give a small lecture and be part of the exhibition with the
project
[Nancy](http://ospublish.constantvzw.org/nancy/ "Nancy, collaborative symbol font installation"),
an installation for collaborative symbol font creation. Many OSP friends
will be present as well: [ginger
coons](http://adaptstudio.ca/ "Ginger Coons") (Libre Graphics Magazine),
[Calcyum](http://www.calcyum.org/ "Calcyum graphic design studio"),
[Emanuele Bonetti and Loredana
Bontempi](http://django.parcodiyellowstone.it/pdy/about/ "parcodiyellowstone"),
[&lt;stdin&gt;](http://stdin.fr/ "<stdin> graphic and media design studio")
just to name a few of them.

**Make Art 2010, **Festival of free art and technologies****

Make Art is an international festival dedicated to free/libre arts and
technologies, distributed digital artworks and net art. The sixth
edition of Make Art focuses on works halfway between art and design,
collaborative, scalable and participative methods. The adventure
"in-between design" begins here, to be imagined in this creative
development, where everyone can participate and where artwork keeps on
evolving. Are these projects just curiosities or a real alternative to
the graphic conformism ruled by an industry that dictates its aesthetic
codes? Please come and join us to answer this question...

Make Art is organized by the collective
[GOTO10](http://goto10.org/ "GOTO10 collective"), and its design is by
[Lafkon](http://www.lafkon.net/ "Studio Lafkon").
