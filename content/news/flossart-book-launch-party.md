Title: FLOSS+Art book launch party
Date: 2008-10-19 21:18
Author: Harrisson
Tags: News, Type, Works
Slug: flossart-book-launch-party
Status: published

[![]({filename}/images/uploads/libertinage-300x85.png "libertinage"){: .alignnone .size-medium .wp-image-1118 width="300" height="85"}]({filename}/images/uploads/libertinage.png)  
The [FLOSS+Art book](http://ospublish.constantvzw.org/?p=565) (that we
proudly were commissioned to design :) ) [will be out this thursday at
Mute Magazine
HQ](http://goto10.org/flossart-book-panel-and-party-at-mute-london/)!  
Book preview, panel discussion and software party!

Don't miss the opportunity to also check out the Libertinage font set we
developed for this book: 26 variations of the Linux Libertine font!
~~Which will be available here and there soon~~... [<blink>Available
HERE!</blink>](http://openfontlibrary.org/media/files/OSP/322)

Thursday 23 October / 18:30 - 20:30  
Mute Magazine HQ  
The Whitechapel Centre  
85 Myrdle Street  
London E1 1HL

Long live free edition and free fonts!
