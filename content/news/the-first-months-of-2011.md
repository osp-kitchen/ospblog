Title: The first months of 2011
Date: 2010-12-23 18:57
Author: OSP
Tags: News, LGM 2010, LGM 2011, Print Party, Printing + Publishing, Scribus Re-loaded
Slug: the-first-months-of-2011
Status: published

What the first half of the new OSP-year looks like from here:

**January**  
Start with a bang: Monday January 3 all OSPs gather for a Scribus
experiment. You'll hear more about once we get there! Antoine from
l'École Estienne, Paris joins us for an internship of three months. We
have new plans for some kind of OSP-school (writing more project
proposals, handing in applications ...). Ludi and Pierre H continue to
infuse their students with F/LOSS energy at [ERG](http://www.erg.be/)
and [La Cambre](http://www.lacambre.be/). Reprint of [Openbaar
Schrijver](http://ospublish.constantvzw.org/works/valentine-scripting)
available by the end of the month.  
<!--more-->  
**February**  
With the help of a stagiair from the [Centre de recherche informatique
et droit](http://www.fundp.ac.be/droit/crid) (CRID) we are continuing
work on a license for 3D objects. Special Print Party celebrating the
fact that text and images from Trotski, Emma Goldman, Selma Lagerlof,
Fitzgerald and Paul Klee have fallen into the public domain in Belgium.
Some of us travel to London to work with students from the
[Communication Art & Design department at the Royal College of
Art](http://www.rca.ac.uk/Default.aspx?ContentID=160469&GroupID=159384&CategoryID=36692&More=1)
who want to develop a publication using Scribus.

**March**  
Launching *Tot Later*, a novel by [Ana
Claese](http://en.wiktionary.org/wiki/anaclasis), designed by OSP.
Hopefully [Harrisson](http://ospublish.constantvzw.org/author/harrisson)
will be home from the hospital (he's undergoing chemotherapy and a bone
marrow transplantation).

**April**  
OSP is travelling to Vietnam :-) We are still looking for part of the
funding so not sure how many of us will be able to make the trip.
Presentations, workshops and work with Vietnamese designers on a font
and a publication planned. All of this the outcome of meeting [Hong Phuc
Dang](http://river-valley.tv/how-to-get-contributors-to-your-freelibreopen-source-project-from-vietnam-and-asia/)
at LGM 2010.

**May**  
May is LGM month! Two OSP-representatives travel to Montréal from May
10-13 to participate in the 6th Libre Graphics Meeting, something we
look much forward to. This year's edition will be extra exciting with so
many new developments going on (Libre Graphics Magazine, Scribus
Reloaded, many new participants joining).
