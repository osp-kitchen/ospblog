Title: Monopolios Artificiales sobre Bienes Intangibles
Date: 2007-03-17 09:38
Author: Femke
Tags: News, Intellectual property, Printing + Publishing
Slug: monopolios-artificiales-sobre-bienes-intangibles
Status: published

![buttons.png]({filename}/images/uploads/buttons.png)

[Nomade](http://nomade.liminar.com.ar/), an Argentinian group of artists
using Free Software, just finished a printed publication on intellectual
property, plus a detailed tour through the process:
<http://nomade.liminar.com.ar/wakka.php?wakka=MaBI&v=der>

Included: design sketches, illustrations, description of the pre-press
process, final pdf's and a html version. Of course everything Open
Source. Very nice!
