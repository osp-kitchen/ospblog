Title: Gender Art Net - presentation + workshop
Date: 2010-04-17 13:21
Author: OSP
Tags: News, Berlin, Map, Webdesign
Slug: genderartnet-presentation-workshop
Status: published

![]({filename}/images/uploads/icons.png "icons"){: .alignright .size-full .wp-image-4434 }

OSP defied volcanic dust and gathers in Berlin to present and discuss
the first stage of the [GenderArtNet](http://www.genderartnet.eu/)
project at project space uqbar.

With: Bettina Knaup, Maria Ptqk, Bojana Pejic, Anne Quirynen, Femke
Snelting, Ludivine Loiseau, Anne-Laure Buisson, Nicolas Malevé and
others.

<p>
<center>
[![Nicolas talks about
maps](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=31488&g2_serialNumber=2)](http://gallery.constantvzw.org/main.php?g2_itemId=31486)
[![Ludi presents the design
process](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=31491&g2_serialNumber=2)](http://gallery.constantvzw.org/main.php?g2_itemId=31489)

</center>
</p>

