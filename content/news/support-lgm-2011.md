Title: Support LGM 2011
Date: 2011-03-21 17:20
Author: OSP
Tags: News, LGM 2011
Slug: support-lgm-2011
Status: published

Donate to help F/LOSS developers and designers work together at LGM!

[![](http://www.libregraphicsmeeting.org/2011/donate/pledgie_banner.png)](http://pledgie.com/campaigns/14610)

Use this code to display a banner on your own website:

<p>
<textarea name="snippet" rows="3" cols="55">
[![](http://www.libregraphicsmeeting.org/2011/donate/pledgie_banner.png)](http://pledgie.com/campaigns/14610)

</textarea>
</p>
And this is how it was made: <http://stdin.fr/Bazar/Pledgie>
