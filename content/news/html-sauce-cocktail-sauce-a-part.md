Title: HTML sauce cocktail, sauce à part
Date: 2017-02-15 23:11
Author: OSP
Tags: News
Slug: html-sauce-cocktail-sauce-a-part
Status: published

PREFACE
-------

Ce texte a été initialement écrit pour la revue [Back
Office](http://www.revue-backoffice.com). Pour des raisons de format,
il n'y sera pas publié, mais nous sommes heureux de le partager ici avec
vous dans sa version longue.

HTML2PRINT SANS SAUCE
---------------------

Depuis sa création en 2006, Open Source Publishing dessine des mises en
page et code avec des logiciels libres ou open source. Il s'agit d'abord
de questionner nos pratiques avec ces médiateurs omniprésents que sont
les outils numériques et les communautés dont ils sont issus. Alors que
la matérialité de l'informatique s'efface derrière des interfaces
«intuitives»^[2](#fn:intuitif){: .footnote-ref}^, le logiciel libre,
par sa nature ouverte, invite à saisir l'épaisseur culturelle des
formats, interfaces et usages.

Splines spirographiques, filtres ImageMagick ou lignes de commande sont
quelques-unes des rencontres qui ont changé notre manière d'appréhender
le vecteur, le bitmap ainsi que la manière de construire et de partager
nos propres outils. Mais pour ce qui est de la mise en page, nous nous
retrouvons face à un dilemme cornélien: celui d'avoir à choisir entre
d'un côté une approche essentiellement visuelle, incarnée par Scribus et
InDesign, et de l'autre une approche intégralement programmatique
incarnée par TeX, LaTeX ou Context.

Dans sa tentative de simuler la manipulation directe de l'objet final,
l'approche WYSIWYG se heurte aux limites du paradigme papier/ciseaux.
Prisonnière de l'héritage de Gutenberg, elle ignore le potentiel de
réinvention du média numérique. L'approche programmatique s'avère elle
aussi décevante car systématique et linéaire. Fonctionnant à sens
unique, du code vers le visuel, elle fonctionne bien pour mettre en page
des flux continus mais permet très difficilement de débrayer et de créer
des mises en pages plus articulées, notamment car le format final n'est
plus éditable.

Après nous être au départ plus particulièrement concentrés sur des
objets imprimés, nous avons commencé à investir le web comme espace de
publication. Ce qui nous séduit dans le design web est l'approche
collaborative ainsi que le va et vient qu'il permet entre design visuel
et design par le code. Populaires, ultra-documentés et basés sur des
formats ouverts élaborés par différents acteurs dans un souci de
dialogue ^[3](#fn:dialogue){: .footnote-ref}^ et de continuité,
les langages du web sont de véritables *lingua franca*: ils sont
éditables par de nombreuses manières, «à la main» et via un large
spectre de logiciels visuels ou programmatiques. Le navigateur web,
pièce maîtresse, réunit en un même espace ces différentes approches.
Par sa nature distribuée (une page étant généralement le résultat de
l’agrégation de nombreuses ressources telles des feuilles de style ou
des images), les formats du web permettent un travail collaboratif entre
des personnes aux compétences diverses, contrastant avec l'approche
solitaire des logiciels comme InDesign ou Scribus.

À divers niveaux tous les membres d'OSP ont une pratique du web et cette
question revient : «Ne pourrions-nous pas utiliser les mêmes
méthodologies et outils pour le design imprimé?». Après un petit audit
nous décidons de nous lancer. Le premier projet sera pour le programme
annuel du Théâtre la Balsamine. Mais si un grand nombre de propositions
pour l'imprimé sont déjà prévues par les standards CSS, de nombreuses
fonctionnalités indispensables (traits de coupe, pagination, titres
courants…) ne sont pas encore implémentées dans les navigateurs web.
Pour palier à ces lacunes, nous compilons une série de recettes pouvant
désormais servir de gabarits pour de futurs projets. Nous l'intitulons
html2print.

<figure>
![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.016.jpg)  

<figcaption>
image issue de la première présentation d'OSP d'utilisation d'HTML pour
des documents multi-pages, Libre Graphics Meeting 2013, Madrid

</figcaption>
</figure>

SAUCE COCKTAIL
--------------

Mars 2015, après deux années de pratique d'html2print en interne pour
diverses commandes graphiques, nous pensons que l'outil est prêt à être
«*bêta*-testé» par d'autres, notamment par des designers qui ne sont pas
nécessairement développeurs. L'occasion d'un workshop à la HEAR de
Strasbourg tombe à pic: quatre journées pour apprendre les bases HTML +
CSS et produire un objet imprimé.

Par ses contraintes de tailles d'écrans *multiple devices*, la mise en
page web se doit d'être adaptative, quelque part consciente de
l'environnement dans lequel elle s'installe. De ce point de vue, elle
s'oppose au design imprimé où la plupart des interfaces traditionnelles
gardent nos mains fixées sur les blocs de Gutenberg. Nous proposons aux
étudiants d'expérimenter les *media queries*, conditions CSS qui
adaptent le design d'un site en fonction du dispositif de lecture
(ordinateur, tablette, GSM…) ou de la taille de l'écran du lecteur.
La fluidité du HTML rend très facile le changement de format: un
changement dans les dimensions de la page et le reste coule. Pour épicer
la sauce, nous proposons aux étudiants de travailler sur un Etherpad
^[4](#fn:Etherpad){: .footnote-ref}^ permettant de produire ces différents
formats à partir d'un même document. La taille de la fenêtre du
navigateur change le format de la page imprimée ainsi que son design.
Et, le fait de partager un seul et même document rend l'apprentissage de
HTML et CSS beaucoup plus excitant et rapide. Les étudiants apprennent
par eux-mêmes en voyant les changements des autres en direct.

<figure>
![Screenshot
Etherpad](http://ospublish.constantvzw.org/images/var/albums/HEAD-Strasbourg-HTML-sauce-cocktail/Screenshot%20from%202015-12-15%2018%3A20%3A15.png?m=1450200225)  

<figcaption>
Le *pad* de la feuille de styles commune

</figcaption>
</figure>
![](http://ospublish.constantvzw.org/images/var/resizes/HEAD-Strasbourg-HTML-sauce-cocktail/1.png?m=1450201121)

<figure>
![](http://comgraph.hear.fr/wp-content/uploads/2015/03/bcb-1.jpg)  

<figcaption>
Pyramide des différents formats produits: du A8 au A0.  
Photo © Hugo Serraz, licence Art Libre

</figcaption>
</figure>
Pour profiter pleinement de l'outil, nous proposons de travailler à
partir d'un contenu d'une certaine longueur, avec plusieurs niveaux
d'informations. Nous glanons sur le projet
Gutenberg^[5](#fn:Gutenberg){: .footnote-ref}^ *The Belgian Cook Book* de
Mrs. Brian Luck, une compilation de recettes d'émigrés belges au
Royaume-Uni en 1915. L'avantage de cette archive étant que les livres y
sont proposés dans différents formats dont HTML.

La première journée est consacrée à un cours accéléré sur les
expressions rationelles (grep)^[6](#fn:grep){: .footnote-ref}^ et syntaxe
de base des langages HTML/CSS. Le jour suivant, des groupes *ad hoc* et
perméables se forment autour de plusieurs chantiers: nettoyage et
optimisation des contenus en utilisant des expressions rationnelles;
structuration sémantique des contenus HTML (titres, sous-titres, termes
techniques, listes, etc.) et enfin la définition des styles visuels
communs à tous les formats imprimables.

Puis, cinq équipes sont formées, chacune prenant en charge un format
d'impression: A7 → micro-format travaillant sur une mise en page en
miroir; A6 paysage → fiches de cuisine cannibale où les ingrédients sont
remplacés par des noms de personnes avec grep; 215×330mm → grand format
à consulter posé sur une table; 990×1075mm → affiches sérigraphiées avec
zoom sur les adverbes avec grep; multi-format → grâce à la propriété
*float* de CSS, les blocs de recettes coulent de gauche à droite comme
du texte, la mise en page s'adapte ainsi d'elle-même selon la taille du
livre. Une sixième équipe se consacre à la réalisation d'illustrations
ASCII via Etherpad.

<figure>
![Screenshot
Etherpad](http://ospublish.constantvzw.org/images/var/albums/HEAD-Strasbourg-HTML-sauce-cocktail/2016-02-12_17-28-07_screenshot.png)  

<figcaption>
Le *pad* de la feuille de styles commune

</figcaption>
</figure>
Nous travaillons sur Etherpad^[4](#fn:Etherpad){: .footnote-ref}^,
un éditeur de texte collaboratif. C'est un des points clés du workshop:
la mise en commun du code rend son apprentissage beaucoup plus excitant
et rapide. Les étudiants apprennent les uns des autres en voyant les
changements en direct. Un seul document définit l'ensemble des objets à
produire: la taille de la fenêtre du navigateur change le format de la
page imprimée ainsi que son design, suivant les règles de tailles,
définies en CSS.

<figure>
![](http://ospublish.constantvzw.org/images/var/albums/HEAD-Strasbourg-HTML-sauce-cocktail/DSC_0107.JPG)  

<figcaption>
Pyramide des différents formats: du A0 au A7. Photo © Hugo Serraz,
licence Art Libre

</figcaption>
</figure>
Le dernier jour du workshop est consacré à l'impression, à la prise de
vue^[8](#fn:photos){: .footnote-ref}^ et présentation des objets
éditoriaux. Les fichiers sources et l'historique du projet sont mises en
ligne sur GitHub. ^[9](#fn:github){: .footnote-ref}^.

<figure>
![](http://comgraph.hear.fr/wp-content/uploads/2015/03/bcb2.jpg)  

<figcaption>
Mise à plat des différents objets imprimés.  
Photo © Hugo Serraz, licence Art Libre

</figcaption>
</figure>

DEGUSTATION
-----------

Concevoir un objet imprimé avec HTML est une expérience nouvelle pour
tous et la différence de niveaux des étudiants (degré d'étude,
connaissances techniques) ne s'est pas manifestée. Html2print est un
outil pédagogiquement intéressant car il supprime le clivage
écran/imprimé. De plus, les navigateurs web étant par conception
tolérants à l'erreur, ils permettent toujours d'obtenir un résultat,
même imparfait. Par ailleurs, le fait d'avoir plusieurs objets sur un
même document était très stimulant pour les étudiants qui pouvaient à
tout moment suivre leur évolution. Au-delà leur intérêt
ortho-typographique, les expressions rationnelles furent grandement
utilisées pour tordre les contenus et leur design. Enfin, le fait que
les technologies abordées dans le workshop soient très populaires et
documentées sur le web, en anglais mais aussi en français, aiguise la
motivation des étudiants.

SAUCE SAMOURAI
--------------

Suite au workshop donné à la HEAR Strasbourg, la sauce html2print
commence doucement à prendre et continue de s'imprimer sur papier. Ainsi
deux participants au workshop ont poursuivi leur travaux: Hugo Serraz et
Léna Robin.

<figure>
[![]({filename}/images/uploads/hugo-serraz.jpg){: .footnote-ref}]({filename}/images/uploads/hugo-serraz.jpg)  

<figcaption>
Hugo Serraz a réalisé, à l'aide de notre outil, deux éditions, chacune
imprimant les actualités d'une journée entière du site *lemonde.fr*.  
Photo © Hugo Serraz

</figcaption>
</figure>
<figure>
[![](http://www.lenarobin.com/images/ContextualisedInformations/5250_BPH_04A77_PR00159_1247_035.jpg)](http://www.lenarobin.com/images/ContextualisedInformations/5250_BPH_04A77_PR00159_1247_035.jpg)  

<figcaption>
Léna Robin <http://lenarobin.github.io/> a concocté une recette
alternative avec Python et Prince XML. «*Contextualised Informations*
est une réflexion sur la prévention et l'analyse des rumeurs et fausses
informations qui circulent en ligne.»  
Photo © Léna Robin

</figcaption>
</figure>
###

### LIENS

-   Fichiers source d'html2print: <http://osp.kitchen/tools/html2print/>
-   Article de blog des étudiants de la HEAR:
    <http://comgraph.hear.fr/2015/03/html-sauce-cocktail/>
-   Fichiers source du workshop:
    <https://github.com/HEAR/HTML_sauce-cocktail-workshop-OSP>
-   Site répertoriant des mises en page générées par du code:
    <http://computedlayout.tumblr.com/>

### NOTES

<div class="footnote">

1.  <div id="fn:sauce">

    </div>

    Nous avons nommé notre workshop «HTML sauce cocktail» en référence
    au contenu apporté pour le workshop, un livre de recettes de cuisine
    belges, et aux sauces typiques des fritkots du
    royaume. [↩](#fnref:sauce "Jump back to footnote 1 in the text"){: .footnote-ref}
2.  <div id="fn:intuitif">

    </div>

    Qui ne demande pas à penser, basé sur des reflexes, des habitudes,
    des gestes
    familier. [↩](#fnref:intuitif "Jump back to footnote 2 in the text"){: .footnote-ref}
3.  <div id="fn:dialogue">

    </div>

    Cela ne signifie pas qu'il n'y ait pas des jeux de pouvoir dans la
    définition de ces
    formats. [↩](#fnref:dialogue "Jump back to footnote 3 in the text"){: .footnote-ref}
4.  <div id="fn:Etherpad">

    </div>

    Etherpad est un éditeur de texte collaboratif et synchrone: les
    utilisateurs peuvent écrire en même temps sur un même doculent
    <http://etherpad.org/>. Dû aux restrictions de sécurité web des
    institutions publiques, nous ne pouvons pas utiliser le serveur
    Etherpad personnel d'OSP. Nous avons dû aller sur Framapad
    <http://framapad.org/> pour contourner ce
    problème. [↩](#fnref:Etherpad "Jump back to footnote 4 in the text"){: .footnote-ref}
5.  <div id="fn:Gutenberg">

    </div>

    Le  projet Gutenberg est une archive de livres du domaine public:
    des livres que l'on peut redistribuer et réutiliser librement parce
    que les droits d'auteur ont expiré 70 ans après la mort de celui-ci.
    <http://gutenberg.org/> [↩](#fnref:Gutenberg "Jump back to footnote 5 in the text"){: .footnote-ref}
6.  <div id="fn:grep">

    </div>

    Ensemble de fonctions permettant d’automatiser la recherche et le
    remplacement d’éléments définis au préalable dans un texte.
    Une fonctionnalité que l'on retrouve dans divers langages de
    programmation, d'éditeurs de code et même dans…
    InDesign. [↩](#fnref:grep "Jump back to footnote 6 in the text"){: .footnote-ref}
7.  <div id="fn:participants">

    </div>

    Intervenants : Stéphanie Vilayphiou, Colm O’Neill et Ludi Loiseau.
    Avec les étudiants en communication graphique années 2 à 5 : So-Hyun
    Bae, Laura Burucoa, Charlotte Chowdurry, Victor Donati, Montasser
    Drissi, Angéline Girard, Romain Goetz, Manon Hachad, Quentin Juhel,
    Théophile Martin, Arman Mohtadji, Clara Neumann, Lisa Pagès,
    Benjamin Riollet, Lena Robin, Hugo Serraz, Caroline
    Sorin. [↩](#fnref:participants "Jump back to footnote 7 in the text"){: .footnote-ref}
8.  <div id="fn:photos">

    </div>

    Consultable sur le blog des étudiants de la HEAR :
    <http://comgraph.hear.fr/2015/03/html-sauce-cocktail>. [↩](#fnref:photos "Jump back to footnote 8 in the text"){: .footnote-ref}
9.  <div id="fn:github">

    </div>

    <https://github.com/HEAR/HTML_sauce-cocktail-workshop-OSP>. [↩](#fnref:github "Jump back to footnote 9 in the text"){: .footnote-ref}

</div>

<style>
.post img {: .footnote-ref} figcaption {: .footnote-ref}</style>
