Title: In preparation: Research meeting Co-position
Date: 2012-01-02 12:30
Author: Femke
Tags: News, future, LGRU, Tools
Slug: in-preparation-research-meeting-co-position
Status: published

![](http://dev4.restruct-web.nl/wp-content/uploads/2012/01/Plan.jpg "Plan"){: .alignnone .size-full .wp-image-263 }

Over at the [Libre Graphics Research Unit](http://www.lgru.net) we are
preparing a second 'Research Meeting' that will take place in Brussels
from 22 to 25 February 2012. Developers, designers and theoreticians
from all over Europe will gather to imagine future Libre Graphics tools
together. The theme for this particular edition is *Co-position*, and
we'll speculate about software for arranging texts and graphical
elements. How can we re-imagine lay-out from scratch? What tools do we
need to support decentralized collaboration? How can we bring together
canvas editing, dynamic lay-outs, web-to-print and Print On Demand in
more interesting ways? What kind of messages do we propagate and how
does this change the tools we need? These and many more questions will
be addressed in four intense days of presentations, discussions,
development sessions and prototyping.

If you are interested in contributing, please write us at donatella @
constantvzw.org. Participation is free but places are limited.
