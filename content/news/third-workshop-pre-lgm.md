Title: Third workshop @ pre-LGM
Date: 2011-04-30 11:35
Author: OSP
Tags: News, LGM 2011, Workshops + teaching
Slug: third-workshop-pre-lgm
Status: published

[\[caption id="attachment\_6332" align="alignleft" width="400"
caption="Joy Penroz using Toonloop (Mérida, Yucatán,
México)"\]]()[![]({filename}/images/uploads/toonloop_periferia.jpg "toonloop_periferia"){: .size-medium .wp-image-6332 }]({filename}/images/uploads/toonloop_periferia.jpg)\[/caption\]</a>

We're happy to announce that [Alexandre
Quessy](http://alexandre.quessy.net) will join us next weekend for a
[Toonloop](http://alexandre.quessy.net/?q=toonloop) animation-workshop
at [StudioXX](http://www.studioxx.org/en/).

For the workshops on Saturday, places are limited. Please register
through <http://www.libregraphicsmeeting.org/2011/registration> and
mention the workshop(s) of your choice in the comments box. First in,
first served :-)

Of course you are always welcome to drop by while workshops are going
on. For the **Famous People** session on Sunday we try to fit as many as
possible. Just bring your laptop and a proposal for a VIP to add to the
collection.

SATURDAY 7 MAY  
A. 11:00-16:00 **Digital Pattern Making** (Susan Spencer)  
B. 11:00-16:00 **Toonloop animation** (Alexandre Quessy)

SUNDAY 8 MAY  
C. 13:00-17:00 **Famous People** (Brad Phillips, ginger coons, Tom
Lechner, ...)

On Saturday there is also a debate and exhibition opening. More details
here:
[http://www.libregraphicsmeeting.org/2011/pre-lgm](http://www.libregraphicsmeeting.org/2011/pre-lgm%20)
