Title: Bon Voyage OSP!
Date: 2011-04-01 09:55
Author: OSP
Tags: News, LGM 2010, LGM 2011, LGM 2012, Libre Fonts, Scribus, Vietnam
Slug: bon-voyage-osp
Status: published

[![]({filename}/images/uploads/open-design.png "open-design"){: .alignnone .size-full .wp-image-6053 }](http://opendesign.asia)  
Today 5 OSPs travel to Ho Chi Minh City in Vietnam, to participate in
[**Open Design Week**](http://opendesign.asia). Alex, John, Pierre,
Pierre and Stéphanie will be touring through the Mekong Delta to meet up
with local F/LOSS fans through workshops, presentations and an
exhibition. The second week is reserved for an intense work session with
the aim to produce a collaborative font between Asian and European
participants. This font will be used to produce a publication
on-the-spot. **Open Design Week** is initiated by Mario Behling and
[Hong Phuc
Dang](http://river-valley.tv/media/conferences/lgm2010/0104-Hong-Phuc-Dang/).
Hong Phuc came to Brussels in 2010 for the 5th edition of the Libre
Graphics Meeting, and hopefully she'll be at LGM 2011 too.

*Bon Voyage OSP!*
