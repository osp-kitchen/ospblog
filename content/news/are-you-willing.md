Title: Full exclusive rights in perpetuity
Date: 2008-10-08 22:38
Author: OSP
Tags: News, Culture of work, Type
Slug: are-you-willing
Status: published

`Hi, OSP.`  
`I found you on Open Font Library. I am building a website containing webmasters resources, and I'd like to have 3 original exclusive fonts that I can give away to members who join my site.`  
`I'd like to pay you to create 3 original fonts - are you willing?`  
`If you have 3 old fonts on your harddrive that you created and have not sold or given away to anyone else yet - then those might work as well.`  
`Anyway, email me or call me and let me know if you're interested. I can pay $75 per font. I want full exclusive rights in perpetuity.`  
`I'm contacting several different Open Font Library creators with this offer.`  
`All the best,`  
`xxx xxxxx`

----

Dear xxx,

I don't think you got the point.

Our work, and the idea behind the Open Font Library project, is exactly
the opposite of making money through selling "exclusive rights in
perpetuity". We think our fonts are worth more than the \$75 you are
prepared to pay, so we create \*Free Fonts\* because we think that it is
more valuable to share them with others and have them used, developed
and distributed by everyone, anywhere, for every occasion. You could
give them away to your members too, if you'd like to.

Good luck with your website!

OSP
