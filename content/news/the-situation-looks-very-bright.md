Title: The situation looks very bright
Date: 2008-03-12 11:38
Author: Femke
Tags: News, Tools, Standards + Formats
Slug: the-situation-looks-very-bright
Status: published

In the summer of 1997, the [NLNet Foundation](http://nlnet.nl) sold its
commercialized internet provision activities to UUNET (the internet
subsidiary of WorldCom). This created a fund from which the foundation
now supports activities that provide network technology to the community
and keep outcomes in the "public domain". NLnet has picked *Identity*,
*Privacy & Presence* and *Open Document Format* as the two main areas it
will focus its funding on:

> A self-imposed social acceptance of closed formats is sometimes
> difficult to break through, but with the dominant binary formats of
> the past being deprecated by their vendors themselves the situation
> looks very bright.

<http://nlnet.nl/themes/2008odf/>
