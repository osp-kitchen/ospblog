Title: ...quietly digitizing around 1,000 public domain titles every day
Date: 2008-05-19 10:17
Author: nicolas
Tags: News, Archiving, Digitalisation
Slug: quietly-digitizing-around-1000-public-domain-titles-every-day
Status: published

![Digitalising book for the Internet
Archive]({filename}/images/uploads/02_comp2.jpg "02_comp2"){: .alignleft .size-medium .wp-image-503 }

[A photo-reportage by Dave
Bullock](http://www.wired.com/entertainment/theweb/multimedia/2008/03/gallery_internet_archive),
published on the Wired, that shows the process of digitalising books for
the Internet Archive. This process is still done manually due to the
fragility of old books and their size variance.

*"For those picturing an efficient, automated process involving robotic
arms and high-tech scanners, the scanning at the University of
California's Northern Regional Library Facility is relatively primitive.
With monastic diligence, workers sit in book-scanning stations and
manually turn pages all day long."*
