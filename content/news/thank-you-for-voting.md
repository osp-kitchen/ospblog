Title: Thank You for Voting!
Date: 2010-03-05 13:36
Author: OSP
Tags: News, LGM 2010
Slug: thank-you-for-voting
Status: published

We asked your opinion: [Should the logo we proposed for the Libre
Graphics Meeting drip or not?](/drip-poll) 50 readers voted, and **yes**
was selected in total 51 times and **no** 34 times (more than one answer
possible). Apart from that, strong opinions were voiced on- and offline.
Interestingly, these often were *against* the drip. So, we'll probably
need to keep things as they are -- we'll circulate flyers and stickers
with melted squares, and the **M** for **Meeting** flag on official
channels. For T-shirts, we'll prepare you a surprise :-)

Keep donating to LGM 2010 by the way:
<http://pledgie.com/campaigns/8926>  
![]({filename}/images/uploads/drip-cut.gif "drip-cut"){: .alignright .size-full .wp-image-3994 }
