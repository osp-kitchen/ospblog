Title: Bienvenue to Hadopi logo
Date: 2010-01-14 12:22
Author: nicolas
Tags: News
Slug: bienvenue-to-hadopi-logo
Status: published

The [Hadopi](http://www.laquadrature.net/en/HADOPI) is a big thing in
France. At its origin, a piece of legislation that implements the
[*three strikes*
model](http://en.wikipedia.org/wiki/Three_strikes_(policy)) for illegal
downloads. You receive three warnings and then you are 'banned' from the
internet. The authority who is in charge of applying the sanction is the
Hadopi. A new logo has been created for Hadopi. And provoked a flurry of
discussions about ... typography.

It seems that the logo is made with a proprietary font that can only be
used by France Telecom (called *Bienvenue*, ironically). The graphic
designers who made the logo issued a statement saying that it was a
wrong version of the logo that had been sent by mistake and released the
'right' one made with two other commercial fonts. The problem being they
only acquired the licenses for those in the morning of the second
release, leading to the suspicion they had them on their hard drives
before having bought them.

Read more:  
<http://www.itespresso.fr/logo-hadopi-meli-melo-dans-la-typo-33128.html>  
<http://www.lepost.fr/article/2010/01/11/1881457_logo-hadopi-illegal-y-avait-mieux-a-pirater-que-france-telecom.html>

<small>Laurence, thanks for the tip</small>
