Title: Cookbook Launch
Date: 2009-06-22 07:56
Author: Yi
Tags: News, Drawing, Printing + Publishing, Recipes, Scribus
Slug: cookbook-launch
Status: published

**22 June, 16:00 @ Puerto, Varkensmarkt 23, Brussels**

[![cover\_recto\_th]({filename}/images/uploads/cover_recto_th.png "cover_recto_th"){: .float}](/puerto)
Join us this afternoon for the long awaited festive launch of the
[**Puerto Livre de Cuisine Kookboek**](/puerto): 70 delicious recipes,
written, translated (Dutch and French) and illustrated in collaboration
with the inhabitants of Puerto. Puerto is an organisation working from
the center of Brussels, where they generously support anyone in need of
a home. The book was laid out in Scribus, Gimp and Linux Libertine and
is now available for only **12,50 €**. If you're in Brussels, order your
copy at: puerto@archipel.be or buy the POD-version from lulu.com! All
benefits support the work of Puerto.  
[![Support independent publishing: buy this book on
Lulu.](http://www.lulu.com/services/buy_now_buttons/images/orange.gif)](http://www.lulu.com/commerce/index.php?fBuyContent=7306302)

Some pictures of the launch:  
<http://ospublish.constantvzw.org/image/?level=album&id=33>
