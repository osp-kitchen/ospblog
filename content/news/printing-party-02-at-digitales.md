Title: Printing Party 0.2 at Digitales
Date: 2006-06-04 14:39
Author: Femke
Tags: News, Print Party, Printing + Publishing
Slug: printing-party-02-at-digitales
Status: published

![slingers](http://ospublish.constantvzw.org/blog/wp-content/_slingers.JPG "slingers"){: }  
At this years' edition of
[Digitales](http://www.stormy-weather.be/digitales-2006/), we tested out
a recipe for producing booklets, using texts with open licenses that are
available on line. The recipe was an excuse to discuss the relation
between software and design, why it would be interesting for
designers/publishers to consider using FLOSS tools and what the problems
might be.  
<!--more-->  
After demonstrating the various steps in the process, here's me proudly
showing the end result:

[![showing the
result](http://ospublish.constantvzw.org/blog/wp-content/_DSC00678.JPG "showing the result"){: }](http://ospublish.constantvzw.org/blog/wp-content/DSC00678.JPG)

[![folding +
stapling](http://ospublish.constantvzw.org/blog/wp-content/_DSC00677.JPG "folding + stapling"){: }](http://ospublish.constantvzw.org/blog/wp-content/DSC00677.JPG)

[![screen](http://ospublish.constantvzw.org/blog/wp-content/_DSC00676.JPG "screen"){: }](http://ospublish.constantvzw.org/blog/wp-content/DSC00676.JPG)

[![recipe](http://ospublish.constantvzw.org/blog/wp-content/_DSC00673.JPG "recipe"){: }](http://ospublish.constantvzw.org/blog/wp-content/DSC00673.JPG)
