Title: Towards a Ransom Note for Libre Typography
Date: 2011-05-02 18:26
Author: John
Tags: News, context, libre typography
Slug: towards-a-ransom-note-for-libre-typography
Status: published

Shortly before leaving for [Open Design Weeks](http://opendesign.asia)
in Vietnam, I received my copy of [Fonts in
Context](http://www.boekplan.com/node/11) from Boekplan in the mail.
It's a start in the direction of generating a higher volume of
documentation for Context, an issue which remains a sticking point for
broader adoption of the software.

Perusing it I came across an interesting feature called font fallbacks.
These allow you to specify a specific font to be used in the case your
main typeface is a missing any glyphs. Thanks to Context's consummate
configurability, a typeface can utilize multiple fallbacks and one can
specify which Unicode hexcodes (or ranges thereof) are to be substituted
in any given case. This allows flexible typographic responses to
inadequate coverage in any given typeface.

But my favorite aspect of this feature's configurability is the

    force

parameter. By passing

    force=yes

into the parameter of the macro, one can swiftly replace a character in
any typeface with the missing (or totally different) character from any
another.

This might not seem too useful, but in terms of doing actual
generative-style design through a generative typesetting process, it
provides the perfect platform for a functional design piece I've
tentatively labeled 'Libre Ransom'. My goal is to have something of this
working by (or during) [Libre Graphics Meeting
2011](http://www.libregraphicsmeeting.org/2011/) in Montreal.

The idea behind 'Libre Ransom' is to build a visual test for libre font
coverage of underserved alphabets. By using a relatively simple phrase
and translating it into various languages, we can generate typescript
definitions in Context which will utilize the maximum available amount
of libre typefaces for the phrase. You can think of this as something
like the ACID test suite for CSS, which you can watch visually gauge the
capabilities of your browser's implementation.

Thanks to the contributions being made to the (soon to be Phoenix-like)
[Open Font Library](http://openfontlibrary.org) and [Google Web
Fonts](http://www.google.com/webfonts), English will have every
individual character of it's phrase in a different font. Likely this
will be true of more complicated Latin alphabets as well.

Other languages, however, will not be so lucky.

On the one hand this project will be a showcase for the already
broad-range of coverage for the languages of European extraction. On the
other hand, it will serve as a reminder of the privileged position of
those languages, their tendencies towards colonization of lingustic,
economic, and physical space, and a signal of an intent to utilize the
unique capacity of libre type, our freedom to contritube, in order to
ensure that under-served languages continue to see more and more options
for their *liberated typography*.

*This post was re-published from my blog at* [dripping
digital](http://drippingdigital.com/blog/2011/05/towards-a-ransom-note-for-libre-typography/trackback/).
