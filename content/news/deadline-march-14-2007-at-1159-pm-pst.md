Title: Deadline March 14, 2007 at 11:59 PM PST
Date: 2007-03-08 12:48
Author: Femke
Tags: News, Type, Libre Fonts
Slug: deadline-march-14-2007-at-1159-pm-pst
Status: published

The **Open Font Library** (OFL.o) needs a logo to help identify their
project. They want the community to help create this logo and three
judges from the OFL.o will select the winning logo which they will use
in all of their branding.

Logos need to be submitted in .svg before March 14, 2007 at 11:59 PM PST

Details here: <http://openfontlibrary.org/?ccm=/OFLBLogo>
