Title: Jonctions/Verbindingen 10 Live
Date: 2007-11-25 13:15
Author: OSP
Tags: News
Slug: jonctionsverbindingen-10-live
Status: published

\[All lectures have been archived + will be on line soon!\]

<del datetime="2007-12-03T15:17:33+00:00">Jonctions/Verbindingen 10
Festival has started! Follow lectures + discussions on line:
<http://www.constantvzw.com/vj10/live>

OSP selection:

~~*Sunday 25/11 | 20:45*~~  
[~~**InfoEnclosure-2.0**</a> Dmytri Kleiner~~  
*Saturday 1/12 | 12:00*  
](http://data.constantvzw.org/site/spip.php?article140)[**Open source
software
usability**](http://data.constantvzw.org/site/spip.php?article150)
Görkem Çetin  
*Saturday 1/12 | 17:00*  
[**From centres of calculation to centres of envelopment: intensive
movement in digital signal
processing**](http://data.constantvzw.org/site/spip.php?article160)
Adrian Mackenzie  
*Saturday 1/12 | 19:00*  
[**La répétition d’un geste ne rend-elle pas forcément
fou?**](http://data.constantvzw.org/site/spip.php?article153) Inès
Rabadán

</del>
