Title: Linux Limousine
Date: 2009-01-05 18:42
Author: Harrisson
Tags: News, Type, Works, Font, Fontforge, Libre Fonts, Release, Type
Slug: linux-limousine
Status: published

[![]({filename}/images/uploads/text2383.png "text2383"){: .alignnone .size-full .wp-image-1635 }]({filename}/images/uploads/text2383.png)

We're currently working on a poster for the support comittees for the
nine people accused of “criminal association for the purposes of
terrorist activity". They were arrested the 11th of november 2008, in
France, and 2 of them are still in jail.

They and others are the victims of a witch-hunt in which the word
“terrorism” is applied to any ideas and practices which challenge the
status quo. An international movement is emerging in their support.

For the poster, we re-mixed an open font, the [Free
Sans](http://en.wikipedia.org/wiki/Free_UCS_Outline_Fonts) (think of
Free Avec ;) as Pierre might say) from **Free UCS Outline Fonts**.

Even if the license is a bit unclear:

> From FreeSans font:  
> Copyleft 2002, 2003, 2005, 2008 Free Software Foundation.

We did a version with A, R, a, and t glyph modified. The general aspect
of the font completly changed. There's only the regular weight yet, but
other may follow. Other versions as well...

Those "terrorists" were arrested in the Goutaillou Farm, in Tarnac,
[Corrèze](http://en.wikipedia.org/wiki/Corr%C3%A8ze). Corrèze is part of
the [Limousin](http://en.wikipedia.org/wiki/Limousin_(region)) region.
This is why we called it Limousine.  
It is also referred to the Limousine car, or limo, which may come from
this etymology:

> Le nom limousine viendrait du Limousin, la région de Limoges, dont les
> habitants portaient une vaste pélerine pour se protéger de la pluie,
> ou en référence à Charles Jeantaud, né à Limoges (1843-1906),
> l'inventeur de ce type de carrosserie appliquée aux premières voitures
> confortables. L'origine la plus probable est celle du véhicule
> hippomobile appelé limousine.

We hope they'll go free soon.

[Site of the US support committee for the Tarnac
9](http://tarnac9.wordpress.com/)  
[Site des comités de support](http://www.soutien11novembre.org/)
