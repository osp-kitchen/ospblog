Title: Sapins-sapins by hand
Date: 2008-06-23 21:56
Author: Harrisson
Tags: News, Tools, Works, Design Samples, Handmade, Music
Slug: sapins-sapins-by-hand
Status: published

[![poster ptit faystival
2008]({filename}/images/uploads/pf_web.png "pf_web"){: .alignleft .size-full .wp-image-543 }]({filename}/images/uploads/pf_web.png)  
<!--more-->  
[Petit-Fays](http://fr.wikipedia.org/wiki/Petit-Fays) is a charming
small village in the quiet remote pine covered mountains of the belgian
[Ardennes.](http://en.wikipedia.org/wiki/Ardennes)

Each year, there is the Ball of Petit-Fays village. During this week-end
there are citizen diners, a big tent for feast and dance, an open air
bar with cherry beer and barbecue, a nomadic friterie and a wild night
long disco.

Some "Enfants du Pays", with strong musical professionnal amateurisme
skills, hacked this traditionnal feast by adding smoothly a live music
festival for 4 years. This **[Ptit
Faystival](http://ptitfaystival.livejournal.com/)** opened the
countryside community to share pop - rock - folk music with a friendly
urban public during a mythical week-end.

This year we had the honor to be asked to do the poster. And we've done
it entierelly by hand. We mixed spraycan/stencyls with silkscreen to get
a forest of 200 singular pines.

Feel warmly welcome to join this partying forest! It will be this july
12, at 4pm, and for at least 24 hours...

> = = = = = = = =  
> P'TIT FAYSTIVAL  
> = = = = = = = =
>
> Dans le cadre charmant du village de Petit-Fays,
>
> à partir de 16h, un festival de concerts  
> de musiciens  
> hors du commun  
> sous le chapiteau où commence à 22h,
>
> le bal annuel du village  
> animé par DJ New Sensation.
>
> Le camping gratuit est possible sur place.
>
> **Les sites des musiciens:**
>
> [www.myspace.com/pekkokappi](http://www.myspace.com/pekkokappi)
>
> <http://lespotagersnatures.free.fr/contenu/participants/?g=27>
>
> [www.myspace.com/twopindin](http://www.myspace.com/twopindin)
>
> [www.myspace.com/raypacinoensemble](http://www.myspace.com/raypacinoensemble)
>
> [www.myspace.com/djelephantpower](http://www.myspace.com/raypacinoensemble)
>
> **Itinéraire:**
>
> Autoroute Bruxelles-Luxembourg venant de Bruxelles  
> sortie 22 Beauraing, aller jusqu'à Beauraing  
> puis direction Bouillon (N95).  
> Après Bièvre, prendre à droite direction Petit-Fays.
>
> Venant de Luxembourg, sortie 25, direction Reims-Bouillon N89 sur
> 20km.  
> Sortie à Menuchet-Dinant et prendre la N95 direction Dinant (10km).  
> Avant Bièvre prendre à gauche direction Petit Fays.
>
> Gare SNCB: Graide
