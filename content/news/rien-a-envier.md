Title: Rien a envier!
Date: 2009-02-19 17:47
Author: Harrisson
Tags: News, Libre Fonts, Not-Courier sans, Presentations, Printing + Publishing, Scribus, Type
Slug: rien-a-envier
Status: published

![p1080836]({filename}/images/uploads/p1080836.jpg "p1080836"){: .alignleft .size-full .wp-image-1909 }  
Enfin! FLOSS+Art Book is Launched!  
Damn it looks good!  
<!--more-->  
![p1080853]({filename}/images/uploads/p1080853.jpg "p1080853"){: .alignleft .size-full .wp-image-1909 }  
![p1080861]({filename}/images/uploads/p1080861.jpg "p1080861"){: .alignleft .size-full .wp-image-1909 }

Print is nice, typographic grid kicks, and "n'a rien a envier" to
proprietary software book production.  
The 26 libertinage fonts work very well combined with NotCourier.  
Fidget letterines enhance.  
Dynamic layout, good rythm from start to end.  
Not boring visually for a text book.  
We're very very proud!  
Small step for printing, big step for open design!
